<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/15/2018
 * Time: 3:23 PM
 */
include APPPATH."controllers/BaseController.php";
class Attorney extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if( ! isset($this->session->userdata('logged_user')['id']))
        {
            redirect(base_url(''));
        }
        if($this->session->userdata('logged_user')['type'] != 1)
        {
            if($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-user-info-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 2)
            {
                redirect(base_url('card-user-info-form'));
            }
            else
            {
                redirect(base_url());
            }
        }
        else
        {
            if($this->session->userdata('logged_user')['let_to'] === 'let_to_silver' || $this->session->userdata('logged_user')['let_to'] === 'let_to_gold')
            {
                redirect(base_url('debit-page'));
            }
            elseif ($this->session->userdata('logged_user')['let_to'] === 'solved' || $this->session->userdata('logged_user')['let_to'] === 'msg_to_user')
            {
                redirect(base_url('solved-page'));
            }
        }
        $exist = $this->db->select('id')->from('logged_users')->where('status',1)->where('id' , $this->session->userdata('logged_user')['log_id'])->get()->num_rows();
        if($exist == 0)
        {
            redirect(base_url('log-out'));
        }
        $this->lang->load('form_validation_lang','persian');
        $this->config->set_item('language', 'persian');
    }

    public function user_info_form()
    {
        $data['guide'] = $this->db->select('*')->from('settings')->where('in_group','guide')->where('status' , 1)->where('item' , 'user_info')->get()->row()->value;
        if(in_array('basic',explode(',',$this->session->userdata('logged_user')['access_lvl'])))
        {
            $data['user_info'] = $this->M_UserMeta->get_users_info();
            $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row()->tracking_code;
        }
        else
        {
            if($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-user-info-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 1)
            {
                redirect(base_url('attorney-user-info-form'));
            }
            else
            {
                redirect(base_url('card-user-info-form'));
            }
        }
        $data['title'] = 'تکمیل اطلاعات شرکا';
        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        //var_dump($this->session->userdata('logged_user'));die;
        $this->UserView('attorney/attorney_users_info_form',$data);

    }

    public function user_info_form_process()
    {
        $inputs = $this->input->post();
        $config = [
            [
                'field' => 'first_name',
                'label' => 'نام',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'last_name',
                'label' => 'نام خانوادی',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'father_name',
                'label' => 'نام پدر',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'birth_certif_number',
                'label' => 'شماره شناسنامه',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'national_code',
                'label' => 'کد ملی',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'birth_date',
                'label' => 'تاریخ تولد',
                'rules' => 'trim|required|check_date',
                'errors' => ['check_date' => 'تاریخ به شکل درستی وارد نشده است']
            ],
            [
                'field' => 'address',
                'label' => 'ادرس',
                'rules' => 'trim|required|max_length[500]'
            ],
            [
                'field' => 'postal_code',
                'label' => 'کد پستی',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'home_number',
                'label' => 'شماره ثابت',
                'rules' => 'trim|required|Regex_home_number',
                'errors' => ['Regex_home_number' => 'شماره ثابت را به صورت صحیح وارد کنید']
            ],
            [
                'field' => 'phone_number',
                'label' => 'شماره تلفن',
                'rules' => 'trim|required|Regex',
                'errors' => ['Regex' => 'شماره تلفن وارد شده اشتباه است']
            ]
        ];
        $this->form_validation->set_rules($config);
        if($this->form_validation->run() == TRUE)
        {
            if( $inputs['func'] == 'edit' || ( ! empty($_FILES['birth_certificate_img']['tmp_name']) && is_uploaded_file( $_FILES['birth_certificate_img']['tmp_name']) && ! empty($_FILES['national_card_img']['tmp_name']) && is_uploaded_file( $_FILES['national_card_img']['tmp_name'])) )
            {
                if($inputs['func'] == 'edit')
                {
                    $result = $this->db->select('*')->from('user_meta')->where('user_id' , $inputs['user_id'])->get()->row();
                    if(! empty($_FILES['birth_certificate_img']['tmp_name']))
                    {
                        $img1 = $this->upload_image_ftp('birth_certificate_img', 'user_meta');
                    }
                    else
                    {
                        $img1 = explode('/' ,$result->birth_certificate_img)[1];
                    }
                    if(! empty($_FILES['national_card_img']['tmp_name']))
                    {
                        $img2 = $this->upload_image_ftp('national_card_img', 'user_meta');
                    }
                    else
                    {
                        $img2 = explode('/' ,$result->national_card_img)[1];
                    }
                    if(! empty($_FILES['membership_card_img_front']['tmp_name']))
                    {
                        $img3 = $this->upload_image_ftp('membership_card_img_front', 'user_meta');
                    }
                    else
                    {
                        if(isset($result->membership_card_img_front[0]))
                        {
                            $img3 = explode('/', $result->membership_card_img_front)[1];
                        }
                        else
                        {
                            $img3 = $img3 = 'default_default.png';
                        }
                    }
                    if(! empty($_FILES['membership_card_img_back']['tmp_name']))
                    {
                        $img4 = $this->upload_image_ftp('membership_card_img_back', 'user_meta');
                    }
                    else
                    {
                        if(isset($result->membership_card_img_back[1]))
                        {
                            $img4 = explode('/', $result->membership_card_img_back)[1];
                        }
                        else
                        {
                            $img4 = 'default_default.png';
                        }
                    }
                }
                else
                {
                    $img1 = $this->upload_image_ftp('birth_certificate_img', 'user_meta');
                    $img2 = $this->upload_image_ftp('national_card_img', 'user_meta');
                    if(! empty($_FILES['membership_card_img_front']['tmp_name']))
                    {
                        $img3 = $this->upload_image_ftp('membership_card_img_front', 'user_meta');
                    }
                    else
                    {
                        $img3 = 'default_default.png';
                    }
                    if(! empty($_FILES['membership_card_img_back']['tmp_name']))
                    {
                        $img4 = $this->upload_image_ftp('membership_card_img_back', 'user_meta');
                    }
                    else
                    {
                        $img4 = 'default_default.png';
                    }
                }

                if(($img1 != FALSE && $img2 != FALSE && $img3 != FALSE && $img4 != FALSE))
                {
                    $inputs['birth_certificate_img'] = 'user_meta/' . $img1;
                    $inputs['national_card_img'] = 'user_meta/' . $img2;
                    $inputs['membership_card_img_front'] = 'user_meta/' . $img3;
                    $inputs['membership_card_img_back'] = 'user_meta/' . $img4;
                    $result = $this->M_UserMeta->insert_new_user($inputs, 'attorney');
                    echo json_encode(['status' => TRUE, 'result' => $result]);
                }
                else
                {
                    echo json_encode(['status' => FALSE, 'result' => 'تصاویر انتخابی شما مجاز نمی باشد . لطفا به حجم و نوع فایل انتخابی توجه فرمایید .']);
                }

            }
            else
            {
                echo json_encode(['status' => FALSE, 'result' => 'لطفا همه تصویر ها را پر کنید .']);
            }
        }
        else
        {
            echo json_encode(['status' => 0, 'result' => $this->form_validation->error_array()]);
        }
    }

    public function attorney_form()
    {
        $data['guide'] = $this->db->select('*')->from('settings')->where('in_group','guide')->where('status' , 1)->where('item' , 'attorney')->get()->row()->value;
        if(in_array('users',explode(',',$this->session->userdata('logged_user')['access_lvl'])))
        {
            $data['attorney_info'] = $this->M_Attorney->get_user_attorneys($this->session->userdata('logged_user')['log_id']);
            $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row()->tracking_code;

        }
        else
        {
            if($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-user-info-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 1)
            {
                redirect(base_url('attorney-user-info-form'));
            }
            else
            {
                redirect(base_url('card-user-info-form'));
            }
        }
        //var_dump($data['attorney_info']);die;
        $data['title'] = 'تکمیل اطلاعات وکالت';
        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        $this->UserView('attorney/attorney_attorney_form', $data);
    }

    public function attorney_form_process()
    {
        //var_dump($this->input->post());
        //var_dump($_FILES['img']);die;
        $inputs = $this->input->post();
        $config = [
            [
                'field' => 'delegated',
                'label' => 'موکل',
                'rules' => 'trim'
            ],
            [
                'field' => 'attorney',
                'label' => 'وکیل',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'attorney_number',
                'label' => 'شماره وکالت',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'date',
                'label' => 'تاریخ وکالت',
                'rules' => 'trim|required|check_date',
                'errors' => ['check_date' => 'تاریخ وکالت اشتباه وارد شده است']
            ],
            [
                'field' => 'attorney_serial',
                'label' => 'سریال وکالت',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'office_number',
                'label' => 'شماره دفتر',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'city',
                'label' => 'شهر',
                'rules' => 'trim|required'
            ],

        ];
        $this->form_validation->set_rules($config);
        if($this->form_validation->run() == TRUE)
        {
            if($inputs['func'] == 'edit' || ( ! empty($_FILES['img']['tmp_name']) && is_uploaded_file( $_FILES['img']['tmp_name']))) {

                if ($inputs['parent_id'] != 0 || ($inputs['attorney_number'] == $this->session->userdata('logged_user')['attorney_number'] && $inputs['date'] == $this->session->userdata('logged_user')['attorney_date'])) {
                    if($inputs['func'] == 'edit')
                    {
                        if(! empty($_FILES['img']['tmp_name']))
                        {
                            $img = $this->upload_image_ftp('img', 'attorneys');
                        }
                        else
                        {
                            $result = $this->db->select('*')->from('attorneys')->where('id' , $inputs['id'])->get()->row();
                            $img = explode('/',$result->img)[1];
                        }
                    }
                    else
                    {
                        $img = $this->upload_image_ftp('img', 'attorneys');
                    }
                    if($img != FALSE) {
                        $inputs['ground_id'] = $this->session->userdata('logged_user')['id'];
                        $inputs['img'] = 'attorneys/' . $img;
                        $result = $this->M_Attorney->insert_new_attorney($inputs, 'attorney');
                        echo json_encode(['status' => TRUE, 'result' => $result]);
                    }
                    else
                    {
                        echo json_encode(['status' => FALSE, 'result' => 'تصاویر انتخابی شما مجاز نمی باشد . لطفا به حجم و نوع فایل انتخابی توجه فرمایید .']);
                    }
                }
                else
                {
                    echo json_encode(['status' => FALSE, 'result' => 'اطلاعات وکالت با اطلاعات قبلی هم خوانی ندارد']);
                }
            }
            else
            {
                echo json_encode(['status' => FALSE , 'result' => 'لطفا تمامی تصویر ها را انتخاب کنید ']);
            }
        }
        else
        {
            echo json_encode(['status' => 0 , 'result' => $this->form_validation->error_array()]);
        }

    }

    public function bills_cheques_form()
    {
        $data['guide'] = $this->db->select('*')->from('settings')->where('in_group','guide')->where('status' , 1)->where('item' , 'bill_cheque')->get()->row()->value;
        if((in_array('attorneys',explode(',',$this->session->userdata('logged_user')['access_lvl']))))
        {
            $data['bills_cheques_info'] = $this->M_BillCheque->get_bills_cheques($this->session->userdata('logged_user')['log_id']);
            //var_dump($data['bills_cheques_info']);die;
            $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row()->tracking_code;
        }
        else
        {
            if($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-document-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 1)
            {
                redirect(base_url('attorney-attorney-form'));
            }
            else
            {
                redirect(base_url('card-card-form'));
            }
        }
        $data['title'] = 'تکمیل اطلاعات مالی';
        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        $this->UserView('attorney/attorney_bills_cheques_form', $data);
    }

    public function bills_cheques_form_process()
    {

        $inputs = $this->input->post();
        $config = [
            [
                'field' => 'number',
                'label' => 'شماره',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'amount',
                'label' => 'مبلغ',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'date',
                'label' => 'تاریخ پرداخت',
                'rules' => 'trim|required|check_date',
                'errors' => ['check_date' => 'تاریخ اشتباه وارد شده است ']
            ],
            [
                'field' => 'place',
                'label' => 'محل پرداخت',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'payer_full_name',
                'label' => 'پرداخت کننده',
                'rules' => 'trim'
            ],
            [
                'field' => 'type',
                'label' => 'نوع',
                'rules' => 'trim|required'
            ],

        ];
        $this->form_validation->set_rules($config);
        if($this->form_validation->run() == TRUE) {
            $bill_cheque = $this->db->select('*')->from('bills_cheques')->where('number' , $inputs['number'])->where('type' , $inputs['type'])->where('status' , 1)->get();
            if($bill_cheque->num_rows() == 0 || $bill_cheque->row()->logged_user_id == $this->session->userdata('logged_user')['log_id']) {
                if ($inputs['func'] == 'edit' || (!empty($_FILES['img']['tmp_name']) && is_uploaded_file($_FILES['img']['tmp_name']))) {
                    if($inputs['func'] == 'edit')
                    {
                        if(! empty($_FILES['img']['tmp_name']))
                        {
                            $img = $this->upload_image_ftp('img', 'bills_cheques');
                        }
                        else
                        {
                            $result = $this->db->select('*')->from('bills_cheques')->where('id' , $inputs['id'])->get()->row();
                            $img = explode('/',$result->img)[1];
                        }
                    }
                    else
                    {
                        $img = $this->upload_image_ftp('img', 'bills_cheques');
                    }
                    if($img != FALSE)
                    {
                        $inputs['img'] = 'bills_cheques/' . $img;
                        $result = $this->M_BillCheque->insert_new_bills_cheques($inputs, 'attorney');
                        echo json_encode(['status' => TRUE, 'result' => $result]);
                    }
                    else
                    {
                        echo json_encode(['status' => FALSE, 'result' => 'تصاویر انتخابی شما مجاز نمی باشد . لطفا به حجم و نوع فایل انتخابی توجه فرمایید .']);
                    }
                }
                else
                {
                    echo json_encode(['status' => FALSE, 'result' => 'لطفا تمامی تصویر ها را انتخاب کنید']);
                }
            }
            else
            {
                echo json_encode(['status' => FALSE, 'result' => 'شماره فیش یا چک وارد شده قبلا در سیستم ثبت شده است .']);
            }
        }
        else
        {
            echo json_encode(['status' => 0 , 'result' => $this->form_validation->error_array()]);
        }
    }

}
