<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/15/2018
 * Time: 3:23 PM
 */
include APPPATH."controllers/BaseController.php";
class Login extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function attorney_login()
    {
        $web_status = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'web_status')->get()->row()->value;
        if($web_status == 0)
        {
            $this->session->set_flashdata('attorney_errors', '<p>وب سایت در حال بروزرسانی میباشد . </p>');
            redirect(base_url(''));
        }
        $this->lang->load('form_validation_lang','persian');
        $this->config->set_item('language', 'persian');
        $inputs = $this->input->post();
        $this->form_validation->set_rules('membership_number','شماره عضویت','trim|required');
        $this->form_validation->set_rules('attorney_number','شماره وکارت','trim|required');
        $this->form_validation->set_rules('attorney_date','تاریخ وکالت','trim|required');
        $this->form_validation->set_rules('bill_number','شماره فیش','trim');
        $this->form_validation->set_rules('bill_date','تاریخ فیش','trim');
        $this->form_validation->set_rules('f_name','نام','trim|required');
        $this->form_validation->set_rules('l_name','نام خانوادگی','trim|required');
        $this->form_validation->set_rules('phone_number','شماره تلفن','trim|required|Regex',['Regex' => 'شماره تلفن اشتباه است']);

        if($this->form_validation->run())
        {
            $result = $this->M_Ground->search_for_ground(toEnglishNum($inputs['membership_number']),toEnglishNum($inputs['attorney_number']),toEnglishNum($inputs['attorney_date']) , 1);

            if($result->num_rows() == 1)
            {
                if ($result->row()->tracking_code == NULL) {
                    $ground_id = $result->row()->id;
                    $logged_user = $this->M_LoggedUser->check_and_add_to_logged_users($result->row()->id,$inputs);
                    $this->session->set_userdata('logged_user',
                    [
                        'log_id' => $logged_user->id ,
                        'id' => $ground_id,
                        'membership_number' => $inputs['membership_number'],
                        'attorney_number' => $inputs['attorney_number'] ,
                        'attorney_date' => $inputs['attorney_date'] ,
                        'access_lvl'=>$logged_user->access_lvl,
                        'type' => 1 ,
                        'let_to' => 0
                    ]);
                    redirect(base_url('attorney-user-info-form'));
                } else {
                    if(isset($inputs['national_code']) && $inputs['national_code'])
                    {
                        $this->M_BlackList->check_and_add_to_black_list($result->row()->id,$inputs);
                        $this->session->set_flashdata('attorney_errors', '<p>این اطلاعات قبلا در سیستم ثبت شده است برای اطلاعات بیشتر به تعاونی مراجعه کنید . </p>');
                        $this->session->set_flashdata('attorney_inputs_history', $inputs);
                        redirect(base_url(''));
                    }
                    else
                    {
                        $this->session->set_flashdata('black_list', true);
                        $this->session->set_flashdata('attorney_errors', '<p>لطفا کد ملی خود را وارد کنید سپس تایید را بزنید . </p>');
                        $this->session->set_flashdata('attorney_inputs_history', $inputs);
                        redirect(base_url(''));
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('attorney_errors', '<p>اطلاعات وارد شده اشتباه است .</p>');
                $this->session->set_flashdata('attorney_inputs_history', $inputs);
                redirect(base_url(''));
            }
        }
        else
        {
            $this->session->set_flashdata('attorney_errors' , validation_errors());
            $this->session->set_flashdata('attorney_inputs_history' , $inputs);
            redirect(base_url(''));
        }
    }
    public function card_login()
    {
        $web_status = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'web_status')->get()->row()->value;
        if($web_status == 0)
        {
            $this->session->set_flashdata('card_errors', '<p>وب سایت در حال بروزرسانی میباشد . </p>');
            redirect(base_url(''));
        }
        $this->lang->load('form_validation_lang','persian');
        $this->config->set_item('language', 'persian');
        $inputs = $this->input->post();
        $this->form_validation->set_rules('membership_number','شماره عضویت','trim|required');
        $this->form_validation->set_rules('bill_number','شماره فیش','trim');
        $this->form_validation->set_rules('bill_date','تاریخ فیش','trim');
        $this->form_validation->set_rules('f_name','نام','trim|required');
        $this->form_validation->set_rules('l_name','نام خانوادگی','trim|required');
        $this->form_validation->set_rules('phone_number','شماره تلفن','trim|required|Regex',['Regex' => 'شماره تلفن اشتباه است']);

        if($this->form_validation->run())
        {
            $result = $this->M_Ground->search_for_ground(toEnglishNum($inputs['membership_number']), '' , '' , 2);

            if($result->num_rows() == 1)
            {
                if ($result->row()->tracking_code == NULL) {
                    $ground_id = $result->row()->id;
                    $logged_user = $this->M_LoggedUser->check_and_add_to_logged_users($result->row()->id,$inputs);
                    $this->session->set_userdata('logged_user',
                    [
                        'log_id' => $logged_user->id ,
                        'id' => $ground_id,
                        'membership_number' => $inputs['membership_number'],
                        'attorney_number' => '' ,
                        'attorney_date' => '' ,
                        'access_lvl'=>$logged_user->access_lvl,
                        'type' => 2 ,
                        'let_to' => 0
                    ]);
                    redirect(base_url('card-user-info-form'));
                } else {
                    if(isset($inputs['national_code']) && $inputs['national_code'])
                    {
                        $this->M_BlackList->check_and_add_to_black_list($result->row()->id,$inputs);
                        $this->session->set_flashdata('card_errors', '<p>این اطلاعات قبلا در سیستم ثبت شده است برای اطلاعات بیشتر به تعاونی مراجعه کنید . </p>');
                        $this->session->set_flashdata('card_inputs_history', $inputs);
                        redirect(base_url(''));
                    }
                    else
                    {
                        $this->session->set_flashdata('black_list', true);
                        $this->session->set_flashdata('card_errors', '<p>لطفا کد ملی خود را وارد کنید سپس تایید را بزنید . </p>');
                        $this->session->set_flashdata('card_inputs_history', $inputs);
                        redirect(base_url(''));
                    }

                }
            }
            else
            {
                $this->session->set_flashdata('card_errors', '<p>اطلاعات وارد شده اشتباه است .</p>');
                $this->session->set_flashdata('card_inputs_history', $inputs);
                redirect(base_url(''));
            }
        }
        else
        {
            $this->session->set_flashdata('card_errors' , validation_errors());
            $this->session->set_flashdata('card_inputs_history' , $inputs);
            redirect(base_url(''));
        }
    }
    public function document_login()
    {
        $web_status = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'web_status')->get()->row()->value;
        if($web_status == 0)
        {
            $this->session->set_flashdata('document_errors', '<p>وب سایت در حال بروزرسانی میباشد . </p>');
            redirect(base_url(''));
        }
        $this->lang->load('form_validation_lang','persian');
        $this->config->set_item('language', 'persian');
        $inputs = $this->input->post();
        $this->form_validation->set_rules('membership_number','شماره عضویت','trim|required');
        $this->form_validation->set_rules('attorney_number','شماره سند','trim|required');
        $this->form_validation->set_rules('attorney_date','تاریخ سند','trim|required');
        $this->form_validation->set_rules('bill_number','شماره فیش','trim');
        $this->form_validation->set_rules('bill_date','تاریخ فیش','trim');
        $this->form_validation->set_rules('f_name','نام','trim|required');
        $this->form_validation->set_rules('l_name','نام خانوادگی','trim|required');
        $this->form_validation->set_rules('phone_number','شماره تلفن','trim|required|Regex',['Regex' => 'شماره تلفن اشتباه است']);

        if($this->form_validation->run() == TRUE)
        {
            $result = $this->M_Ground->search_for_ground(toEnglishNum($inputs['membership_number']),toEnglishNum($inputs['attorney_number']),toEnglishNum($inputs['attorney_date']) , 0);

            if($result->num_rows() == 1)
            {
                if ($result->row()->tracking_code == NULL) {
                    $ground_id = $result->row()->id;
                    $logged_user = $this->M_LoggedUser->check_and_add_to_logged_users($result->row()->id,$inputs);
                    $this->session->set_userdata('logged_user',
                    [
                        'log_id' => $logged_user->id ,
                        'id' => $ground_id,
                        'membership_number' => $inputs['membership_number'],
                        'attorney_number' => $inputs['attorney_number'] ,
                        'attorney_date' => $inputs['attorney_date'] ,
                        'access_lvl'=>$logged_user->access_lvl,
                        'type' => 0 ,
                        'let_to' => 0
                    ]);
                    redirect(base_url('document-user-info-form'));
                } else {
                    if(isset($inputs['national_code']) && $inputs['national_code'])
                    {
                        $this->M_BlackList->check_and_add_to_black_list($result->row()->id,$inputs);
                        $this->session->set_flashdata('document_errors', '<p>این اطلاعات قبلا در سیستم ثبت شده است برای اطلاعات بیشتر به تعاونی مراجعه کنید . </p>');
                        $this->session->set_flashdata('document_inputs_history', $inputs);
                        redirect(base_url(''));
                    }
                    else
                    {
                        $this->session->set_flashdata('black_list', true);
                        $this->session->set_flashdata('document_errors', '<p>لطفا کد ملی خود را وارد کنید سپس تایید را بزنید . </p>');
                        $this->session->set_flashdata('document_inputs_history', $inputs);
                        redirect(base_url(''));
                    }

                }
            }
            else
            {
                $this->session->set_flashdata('document_errors', '<p>اطلاعات وارد شده اشتباه است .</p>');
                $this->session->set_flashdata('document_inputs_history', $inputs);
                redirect(base_url(''));
            }
        }
        else
        {
            $this->session->set_flashdata('document_errors' , validation_errors());
            $this->session->set_flashdata('document_inputs_history' , $inputs);
            redirect(base_url(''));
        }
    }
    public function login_by_userpass()
    {
        $web_status = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'web_status')->get()->row()->value;
        if($web_status == 0)
        {
            $this->session->set_flashdata('userpass_login_error', '<p>وب سایت در حال بروزرسانی میباشد . </p>');
            redirect(base_url(''));
        }
        $this->form_validation->set_rules('membership_number','شماره عضویت','trim|required');
        $this->form_validation->set_rules('password',' رمز عبور','trim|required');
        if($this->form_validation->run() == TRUE)
        {
            $membership_number = toEnglishNum($this->input->post('membership_number'));
            $password = $this->input->post('password');
            $result = $this->M_Ground->search_for_ground_by_userpass($membership_number,strtoupper($password));

            if($result->num_rows() == 1)
            {
                $ground_detail = $result->row();
                $membership_num = explode(' ',$ground_detail->membership_number);
                if(isset($membership_num[1]))
                {
                    $membership_num = $membership_num[0].$membership_num[1];
                }
                else
                {
                    $membership_num = $membership_num[0];
                }
                $arr = preg_split('/(?<=[0-9])(?=[A-Z]+)/i',$membership_num);
                if(isset($arr[1])){
                    $membership_number2 = $arr[0].' '.$arr[1];
                }
                else
                {
                    $membership_number2 = NULL;
                }
                $logged_user = $this->db->select('*')->from('logged_users')
                    ->where('ground_id' , $ground_detail->id)
                    ->group_start()
                    ->where('membership_number' ,$membership_num)
                    ->or_where('membership_number' , $membership_number2)
                    ->group_end()
                    ->where('status' , 1)
                    ->get()
                    ->row();
                if($ground_detail->solved == 1)
                {
                    $let_to = 'solved';
                }
                elseif($ground_detail->msg_to_user == 1)
                {
                    $let_to = 'msg_to_user';
                }
                elseif ($ground_detail->let_to_gold == 1)
                {
                    $let_to = 'let_to_gold';
                }
                elseif ($ground_detail->let_to_silver == 1)
                {
                    $let_to = 'let_to_silver';
                }
                else
                {
                    $let_to = 0;
                }
                $this->session->set_userdata('logged_user',
                    [
                        'log_id' => $logged_user->id ,
                        'id' => $ground_detail->id,
                        'membership_number' => $ground_detail->membership_number,
                        'attorney_number' => $ground_detail->attorney_number ,
                        'attorney_date' => $ground_detail->attorney_date ,
                        'access_lvl'=>$logged_user->access_lvl,
                        'type' => $ground_detail->type,
                        'let_to' => $let_to
                    ]);
                if ($ground_detail->type == 0) {
                    redirect(base_url('document-user-info-form'));
                } elseif ($ground_detail->type == 1) {
                    redirect(base_url('attorney-user-info-form'));
                } elseif ($ground_detail->type == 2) {
                    redirect(base_url('card-user-info-form'));
                }
            }
            else
            {
                $this->session->set_flashdata('userpass_login_error', '<p>اطلاعات وارد شده اشتباه است .</p>');
                redirect(base_url(''));
            }
        }
        else
        {
            $this->session->set_flashdata('userpass_login_error' , '<p>شماره عضویت و رمز عبور باید پر شود .</p>');
            redirect(base_url(''));
        }

    }

    public function log_out()
    {
        $this->session->unset_userdata('logged_user');
        redirect(base_url(''));
    }
}