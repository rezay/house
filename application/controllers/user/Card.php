<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/15/2018
 * Time: 3:23 PM
 */
include APPPATH."controllers/BaseController.php";
class Card extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        if( ! isset($this->session->userdata('logged_user')['id'])){
            redirect(base_url(''));
        }
//var_dump($this->session->userdata('logged_user'));die;
        if($this->session->userdata('logged_user')['type'] != 2)
        {

            if($this->session->userdata('logged_user')['type'] == 1)
            {
                redirect(base_url('attorney-user-info-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-user-info-form'));
            }
            else
            {
                redirect(base_url());
            }
        }
        else
        {

            if($this->session->userdata('logged_user')['let_to'] === 'let_to_silver' || $this->session->userdata('logged_user')['let_to'] === 'let_to_gold')
            {
                redirect(base_url('debit-page'));
            }
            elseif ($this->session->userdata('logged_user')['let_to'] === 'solved' || $this->session->userdata('logged_user')['let_to'] === 'msg_to_user')
            {
                redirect(base_url('solved-page'));
            }
        }

        $exist = $this->db->select('id')->from('logged_users')->where('status',1)->where('id' , $this->session->userdata('logged_user')['log_id'])->get()->num_rows();
        if($exist == 0)
        {
            redirect(base_url('log-out'));
        }
        $this->lang->load('form_validation_lang','persian');
        $this->config->set_item('language', 'persian');
    }
    public function user_info_form()
    {
        $data['guide'] = $this->db->select('*')->from('settings')->where('in_group','guide')->where('status' , 1)->where('item' , 'user_info')->get()->row()->value;
        if(in_array('basic',explode(',',$this->session->userdata('logged_user')['access_lvl'])))
        {
            $data['user_info'] = $this->M_UserMeta->get_users_info();
            $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row()->tracking_code;
        }
        else
        {
            if($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-user-info-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 1)
            {
                redirect(base_url('attorney-user-info-form'));
            }
            else
            {
                redirect(base_url('card-user-info-form'));
            }
        }
        $data['title'] = 'تکمیل اطلاعات شرکا';
        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        //var_dump($this->session->userdata('logged_user'));die;
        $this->UserView('card/card_users_info_form',$data);

    }

    public function user_info_form_process()
    {
        $inputs = $this->input->post();
        $config = [
            [
                'field' => 'first_name',
                'label' => 'نام',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'last_name',
                'label' => 'نام خانوادی',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'father_name',
                'label' => 'نام پدر',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'birth_certif_number',
                'label' => 'شماره شناسنامه',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'national_code',
                'label' => 'کد ملی',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'birth_date',
                'label' => 'تاریخ تولد',
                'rules' => 'trim|required|check_date',
                'errors' => ['check_date' => 'تاریخ به شکل درستی وارد نشده است']
            ],
            [
                'field' => 'address',
                'label' => 'ادرس',
                'rules' => 'trim|required|max_length[500]'
            ],
            [
                'field' => 'postal_code',
                'label' => 'کد پستی',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'home_number',
                'label' => 'شماره ثابت',
                'rules' => 'trim|required|Regex_home_number',
                'errors' => ['Regex_home_number' => 'شماره ثابت را به صورت صحیح وارد کنید']
            ],
            [
                'field' => 'phone_number',
                'label' => 'شماره تلفن',
                'rules' => 'trim|required|Regex',
                'errors' => ['Regex' => 'شماره تلفن وارد شده اشتباه است']
            ]
        ];
        $this->form_validation->set_rules($config);
        if($this->form_validation->run() == TRUE)
        {
            if($inputs['func'] == 'edit' || ( ! empty($_FILES['birth_certificate_img']['tmp_name']) && is_uploaded_file( $_FILES['birth_certificate_img']['tmp_name']) && ! empty($_FILES['national_card_img']['tmp_name']) && is_uploaded_file( $_FILES['national_card_img']['tmp_name'])) )
            {
                if($inputs['func'] == 'edit')
                {
                    $result = $this->db->select('*')->from('user_meta')->where('user_id' , $inputs['user_id'])->get()->row();
                    if(! empty($_FILES['birth_certificate_img']['tmp_name']))
                    {
                        $img1 = $this->upload_image_ftp('birth_certificate_img', 'user_meta');
                    }
                    else
                    {
                        $img1 = explode('/' ,$result->birth_certificate_img)[1];
                    }
                    if(! empty($_FILES['national_card_img']['tmp_name']))
                    {
                        $img2 = $this->upload_image_ftp('national_card_img', 'user_meta');
                    }
                    else
                    {
                        $img2 = explode('/' ,$result->national_card_img)[1];
                    }
                }
                else
                {
                    $img1 = $this->upload_image_ftp('birth_certificate_img', 'user_meta');
                    $img2 = $this->upload_image_ftp('national_card_img', 'user_meta');
                }
                if(($img1 != FALSE && $img2 != FALSE ))
                {
                    $inputs['birth_certificate_img'] = 'user_meta/' . $img1;
                    $inputs['national_card_img'] = 'user_meta/' . $img2;
                    $result = $this->M_UserMeta->insert_new_user($inputs, 'card');
                    echo json_encode(['status' => TRUE, 'result' => $result]);
                }
                else
                {
                    echo json_encode(['status' => FALSE, 'result' => 'تصاویر انتخابی شما مجاز نمی باشد . لطفا به حجم و نوع فایل انتخابی توجه فرمایید .']);
                }

            }
            else
            {
                echo json_encode(['status' => FALSE, 'result' => 'لطفا همه تصویر ها را پر کنید .']);
            }
        }
        else
        {
            echo json_encode(['status' => 0, 'result' => $this->form_validation->error_array()]);
        }
    }

    public function bills_cheques_form()
    {
        //var_dump($this->session->userdata('logged_user'));
        $data['guide'] = $this->db->select('*')->from('settings')->where('in_group','guide')->where('status' , 1)->where('item' , 'bill_cheque')->get()->row()->value;
        if(in_array('card',explode(',',$this->session->userdata('logged_user')['access_lvl'])))
        {
            $data['bills_cheques_info'] = $this->M_BillCheque->get_bills_cheques($this->session->userdata('logged_user')['log_id']);
            //var_dump($data['bills_cheques_info']);die;
            $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row()->tracking_code;
        }
        else
        {
            if($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-document-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 1)
            {
                redirect(base_url('attorney-document-form'));
            }
            else
            {
                redirect(base_url('card-card-form'));
            }
        }
        $data['title'] = 'تکمیل اطلاعات مالی';
        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        $this->UserView('card/card_bills_cheques_form', $data);
    }

    public function bills_cheques_form_process()
    {

        $inputs = $this->input->post();
        $config = [
            [
                'field' => 'number',
                'label' => 'شماره',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'amount',
                'label' => 'مبلغ',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'date',
                'label' => 'تاریخ پرداخت',
                'rules' => 'trim|required|check_date',
                'errors' => ['check_date' => 'تاریخ اشتباه وارد شده است ']
            ],
            [
                'field' => 'place',
                'label' => 'محل پرداخت',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'payer_full_name',
                'label' => 'پرداخت کننده',
                'rules' => 'trim'
            ],
            [
                'field' => 'type',
                'label' => 'نوع',
                'rules' => 'trim|required'
            ],

        ];
        $this->form_validation->set_rules($config);
        if($this->form_validation->run() == TRUE) {
            $bill_cheque = $this->db->select('*')->from('bills_cheques')->where('number' , $inputs['number'])->where('type' , $inputs['type'])->where('status' , 1)->get();
            if($bill_cheque->num_rows() == 0 || $bill_cheque->row()->logged_user_id == $this->session->userdata('logged_user')['log_id']) {
                if ($inputs['func'] == 'edit' || (!empty($_FILES['img']['tmp_name']) && is_uploaded_file($_FILES['img']['tmp_name']))) {
                    if($inputs['func'] == 'edit')
                    {
                        if(! empty($_FILES['img']['tmp_name']))
                        {
                            $img = $this->upload_image_ftp('img', 'bills_cheques');
                        }
                        else
                        {
                            $result = $this->db->select('*')->from('bills_cheques')->where('id' , $inputs['id'])->get()->row();
                            $img = explode('/',$result->img)[1];
                        }
                    }
                    else
                    {
                        $img = $this->upload_image_ftp('img', 'bills_cheques');
                    }
                    if($img != FALSE)
                    {
                        $inputs['img'] = 'bills_cheques/' . $img;
                        $result = $this->M_BillCheque->insert_new_bills_cheques($inputs, 'card');
                        echo json_encode(['status' => TRUE, 'result' => $result]);
                    }
                    else
                    {
                        echo json_encode(['status' => FALSE, 'result' => 'تصاویر انتخابی شما مجاز نمی باشد . لطفا به حجم و نوع فایل انتخابی توجه فرمایید .']);
                    }
                }
                else
                {
                    echo json_encode(['status' => FALSE, 'result' => 'لطفا تمامی تصویر ها را انتخاب کنید']);
                }
            }
            else
            {
                echo json_encode(['status' => FALSE, 'result' => 'شماره فیش یا چک وارد شده قبلا در سیستم ثبت شده است .']);
            }
        }
        else
        {
            echo json_encode(['status' => 0 , 'result' => $this->form_validation->error_array()]);
        }
    }
    public function card_form()
    {
        $data['guide'] = $this->db->select('*')->from('settings')->where('in_group','guide')->where('status' , 1)->where('item' , 'card_info')->get()->row()->value;
        if(in_array('users',explode(',',$this->session->userdata('logged_user')['access_lvl'])))
        {
            $data['card_info'] = $this->M_Card->get_card();
            //var_dump($data['card_info']);die;
            $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row()->tracking_code;
        }
        else
        {
            if($this->session->userdata('logged_user')['type'] == 0)
            {
                redirect(base_url('document-document-form'));
            }
            elseif($this->session->userdata('logged_user')['type'] == 1)
            {
                redirect(base_url('attorney-document-form'));
            }
            else
            {
                redirect(base_url('card-user-info-form'));
            }
        }
        $data['title'] = 'تکمیل اطلاعات مالی';
        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        $this->UserView('card/card_card_form', $data);
    }

    public function card_form_process()
    {

        $inputs = $this->input->post();
        $config = [
            [
                'field' => 'card_color',
                'label' => 'رنگ کارت',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'bought_from',
                'label' => 'خریداری شده از',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'bought_from_person',
                'label' => 'نام و نام خانوادگی شخص',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'card_named',
                'label' => 'کارت بنام شده',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'signatured',
                'label' => 'کارت امضا شده',
                'rules' => 'trim'
            ],

        ];
        $this->form_validation->set_rules($config);
        if($this->form_validation->run() == TRUE) {
            if ($inputs['func'] == 'edit' || (!empty($_FILES['card_front_img']['tmp_name']) && is_uploaded_file($_FILES['card_front_img']['tmp_name']) && !empty($_FILES['card_back_img']['tmp_name']) && is_uploaded_file($_FILES['card_back_img']['tmp_name']))) {
                if($inputs['func'] == 'edit')
                {
                    $result = $this->db->select('*')->from('cards')->where('id' , $inputs['id'])->get()->row();
                    if(! empty($_FILES['card_front_img']['tmp_name']))
                    {
                        $card_front_img = $this->upload_image_ftp('card_front_img', 'card');
                    }
                    else
                    {
                        $card_front_img = explode('/' ,$result->card_front_img)[1];
                    }
                    if(! empty($_FILES['card_back_img']['tmp_name']))
                    {
                        $card_back_img = $this->upload_image_ftp('card_back_img', 'card');
                    }
                    else
                    {
                        $card_back_img = explode('/' ,$result->card_back_img)[1];
                    }
                }
                else
                {
                    $card_front_img = $this->upload_image_ftp('card_front_img', 'card');
                    $card_back_img = $this->upload_image_ftp('card_back_img', 'card');
                }
                if(($card_front_img != FALSE && $card_back_img != FALSE ))
                {
                    $inputs['card_front_img'] = 'card/' . $card_front_img;
                    $inputs['card_back_img'] = 'card/' . $card_back_img;
                    $result = $this->M_Card->insert_new_card($inputs, 'card');
                    echo json_encode(['status' => TRUE, 'result' => $result]);
                }
                else
                {
                    echo json_encode(['status' => FALSE, 'result' => 'تصاویر انتخابی شما مجاز نمی باشد . لطفا به حجم و نوع فایل انتخابی توجه فرمایید .']);
                }

            }
            else
            {
                echo json_encode(['status' => FALSE, 'result' => 'لطفا تمامی تصویر ها را انتخاب کنید']);
            }
        }
        else
        {
            echo json_encode(['status' => 0 , 'result' => $this->form_validation->error_array()]);
        }
    }

}
