<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 3:54 PM
 */
include 'BaseController.php';
class Basic extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($this->session->userdata('logged_user')['id'])
        {
            redirect(base_url('document-user-info-form'));
        }
        $data['title'] = 'صفحه اصلی';
        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        $this->UserView('home',$data);
    }

    public function check_share_amount()
    {
        $ids = json_decode($this->input->post('ids'));
        echo $this->db->select_sum('share_amount')->where_in('user_id' , $ids)->get('user_meta')->row()->share_amount;
    }

    public function enable_status()
    {
        $table = $this->input->post('table');
        $user_ids = $this->input->post('user_ids');
        //var_dump($table.'_'.json_decode($user_ids));
        if($table == 'user_meta')
        {
            $condition = 'user_id';
            $access_lvl = 'users';
        }
        elseif($table == 'attorneys')
        {
            $condition = 'id';
            $access_lvl = 'attorneys';
        }
        elseif($table == 'bills_cheques')
        {
            $condition = 'id';
            $access_lvl = 'bills_cheques';
        }
        elseif($table == 'documents')
        {
            $condition = 'id';
            $access_lvl = 'documents';
        }
        elseif($table == 'certain_documents')
        {
            $condition = 'id';
            $access_lvl = 'certain_documents';
        }
        elseif($table == 'cards')
        {
            $condition = 'id';
            $access_lvl = 'card';
        }
        $user_ids = json_decode($user_ids);
        array_push($user_ids , '*');
		$this->db->set('status' , 1)->where_in($condition , $user_ids)->update($table);

        if( ! in_array($access_lvl,explode(',',$this->session->userdata('logged_user')['access_lvl']))) {
            $session = $this->session->userdata('logged_user');
            $session['access_lvl'] .= ',' . $access_lvl;
            $this->session->set_userdata('logged_user', $session);
            $this->db->set('access_lvl', $session['access_lvl'])->where('id', $this->session->userdata('logged_user')['log_id'])->update('logged_users');
        }
    }

    public function delete_records()
    {
        $table = $this->input->post('table');
        $user_ids = $this->input->post('user_ids');
        if($table == 'user_meta')
        {
            $condition = 'user_id';
        }
        else
        {
            $condition = 'id';
        }
        $user_ids = json_decode($user_ids);
        array_push($user_ids , '*');

		$this->db->where_in($condition , $user_ids)->delete($table);

    }

    public function password_reset()
    {
        $phone_number = $this->input->post('phone_number');
        $result = $this->db->select('*')->from('logged_users')->where('phone_number' , $phone_number)->where('status' , 1)->get();
        if($result->num_rows() > 0)
        {
            $list = str_split('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
            $user_password = '';
            for ($i = 0; $i < 10; $i++)
            {
                $user_password .= $list[mt_rand(0, count($list)-1)];
            }
            $this->db->set('password' , md5($user_password))->where('phone_number' , $result->row()->phone_number)->update('grounds');
            //sms
            Send($user_password.'رمز عبور جدید شما در سامانه تعاونی مسکن' , $phone_number,'',FALSE);
            echo $user_password;
        }
        else
        {
            echo 0;
        }
    }

    public function success_page()
    {
        if( ! isset($this->session->userdata('logged_user')['id']))
        {
            redirect(base_url(''));
        }
        if($this->session->userdata('logged_user')['let_to'] === 'let_to_silver' || $this->session->userdata('logged_user')['let_to'] === 'let_to_gold')
        {
            redirect(base_url('debit-page'));
        }
        elseif ($this->session->userdata('logged_user')['let_to'] === 'solved' || $this->session->userdata('logged_user')['let_to'] === 'msg_to_user')
        {
            redirect(base_url('solved-page'));
        }
        if($this->input->post('description'))
        {
            $this->db->set('description' , $this->input->post('description'))->where('id' , $this->session->userdata('logged_user')['id'])->update('grounds');
        }
        $exist = $this->db->select('id')->from('logged_users')->where('status',1)->where('id' , $this->session->userdata('logged_user')['log_id'])->get()->num_rows();
        if($exist == 0)
        {
            redirect(base_url('log-out'));
        }
        $ground = $this->db->select('*')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row();
        if($ground->tracking_code == NULL )
        {
            $access_lvl = $this->session->userdata('logged_user')['access_lvl'];
            if ($this->session->userdata('logged_user')['type'] == 0) {
                $pre_tracking_code = 10200;
                $type = 0;
                $needed_access_lvl = ['basic','users', 'attorneys', 'certain_documents','documents'];
            } elseif ($this->session->userdata('logged_user')['type'] == 1) {
                $pre_tracking_code = 10500;
                $type = 1;
                $needed_access_lvl = ['basic','users', 'attorneys'];
            } else {
                $pre_tracking_code = 10800;
                $type = 2;
                $needed_access_lvl = ['basic','users', 'card'];
            }
            if (count(array_intersect($needed_access_lvl, explode(',', $access_lvl))) == count($needed_access_lvl)) {

                // create tracking_code
                $biggest_tracking_code = $this->db->select('tracking_code')->from('grounds')->where('type', $type)->order_by('tracking_code', 'DESC')->limit(1)->get()->row();
                if ($biggest_tracking_code->tracking_code == NULL) {
                    $tracking_code = (int)$pre_tracking_code . '00001';
                } else {

                    $tracking_code = (int)$biggest_tracking_code->tracking_code + 1;
                }

                $logged_user = $this->db->select('*')->from('logged_users')->where('status' ,1)->where('id' , $this->session->userdata('logged_user')['log_id'])->get()->row();
                $this->M_LoggedUser->move_logged_users_to_black_list();
                $this->db->set(['full_name' => $logged_user->full_name ,'bill_number' => $logged_user->bill_number , 'bill_date' => $logged_user->bill_date,'phone_number' => $logged_user->phone_number ,'tracking_code'=> (int)$tracking_code])->where('id', $this->session->userdata('logged_user')['id'])->update('grounds');

                // send sms
                $message = $this->db->select('value')->from('settings')->where('in_group' , 'sms')->where('item' , 'lvl1')->get()->row()->value;
                $phone_number = $this->db->select('phone_number')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row()->phone_number;

                if($ground->password == NULL)
                {
                    $list = str_split('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
                    $user_password = '';
                    for ($i = 0; $i < 10; $i++)
                    {
                        $user_password .= $list[mt_rand(0, count($list)-1)];
                    }
                    $data['user_password'] = '1';
                    $this->db->set(['password' => md5($user_password) , 'type' => $this->session->userdata('logged_user')['type']])->where('id' , $this->session->userdata('logged_user')['id'])->update('grounds');
                    $password_message = 'رمز عبور شما در سیستم تعاونی برق '.$user_password;
                    send($password_message , $phone_number , '' , FALSE);
                }
                send($message , $phone_number,'',FALSE);

            } else {
                if ($type == 0) {
                    redirect(base_url('document-bills-cheques-form'));
                } elseif ($type == 1) {
                    redirect(base_url('attorney-bills-cheques-form'));
                } else {
                    redirect(base_url('card-bills-cheques-form'));
                }
            }
        }
        $data['ground'] = $this->db->select('*')->from('grounds')->where('id' , $this->session->userdata('logged_user')['id'])->get()->row();
        $attorney_count = $this->db->where('status' , 1)->where('parent_id !=' , 0)->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->count_all_results('attorneys');
        $data['ground']->tracking_code = substr($data['ground']->tracking_code , 0 , 3).sprintf("%02d", $attorney_count).substr($data['ground']->tracking_code , 5 ,5);

        $data['title'] = 'دریافت کد رهگیری';
        $settings = $this->M_Setting->get_home_setting();
        foreach ($settings as $setting) {
            switch ($setting->item) {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        $this->UserView('success_page', $data);

    }

    public function print_page()
    {
        if( ! isset($this->session->userdata('logged_user')['id']))
        {
            redirect(base_url(''));
        }
        $data['title'] = 'پرینت اطلاعات';
        $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id', $this->session->userdata('logged_user')['id'])->get()->row()->tracking_code;
        $attorney_count = $this->db->where('status' , 1)->where('parent_id !=' , 0)->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->count_all_results('attorneys');
        $data['tracking_code'] = substr($data['tracking_code'] , 0 , 3).sprintf("%02d", $attorney_count).substr($data['tracking_code'] , 5 ,5);

        if($this->session->userdata('logged_user')['let_to'] === 'let_to_silver')
        {
            $debit = $this->db->select('*')->from('debits')->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('type' , 0)->get();
            if($debit->num_rows() > 0) {
                $payments = $this->db->select('priority')->from('payments')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('debit_id', $debit->row()->id)->where('priority !=', NULL)->where('status', 1)->get();
                if ($payments->num_rows() > 0)
                {
                    $data['priority'] = $payments->row()->priority;
                }
                else
                {
                    $data['priority'] = '';
                }

                if ($debit->row()->payed == 1) {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                    $silver_debit_payed = $debit->row()->debit_amount-$debit->row()->accepted_payment;
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount+$silver_debit_payed;
                    }
                    else
                    {
                        $data['total_payments'] = 0+$silver_debit_payed;
                    }
                } else {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('status', 1)->get('bills_cheques')->row()->amount;
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount;
                    }
                    else
                    {
                        $data['total_payments'] = 0;
                    }
                }
            }
            else
            {
                $data['priority'] = '';
                $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                if($total_payments->row()->amount != NULL)
                {
                    $data['total_payments'] = $total_payments->row()->amount;
                }
                else
                {
                    $data['total_payments'] = 0;
                }
            }
        }
        elseif ($this->session->userdata('logged_user')['let_to'] === 'let_to_gold')
        {
            $silver_debit = $this->db->select('*')->from('debits')->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('type' , 0)->get();
            $gold_debit = $this->db->select('*')->from('debits')->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('type' , 1)->get();
            if($gold_debit->num_rows() > 0) {
                $payments = $this->db->select('priority')->from('payments')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('debit_id', $gold_debit->row()->id)->where('priority !=', NULL)->where('status', 1)->get();
                if ($payments->num_rows() > 0)
                {
                    $data['priority'] = $payments->row()->priority;
                }
                else
                {
                    $data['priority'] = '';
                }
                //var_dump($data['priority']);die;
                if ($gold_debit->row()->payed == 1) {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount+$silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment+$gold_debit->row()->debit_amount;
                    }
                    else
                    {
                        $data['total_payments'] = $silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment+$gold_debit->row()->debit_amount;
                    }
                } else {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('status', 1)->get('bills_cheques');
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount+$silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment;
                    }
                    else
                    {
                        $data['total_payments'] = $silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment;
                    }
                }
            }
            else
            {
                $data['priority'] = '';
                $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                if($total_payments->row()->amount != NULL)
                {
                    $data['total_payments'] = $total_payments->row()->amount;
                }
                else
                {
                    $data['total_payments'] = 0;
                }
            }
        }
        else
        {
            $data['priority'] = '';
            $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $this->session->userdata('logged_user')['log_id'])->where('status', 1)->get('bills_cheques');

            if($total_payments->row()->amount != NULL)
            {
                $data['total_payments'] = $total_payments->row()->amount;
            }
            else
            {
                $data['total_payments'] = 0;
            }
        }
        $settings = $this->M_Setting->get_home_setting();
        foreach ($settings as $setting) {
            switch ($setting->item) {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
            }
        }
        $data['membership_number'] = $this->session->userdata('logged_user')['membership_number'];
        if($this->session->userdata('logged_user')['type'] == 2)
        {
            $data['type'] = 'کارتی';
            $data['full_info_user'] = $this->M_Ground->get_full_data_card($this->session->userdata('logged_user')['id']);
            //var_dump($data['full_info_user']);die;
        }
        elseif($this->session->userdata('logged_user')['type'] == 1)
        {
            $data['type'] = 'وکالتی';
            $data['full_info_user'] = $this->M_Ground->get_full_data_attorney($this->session->userdata('logged_user')['id']);
        }
        else
        {
            $data['type'] = 'سند دار';
            $data['full_info_user'] = $this->M_Ground->get_full_data_document($this->session->userdata('logged_user')['id']);
        }
        //var_dump($data['full_info_user']);die;
        $this->UserView('print',$data);
    }

    public function solved_page()
    {
        if( ! isset($this->session->userdata('logged_user')['id']))
        {
            redirect(base_url(''));
        }

        if($this->session->userdata('logged_user')['let_to'] !== 'solved' && $this->session->userdata('logged_user')['let_to'] !== 'msg_to_user')
        {
//            var_dump($this->session->userdata('logged_user'));
            if($this->session->userdata('logged_user')['let_to'] === 'let_to_silver' || $this->session->userdata('logged_user')['let_to'] === 'let_to_gold')
            {
                redirect(base_url('debit-page'));
            }
            else
            {
                if($this->session->userdata('logged_user')['type'] == 1)
                {
                    redirect(base_url('attorney-user-info-form'));
                }
                elseif($this->session->userdata('logged_user')['type'] == 2)
                {
                    redirect(base_url('card-user-info-form'));
                }
                elseif($this->session->userdata('logged_user')['type'] == 0)
                {
                    redirect(base_url('document-user-info-form'));
                }
                else
                {
                    redirect(base_url());
                }
            }
        }

        $data['title'] = 'صفحه اطلاع رسانی';
        $settings = $this->M_Setting->get_home_setting();
        foreach ($settings as $setting) {
            switch ($setting->item) {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        $data['message'] = 'شما با سیستم تصویه حساب کرده اید .';
        $this->UserView('solved_page',$data);
    }

    public function debit_page()
    {
        if( ! isset($this->session->userdata('logged_user')['id']))
        {
            redirect(base_url(''));
        }
        if($this->session->userdata('logged_user')['let_to'] !== 'let_to_silver' && $this->session->userdata('logged_user')['let_to'] !== 'let_to_gold')
        {
            if($this->session->userdata('logged_user')['let_to'] === 'msg_to_user' || $this->session->userdata('logged_user')['let_to'] === 'solved')
            {
                redirect(base_url('solved-page'));
            }
            else
            {
                if($this->session->userdata('logged_user')['type'] == 1)
                {
                    redirect(base_url('attorney-user-info-form'));
                }
                elseif($this->session->userdata('logged_user')['type'] == 2)
                {
                    redirect(base_url('card-user-info-form'));
                }
                elseif($this->session->userdata('logged_user')['type'] == 0)
                {
                    redirect(base_url('document-user-info-form'));
                }
                else
                {
                    redirect(base_url());
                }
            }
        }
        $data['title'] = 'صفحه تصویه بدهی';
        $settings = $this->M_Setting->get_home_setting();
        foreach ($settings as $setting) {
            switch ($setting->item) {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
                case 'emam_img' :
                    $data['emam_img'] = $setting->value;
                    break;
            }
        }
        if($this->session->userdata('logged_user')['let_to'] === 'let_to_silver')
        {
            $data['debit'] = $this->db->select('*')->from('debits')->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('type' , 0)->get()->row();
        }
        else
        {
            $data['debit'] = $this->db->select('*')->from('debits')->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('type' , 1)->get()->row();
        }
        $this->UserView('debit_page',$data);
    }

    public function pay()
    {
        if( ! isset($this->session->userdata('logged_user')['id']))
        {
            redirect(base_url(''));
        }

        if( ! $this->input->post('type') || ! in_array($this->input->post('type') , ['0','1']))
            redirect(base_url('debit-page'));
        $this->load->library('zarinpal');
        $debit = $this->db->select('*')->from('debits')->where('ground_id',$this->session->userdata('logged_user')['id'])->where('type',$this->input->post('type'))->get()->row();
        $payment =
            [
                'logged_user_id' => $this->session->userdata('logged_user')['log_id'],
                'debit_id' => $debit->id,
                'success' => NULL,
            ];
        $payment_id = $this->db->insert('payments' , $payment);

        if($debit->type == 0)
        {
            $debit_amount = $debit->debit_amount-$debit->accepted_payment;
        }
        else
        {
            $debit_amount = $debit->debit_amount;
        }
        //******************************
        //bank
        error_reporting(0);
        include_once(APPPATH.'third_party/bank/config.php');
        //include_once(APPPATH.'third_party/bank/assets/sessions.php');
        include_once(APPPATH.'third_party/bank/assets/nusoap.new.php');

        /**
         * default params for different actions
         */
        //$__AMT = !empty($_POST['AMOUNT']) ? $_POST['AMOUNT'] : null;
        $__AMT = $debit_amount;
        $__CRN = !empty($_POST['CRN']) ? $_POST['CRN'] : time();
        $__TRN = !empty($_POST['TRN']) ? $_POST['TRN'] : null;
        $__RESCODE = !empty($_POST['RESCODE']) ? $_POST['RESCODE'] : null;
        $__MID = !empty($__MID) ? $__MID : null;
        $__TID = !empty($__TID) ? $__TID : null;

        /**
         * Referral Address.Insert your url address here.
         */
        $protocol = ((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off")) ? "https" : "http";
        $__REFADD = $protocol . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; //"http://mabna.creditcard/efartakIPG-plus/e-payment/index.php";

        /**
         * You're not allowed to do any operations
         */
        // if (is_null($__AMT) || is_null($__MID) || is_null($__TID) || is_null($__PRIVATE_KEY)) {
        //     echo $___ERROR_NOT_ALLOWED;
        //     die();
        // }

        /**
         * verify action
         */
        if (!empty($__RESCODE) && !is_null($__RESCODE)) {
            require APPPATH."third_party/bank/assets/verify.php";
        }

        /**
         * IPG payment action
         */
        else {
            if(isset($_POST['SUBMIT']))
                require APPPATH."third_party/bank/assets/do.php";
            else
                require APPPATH."third_party/bank/assets/manual-verify.php";
        }

    }

    public function after_payment()
    {
        $data['title'] = 'نتیجه پرداخت';
        $this->load->helper('cookie');
        $this->load->library('zarinpal');
        $merchant_id = "b4bfa6ec-dd90-11e7-b7da-000c295eb8fc";
        $gets = $this->input->get();
        $authority = $this->zarinpal->getAuthority();

        if(!$authority)
            $authority = $gets['Authority'];

        $online_payment = $this->db->select('*')->from('payments')->where('tracking_code',$authority)->get()->row();


        if($online_payment->tracking_code != $authority)
        {
            return 'false';
        }

        $debit = $this->db->select('*')->from('debits')->where('id' , $online_payment->debit_id)->get()->row();
        if($debit->type == 0)
        {
            $payments = $this->db->select('priority')
                ->from('debits')
                ->join('payments','debits.id=payments.debit_id')
                ->where('debits.type' , 0)
                ->where('debits.status' , 1)
                ->where('payments.priority !=' , NULL)
                ->order_by('payments.priority' , 'DESK')
                ->limit(1)
                ->get();
            if($payments->num_rows() > 0)
            {
                $biggest_priority = $payments->row()->priority;
            }
            else
            {
                $biggest_priority = 100000;
            }
            $final_price = $debit->debit_amount - $debit->accepted_payment;
        }
        else
        {
            $payments = $this->db->select('priority')
                ->from('debits')
                ->join('payments','debits.id=payments.debit_id')
                ->where('debits.type' , 1)
                ->where('debits.status' , 1)
                ->where('payments.priority !=' , NULL)
                ->order_by('payments.priority' , 'DESK')
                ->limit(1)
                ->get();
            if($payments->num_rows() > 0)
            {
                $biggest_priority = $payments->row()->priority;
            }
            else
            {
                $biggest_priority = 100000;
            }
            $final_price = $debit->debit_amount;
        }

        if($_GET['Status'] == 'OK')
        {
            if($this->zarinpal->verify($merchant_id , $final_price, $authority))
            {
                $ref_id = $this->zarinpal->getRefId();
                $priority = $biggest_priority+1;
                $this->db->set(['ref_if'=>$ref_id , 'success' => 1 , 'priority' =>$priority])->where('id',$online_payment->id)->update('payments');

                $data['home'] = TRUE;

                $this->UserView('frontend/after_payment',$data);
            }
            else
            {
                $error = $this->zarinpal->getError();
                $this->db->where('id',$online_payment->id)->set('success',0)->update('payments');
                $data['message'] = $error;
                $data['home'] = TRUE;

                $this->UserView('frontend/after_payment',$data);
            }
        }
        else{
            $this->db->where('id',$online_payment->id)->set('success',0)->update('payments');
            $data['message'] = 'پرداخت با مشکل مواجه شد .';
            $data['home'] = TRUE;

            $this->UserView('frontend/after_payment',$data);
        }

    }
}
