<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 3:57 PM
 */
class BaseController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Attorney');
        $this->load->model('M_BillCheque');
        $this->load->model('M_Document');
        $this->load->model('M_Ground');
        $this->load->model('M_UserMeta');
        $this->load->model('M_CertainDocument');
        $this->load->model('M_Admin');
        $this->load->model('M_Setting');
        $this->load->model('M_BlackList');
        $this->load->model('M_Pelak');
        $this->load->model('M_LoggedUser');
        $this->load->model('M_Card');
        $this->load->model('M_AttorneyType');
		$this->load->model('M_PaymentTitle');
		$this->load->model('M_BuildState');
    }

    public function login()
    {
        if(isset($this->session->userdata('logged_admin')['id'])){
            redirect(base_url('admin-dashboard'));
        }
        $data['page_title']        = 'ورود به پنل مدیریت';
        $data['title']             = 'مسکن';
        $data['name']              = $this->M_Setting->get_site_title();

        $this->AdminView('login' , $data );
    }

    public function logout()
    {
        $this->session->unset_userdata('logged_admin');
        redirect(base_url('admin-dashboard/login'));
    }

    public function isMustBeLogin()
    {
        $id = $this->session->userdata('logged_admin')['id'];
        if(!$id || $id == null)
            redirect(base_url('admin-dashboard/login'));
        else
            return;
    }

    public function valid_access($valids)
    {
        $id = $this->session->userdata('logged_admin')['access_lvl'];
        if(in_array($id , $valids))
            return true;
        else
            redirect(base_url('admin-dashboard'));
    }

    public function check_user_pass_login()
    {
        $posts = $this->input->post();
        if(!$posts)
            return;
        $result = $this->M_Admin->is_admin($posts['username'],$posts['pass']);

        if(empty($result))
        {
            echo 'false';
            return ;
        }
        else
        {
            $login_session=[
                'id'          => $result->id,
                'fname'       => $result->fname,
                'lname'       => $result->lname,
                'username'    => $result->username,
                'access_lvl'  => $result->access_lvl
            ];
            $this->session->set_userdata('logged_admin',$login_session);

            echo 'ok';
            return ;
        }
    }

    public function upload_image_ftp($inputname , $folder )
    {

        $this->load->library('ftp');
        $path = realpath(APPPATH . '../assets/backend/images/'.$folder);
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '300';
        $config['overwrite']     = TRUE;
        $config['file_name']     = uniqid();
        $config['remove_spaces'] = TRUE;


        $this->upload->initialize($config);

        if($this->upload->do_upload($inputname))
        {
            $data = $this->upload->data();
            $source = $path."/".$data['file_name'];

            $config['hostname'] = 'ftp://upload.sjtmkbt.ir';
            $config['username'] = 'uploadsjtmkbt';
            $config['password'] = '9m4ex1ca0bur';
            $config['port']     = 21;
            $config['passive']  = FALSE;
            $config['debug']    = TRUE;

            $this->ftp->connect($config);

            $this->ftp->upload($source , '/public_html/images/'.$folder.'/'.$data["file_name"], 'auto', 0775);

            $this->ftp->close();
            unlink('assets/backend/images/'.$folder.'/'.$data['file_name']);
            return $data['file_name'];
        }
        else {
            //$error = array('error' => $this->upload->display_errors());
            //var_dump($error);
            return FALSE;
        }
    }

    public function upload_image_local($inputname , $folder )
    {
        $path = realpath(APPPATH . '../assets/backend/images/'.$folder);
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '300';
        $config['overwrite']      = TRUE;
        $config['file_name']     = uniqid();
        $config['remove_spaces'] = TRUE;


        $this->upload->initialize($config);

        if($this->upload->do_upload($inputname))
        {
            $data = $this->upload->data();
//            $conf['image_library'] = 'gd2';
//            $conf['source_image'] = $data['full_path'];
//            $conf['maintain_ratio'] = TRUE;
//            $conf['width'] = 500;
//            $this->load->library('image_lib',$conf);
//            $this->image_lib->resize();
            return $data['file_name'];
        }
        else {
            //$error = array('error' => $this->upload->display_errors());
            //var_dump($error);
            return FALSE;
        }
    }

    public function index()
    {
        $this->isMustBeLogin();

        $data['title'] = 'صفحه اصلی';
        $data['name']  = $this->M_Setting->get_site_title();

        $this->AdminView('main_page', $data);
    }

    public function UserView($template, $data = [])
    {
        $this->blade->render('user_side/'.$template, $data);
    }

    public function AdminView($template, $data = [])
    {
        $this->blade->render('admin_side/'.$template, $data);
    }

    public function test()
    {
        $this->load->library('ftp');

        $config['hostname'] = 'ftp://upload.sjtmkbt.ir';
        $config['username'] = 'uploadsjtmkbt';
        $config['password'] = '9m4ex1ca0bur';
        $config['port']     = 21;
        $config['passive']  = FALSE;
        $config['debug']    = TRUE;

        $this->ftp->connect($config);

        $this->ftp->upload('/local/path/to/myfile.html', '/public_html/myfile.html', 'auto', 0775);

        $this->ftp->close();
    }
}
