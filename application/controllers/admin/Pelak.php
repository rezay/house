<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/2/2018
 * Time: 3:59 PM
 */
include APPPATH."controllers/BaseController.php";
class Pelak extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    public function pelaks_list()
    {
        $data['title'] = 'لیست پلاک ها';

        $data['pelaks'] = $this->M_Pelak->get_all_pelaks();
        $this->AdminView('pelaks', $data);
    }
    public function pelak_by_id ($pelak_id)
    {
        $data['pelak']    = $this->M_Pelak->get_pelak_by_id($pelak_id);
        $data['title']   = 'ویرایش پلاک ';
        $this->AdminView('edit_pelaks', $data);
    }



    public function update_pelaks()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $this->db->where('id',$posts['pelak_id']);
        $this->db->update('pelaks',array('pelak'=>$posts['pelak']));

        $data['pelaks'] = $this->M_Pelak->get_all_pelaks();
        $data['title'] = 'پلاک ها';

        $this->AdminView('pelaks', $data);

    }
    public function add_new_pelak()
    {
        $this->valid_access([1]);
        $data['title'] = 'ثبت پلاک جدید';

        $this->AdminView('new_pelak' ,$data);
    }
    public function insert_pelak()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();


        $pelak =$posts['pelak'];

        $result=$this->M_Pelak->insert_pelak($pelak);
        if ($result){
            $this->session->set_flashdata('message','پلاک با موفقیت ثبت شد');
        }
        else
        {
            $this->session->set_flashdata('message','مشکلی در ثبت رخ داده است');
        }
        redirect(base_url('admin-dashboard/pelak-list'));

    }

    public function remove_pelak()
    {
        $this->valid_access([1]);
        $pelak_id = $this->input->post('pelak_id');

        $this->M_Pelak->change_status_pelak($pelak_id);
    }

}