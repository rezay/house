<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/2/2018
 * Time: 3:59 PM
 */
include APPPATH."controllers/BaseController.php";
class Ground extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    public function add_new_ground()
    {
        $data['title'] = 'اضافه کردن داده جدید';
        $second_admin_access = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'second_admin_access')->get();
        if($second_admin_access->num_rows() == 0)
        {
            $access = 0;
        }
        else
        {
            $access = $second_admin_access->row()->value;
        }
        if($access || $this->session->userdata('logged_admin')['access_lvl'] == 1)
        {
            $this->AdminView('add_new_ground', $data);
        }
        else
        {
            $this->AdminView('error_pages/error_403',$data);
        }
    }

    public function new_ground_process()
    {
        $second_admin_access = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'second_admin_access')->get()->row()->value;
        if((isset($second_admin_access) && $second_admin_access == 1) || $this->valid_access([1]))
        {
            $membership_number = $this->input->post('membership_number');
            $attorney_number = $this->input->post('attorney_number');
            $attorney_date = $this->input->post('attorney_date');
            $type = NULL;
            $exist = $this->db->select('*')->from('grounds')->where('membership_number' ,$membership_number)->get();

            if($exist->num_rows() > 0)
            {
                $this->session->set_flashdata('admin_messages' , 'قبلا داده ای با این شماره عضویت ثبت شده است .');
            }
            else
            {
                $ground_data =[
                    'membership_number' => trim($membership_number),
                    'attorney_number' => trim($attorney_number) ,
                    'attorney_date' => trim($attorney_date),
                    'type' => $type
                ];
                $this->db->insert('grounds',$ground_data);
                $this->session->set_flashdata('admin_messages' , 'داده جدید با موفقیت ثبت شد .');
            }
        }
        else
        {
            $this->session->set_flashdata('admin_messages' , 'شما دسترسی به ثبت این داده ندارید');
        }

        redirect(base_url('admin-dashboard/add-new-ground'));

    }

    public function search_ground()
    {

        $data['title'] = 'جستجوی داده';
        if($this->input->post('membership_number') != '') {
            $membership_number = $this->input->post('membership_number');
            $exist = $this->db->select('*')->from('grounds')->where('membership_number', $membership_number)->get();
            if ($exist->num_rows() > 0) {
                $data['ground'] = $exist->row();
            } else {
                $this->session->set_flashdata('admin_messages', 'داده ای با این شماره عضویت موجود نمیباشد .');
                redirect(base_url('admin-dashboard/search-ground'));
            }
        }
        $second_admin_access = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'second_admin_access')->get();
        if($second_admin_access->num_rows() == 0)
        {
            $access = 0;
        }
        else
        {
            $access = $second_admin_access->row()->value;
        }
        if($access || $this->session->userdata('logged_admin')['access_lvl'] == 1)
        {
            $data['access'] = true;
        }
        else
        {
            $data['access'] = false;
        }
        $this->AdminView('search_ground', $data);

    }

    public function update_ground()
    {
        $second_admin_access = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'second_admin_access')->get()->row()->value;
        if((isset($second_admin_access) && $second_admin_access == 1) || $this->valid_access([1])) {
            $tracking_code_exist = $this->db->select('tracking_code')->from('grounds')->where('id', $this->input->post('ground_id'))->get()->row();
            if ($tracking_code_exist->tracking_code == NULL) {
                $membership_number = $this->input->post('membership_number');
                $attorney_number = $this->input->post('attorney_number');
                $attorney_date = $this->input->post('attorney_date');
                $data = [
                    'membership_number' => trim($membership_number),
                    'attorney_number' => trim($attorney_number),
                    'attorney_date' => trim($attorney_date),
                ];
                $this->db->set($data)->where('id', $this->input->post('ground_id'))->update('grounds');
            } else {
                $membership_number = $this->input->post('membership_number');
                $attorney_number = $this->input->post('attorney_number');
                $attorney_date = $this->input->post('attorney_date');
                $data = [
                    'membership_number' => $membership_number,
                    'attorney_number' => $attorney_number,
                    'attorney_date' => $attorney_date,
                ];
                $this->db->set($data)->where('id', $this->input->post('ground_id'))->update('grounds');
            }
            $this->session->set_flashdata('admin_messages', 'تغییرات با موفقیت اعمال شد .');
        }
        else
        {
            $this->session->set_flashdata('admin_messages' , 'شما دسترسی به تغییر داده ندارید');
        }

        redirect(base_url('admin-dashboard/search-ground'));
    }

}
