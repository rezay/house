<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/2/2018
 * Time: 3:59 PM
 */
include APPPATH."controllers/BaseController.php";
class PaymentTitle extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    public function payment_titles_list()
    {
        $data['title'] = 'لیست عناوین پرداختی';

		$data['payment_titles'] = $this->M_PaymentTitle->get_all_payment_titles();

        $this->AdminView('payment_title/payment_titles', $data);
    }
    public function payment_title_by_id ($payment_title_id)
    {
        $data['payment_title']    = $this->M_PaymentTitle->get_payment_title_by_id($payment_title_id);
        $data['title']   = 'ویرایش عنوان پرداختی ';
        $this->AdminView('payment_title/edit_payment_title', $data);
    }
    public function update_payment_title()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $this->db->where('id',$posts['payment_title_id']);
        $this->db->update('payment_titles',['payment_title'=>$posts['payment_title']]);

        $data['payment_titles'] = $this->M_PaymentTitle->get_all_payment_titles();
        $data['title'] = 'عناوین پرداختی';

        $this->AdminView('payment_title/payment_titles', $data);

    }
    public function add_new_payment_title()
    {
        $this->valid_access([1]);
        $data['title'] = 'ثبت عنوان پرداختی جدید';

        $this->AdminView('payment_title/new_payment_title' ,$data);
    }
    public function insert_payment_title()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $payment_title =$posts['payment_title'];

        $result=$this->M_PaymentTitle->insert_payment_title($payment_title);
        if ($result)
        {
            $this->session->set_flashdata('message','عنوان پرداختی با موفقیت ثبت شد');
        }
        else
        {
            $this->session->set_flashdata('message','مشکلی در ثبت رخ داده است');
        }
        redirect(base_url('admin-dashboard/payment-title-list'));

    }
    public function remove_payment_title()
    {
        $this->valid_access([1]);
        $payment_title_id = $this->input->post('payment_title_id');

        $this->M_PaymentTitle->change_status_payment_title($payment_title_id);
    }

}
