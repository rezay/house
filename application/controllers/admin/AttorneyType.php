<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/2/2018
 * Time: 3:59 PM
 */
include APPPATH."controllers/BaseController.php";
class AttorneyType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    public function attorney_types_list()
    {
        $data['title'] = 'لیست نوع وکالت ها';

		$data['attorney_types'] = $this->M_AttorneyType->get_all_attorney_types();

        $this->AdminView('attorney_type/attorney_types', $data);
    }
    public function attorney_type_by_id ($attorney_type_id)
    {
        $data['attorney_type']    = $this->M_AttorneyType->get_attorney_type_by_id($attorney_type_id);
        $data['title']   = 'ویرایش نوع وکالت ';
        $this->AdminView('attorney_type/edit_attorney_type', $data);
    }
    public function update_attorney_type()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $this->db->where('id',$posts['attorney_type_id']);
        $this->db->update('attorney_types',['attorney_type'=>$posts['attorney_type']]);

        $data['attorney_types'] = $this->M_AttorneyType->get_all_attorney_types();
        $data['title'] = 'نوع وکالت ها';

        $this->AdminView('attorney_type/attorney_types', $data);

    }
    public function add_new_attorney_type()
    {
        $this->valid_access([1]);
        $data['title'] = 'ثبت نوع وکالت جدید';

        $this->AdminView('attorney_type/new_attorney_type' ,$data);
    }
    public function insert_attorney_type()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $attorney_type =$posts['attorney_type'];

        $result=$this->M_AttorneyType->insert_attorney_type($attorney_type);
        if ($result)
        {
            $this->session->set_flashdata('message','نوع وکالت با موفقیت ثبت شد');
        }
        else
        {
            $this->session->set_flashdata('message','مشکلی در ثبت رخ داده است');
        }
        redirect(base_url('admin-dashboard/attorney-type-list'));

    }
    public function remove_attorney_type()
    {
        $this->valid_access([1]);
        $attorney_type_id = $this->input->post('attorney_type_id');

        $this->M_AttorneyType->change_status_attorney_type($attorney_type_id);
    }

}
