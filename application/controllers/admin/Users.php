<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/3/2018
 * Time: 11:35 AM
 */
include APPPATH."controllers/BaseController.php";
class Users extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    public function document_users($type = 'all' , $page)
    {
        $data['title'] = 'لیست کاربران سند دار';
        $data['row_start'] = $page;
        $data['users'] = $this->M_Ground->get_users($page,$type,0);
        foreach($data['users'] as $index => $user)
        {
            $attorney_count = $this->db->where('status' , 1)->where('parent_id !=' , 0)->where('ground_id' , $user->ground_id)->where('logged_user_id' , $user->logged_user_id)->count_all_results('attorneys');
            $data['users'][$index]->tracking_code = substr($user->tracking_code , 0 , 3).sprintf("%02d", $attorney_count).substr($user->tracking_code , 5 ,5);
        }
        //**********pagination**************
        $this->load->library('pagination');
        $data['base_url'] = base_url('admin-dashboard/document-users');
        $config['base_url'] = base_url('admin-dashboard/document-users/'.$type);
        $config['total_rows'] = $this->M_Ground->users_count($type,0);
        $config['per_page'] = 50;

        $this->pagination->initialize($config);
        $this->AdminView('users_list', $data);
    }
    public function attorney_users($type = 'all' , $page)
    {
        $data['title'] = 'لیست کاربران وکالتی';
        $data['row_start'] = $page;
        $data['users'] = $this->M_Ground->get_users($page,$type,1);
        foreach($data['users'] as $index => $user)
        {
            $attorney_count = $this->db->where('status' , 1)->where('parent_id !=' , 0)->where('ground_id' , $user->ground_id)->where('logged_user_id' , $user->logged_user_id)->count_all_results('attorneys');
            $data['users'][$index]->tracking_code = substr($user->tracking_code , 0 , 3).sprintf("%02d", $attorney_count).substr($user->tracking_code , 5 ,5);
        }
        //**********pagination**************
        $this->load->library('pagination');

        $data['base_url'] = base_url('admin-dashboard/attorney-users');
        $config['base_url'] = base_url('admin-dashboard/attorney-users/'.$type);
        $config['total_rows'] =  $this->M_Ground->users_count($type,1);
        $config['per_page'] = 50;

        $this->pagination->initialize($config);
        $this->AdminView('users_list', $data);
    }
    public function card_users($type = 'all' , $page)
    {
        $data['title'] = 'لیست کاربران کارتی';
        $data['row_start'] = $page;
        $data['users'] = $this->M_Ground->get_users($page,$type,2);
        //**********pagination**************
        $this->load->library('pagination');

        $data['base_url'] = base_url('admin-dashboard/card-users');
        $config['base_url'] = base_url('admin-dashboard/card-users/'.$type);
        $config['total_rows'] = $this->M_Ground->users_count($type,2);
        $config['per_page'] = 50;

        $this->pagination->initialize($config);
        $this->AdminView('users_list', $data);
    }

    public function full_info_card_user($ground_id)
    {
        $data['title'] = 'ریز اطلاعات کاربران';
        $logged_user_id = $this->db->select('id')->from('logged_users')->where('ground_id' , $ground_id)->get()->row()->id;
        $data['full_info_user'] = $this->M_Ground->get_full_data_card($ground_id);
        $data['ground_id'] = $ground_id;
        $data['user'] = $this->db->select('*')->from('grounds')->where('id' , $ground_id)->get()->row();
        $data['silver_debits'] = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 0)->get()->result();
        $data['gold_debits'] = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 1)->get()->result();
        $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->where('accepted', 1)->get('bills_cheques');
        if($total_payments->num_rows() > 1)
        {
            $data['total_payments'] = $total_payments->row()->amount;
        }
        else
        {
            $data['total_payments'] = 0;
        }
        //var_dump($data['full_info_user']);die;
        $data['attorney_types'] = $this->M_AttorneyType->get_all_attorney_types();
		$data['payment_titles'] = $this->M_PaymentTitle->get_all_payment_titles();
		$data['build_states'] = $this->M_BuildState->get_all_build_states();
        $this->AdminView('full_info_card_user',$data);
    }
    public function full_info_attorney_user($ground_id)
    {
        $data['title'] = 'ریز اطلاعات کاربران';
        $logged_user_id = $this->db->select('id')->from('logged_users')->where('ground_id' , $ground_id)->get()->row()->id;
        $data['full_info_user'] = $this->M_Ground->get_full_data_attorney($ground_id);
        $data['ground_id'] = $ground_id;
        $data['user'] = $this->db->select('*')->from('grounds')->where('id' , $ground_id)->get()->row();

        $attorney_count = $this->db->where('status' , 1)->where('parent_id !=' , 0)->where('ground_id' , $ground_id)->where('logged_user_id' , $logged_user_id)->count_all_results('attorneys');
        $data['full_info_user']['info'][0]->tracking_code = substr($data['full_info_user']['info'][0]->tracking_code , 0 , 3).sprintf("%02d", $attorney_count).substr($data['full_info_user']['info'][0]->tracking_code , 5 ,5);

        $data['silver_debits'] = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 0)->get()->result();
        $data['gold_debits'] = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 1)->get()->result();
        $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->where('accepted', 1)->get('bills_cheques');
        if($total_payments->num_rows() > 1)
        {
            $data['total_payments'] = $total_payments->row()->amount;
        }
        else
        {
            $data['total_payments'] = 0;
        }
        $data['attorney_types'] = $this->M_AttorneyType->get_all_attorney_types();
		$data['payment_titles'] = $this->M_PaymentTitle->get_all_payment_titles();
		$data['build_states'] = $this->M_BuildState->get_all_build_states();
        $this->AdminView('full_info_attorney_user',$data);
    }
    public function full_info_document_user($ground_id)
    {
        $logged_user_id = $this->db->select('id')->from('logged_users')->where('ground_id' , $ground_id)->where('status' , 1)->get()->row()->id;
        $ground_ids_sum = $this->db->select_sum('share_amount')->from('user_meta')->where('ground_id' , $ground_id)->where('logged_user_id' , $logged_user_id)->where('status' ,1)->get()->row()->share_amount;
        if($ground_ids_sum > 1)
        {
            $data['message'] = 'مجموع سهم این کاربران بیشتر از یک میباشد .';
        }
        elseif ($ground_ids_sum < 1)
        {
            $data['message'] = 'مجموع سهم این کاربران کمتر از یک میباشد .';
        }
        $data['ground_id'] = $ground_id;
        $data['user'] = $this->db->select('*')->from('grounds')->where('id' , $ground_id)->get()->row();
        $data['silver_debits'] = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 0)->get()->result();
        $data['gold_debits'] = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 1)->get()->result();
        $data['title'] = 'ریز اطلاعات کاربران';
        $data['full_info_user'] = $this->M_Ground->get_full_data_document($ground_id);
        $attorney_count = $this->db->where('status' , 1)->where('parent_id !=' , 0)->where('ground_id' , $ground_id)->where('logged_user_id' , $logged_user_id)->count_all_results('attorneys');
        $data['full_info_user']['info'][0]->tracking_code = substr($data['full_info_user']['info'][0]->tracking_code , 0 , 3).sprintf("%02d", $attorney_count).substr($data['full_info_user']['info'][0]->tracking_code , 5 ,5);
        $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->where('accepted', 1)->get('bills_cheques');
        if($total_payments->num_rows() > 1)
        {
            $data['total_payments'] = $total_payments->row()->amount;
        }
        else
        {
            $data['total_payments'] = 0;
        }
        $data['attorney_types'] = $this->M_AttorneyType->get_all_attorney_types();
        $data['payment_titles'] = $this->M_PaymentTitle->get_all_payment_titles();
        $data['build_states'] = $this->M_BuildState->get_all_build_states();
        //var_dump($data['full_info_card_user']);die;
        $this->AdminView('full_info_document_user',$data);
    }

    public function update_info()
    {
        $inputs = $this->input->post();

        foreach($inputs as $index => $input)
        {
            if(is_integer($input) || is_double($input)) {
                $inputs[$index] = convertPersianNumbersToEnglish($input);
            }
            if(strpos($index ,'date') !== FALSE) {
                $d = explode('/',$input);
                if( ! isset($d[1]) || ! isset($d[2]) || jcheckdate($d[1] , $d[2] , $d[0]) == FALSE){
                    $inputs = array_diff_key($inputs , [$index => '**']);
                }
            }
        }
        if($this->input->post('table') == 'documents')
        {
			$data = array_diff_key($inputs,['table' => '**','ground_id' => '**']);
			$this->db->where('id', $this->input->post('id'))->set($data)->update($this->input->post('table'));
        }
        elseif($this->input->post('table') == 'certain_documents')
		{
			$data = array_diff_key($inputs,['table' => '**','ground_id' => '**']);
			$this->db->where('id', $this->input->post('id'))->set($data)->update($this->input->post('table'));
		}
        elseif($this->input->post('table') == 'user_meta')
        {
            if(isset($inputs['share_amount']))
            {
                $this->db->set('share_amount', $inputs['share_amount'])->where('user_id', $this->input->post('user_id'))->update('user_meta');
            }
            $data = array_diff_key($inputs,['share_amount' => '**' , 'table' => '**','user_id' => '**']);
            $this->db->where('user_id', $this->input->post('user_id'))->set($data)->update($this->input->post('table'));
        }
        elseif($this->input->post('table') == 'attorneys')
        {
            $data = array_diff_key($inputs,['table' => '**','id' => '**']);
//            $data['amount'] = parse_number($data['amount']);
            $this->db->where('id', $this->input->post('id'))->set($data)->update($this->input->post('table'));
        }
		elseif($this->input->post('table') == 'bills_cheques')
		{
			$data = array_diff_key($inputs,['table' => '**','id' => '**']);
			$data['amount'] = parse_number($data['amount']);
			$this->db->where('id', $this->input->post('id'))->set($data)->update($this->input->post('table'));
		}
		elseif($this->input->post('table') == 'cards')
		{
			$data = array_diff_key($inputs,['table' => '**','id' => '**']);
//			$data['amount'] = parse_number($data['amount']);
			$this->db->where('id', $this->input->post('id'))->set($data)->update($this->input->post('table'));
		}
        else
        {
            $logged_user_data = [
                'bill_number' => $this->input->post('bill_number'),
                'bill_date' => $this->input->post('bill_date'),
                'full_name' => $this->input->post('full_name'),
                'phone_number' => $this->input->post('phone_number'),
            ];
            $data = [
                'ground_number' => $this->input->post('ground_number'),
                'delivery_date' => $this->input->post('delivery_date'),
                'build_state_id' => $this->input->post('build_state_id'),
                'attorney_type_id' => $this->input->post('attorney_type_id'),
                'bill_number' => $this->input->post('bill_number'),
                'bill_date' => $this->input->post('bill_date'),
                'full_name' => $this->input->post('full_name'),
                'phone_number' => $this->input->post('phone_number'),
            ];
            if($this->input->post('password') != '')
            {
                $data['password'] = md5(strtoupper($this->input->post('password')));
            }

            $this->db->where('ground_id',$this->input->post('id'))->set($logged_user_data)->update('logged_users');
            $this->db->where('id',$this->input->post('id'))->set($data)->update($this->input->post('table'));
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    public function update_debits()
    {
        $inputs = $this->input->post();
        $exist = $this->db->select('*')->from('debits')->where('ground_id' , $inputs['ground_id'])->where('type' , $inputs['type'])->get()->num_rows();
        if( ! $exist)
        {
            if ($inputs['type'] == 1) {
                $this->db->insert('debits', [
                    'ground_id' => $inputs['ground_id'],
                    'debit_amount' => parse_number($inputs['gold_debit']),
                    'accepted_payment' => NULL,
                    'type' => 1
                ]);
            } else {
                $this->db->insert('debits', [
                    'ground_id' => $inputs['ground_id'],
                    'debit_amount' => parse_number($inputs['silver_debit']),
                    'accepted_payment' => parse_number($inputs['accepted_payment']),
                    'type' => 0
                ]);
            }
        }
        else
        {
            if ($inputs['type'] == 1) {
                $this->db->set('debit_amount' , parse_number($inputs['gold_debit']))->where('ground_id' , $inputs['ground_id'])->where('type' , $inputs['type'])->update('debits');
            } else {
                $this->db->set(['debit_amount' => parse_number($inputs['silver_debit']) , 'accepted_payment' => parse_number($inputs['accepted_payment'])])->where('ground_id' , $inputs['ground_id'])->where('type' , $inputs['type'])->update('debits');
            }
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    public function upload_new_image()
    {
        $column = $this->input->post('column');
        $folder = $this->input->post('folder');
        $condition_value = $this->input->post('condition_value');
//        var_dump($folder."---".$condition_value."---".$column);die;

        switch ($folder)
        {
            case 'attorneys' :
                $table = 'attorneys';
                $condition = 'id';
                break;
            case 'bills_cheques' :
                $table = 'bills_cheques';
                $condition = 'id';
                break;
            case 'documents' :
                $table = 'documents';
                $condition = 'ground_id';
                break;
            case 'certain_documents' :
                $table = 'certain_documents';
                $condition = 'ground_id';
                break;
            case 'user_meta' :
                $table = 'user_meta';
                $condition = 'user_id';
                break;
            case 'card' :
                $table = 'users';
                $condition = 'user_id';
                break;
			case 'personal_photos' :
				$table = 'grounds';
				$condition = 'id';
				break;
        }
//        var_dump($condition);die;
        $result = $this->upload_image_ftp('new_img' , $folder);

        if($result)
        {
            $this->db->where($condition , $condition_value)->set($column , $folder."/".$result)->update($table);
            echo $folder."/".$result;
        }
    }
    public function black_list_seen($ground_id)
    {
        $this->db->set('seen' , 1)->where('ground_id' , $ground_id)->update('black_list');
    }
    public function change_silver()
    {
        $this->valid_access([1]);
        $ground_id = $this->input->post('ground_id');
        $status = $this->input->post('status');
        if($status == 0)
        {
            $let_to_gold = $this->db->select('let_to_gold')->from('grounds')->where('id' , $ground_id)->get()->row()->let_to_gold;
            if($let_to_gold == 0)
            {
                $this->db->set('let_to_silver', $status)->where('id', $ground_id)->update('grounds');
                $this->db->set('bycot', $status)->where('ground_id', $ground_id)->update('black_list');
            }
        }
        else
        {
            $this->db->set('let_to_silver', $status)->where('id', $ground_id)->update('grounds');
            $this->db->set('bycot', $status)->where('ground_id', $ground_id)->update('black_list');
            $message = $this->db->select('value')->from('settings')->where('in_group' , 'sms')->where('item' , 'lvl2')->get()->row()->value;
            $phone_number = $this->db->select('phone_number')->from('grounds')->where('id' , $ground_id)->get()->row()->phone_number;
            Send($message , $phone_number,'',FALSE);
        }
    }
    public function change_reviewed()
    {
        $ground_id = $this->input->post('ground_id');
        $status = $this->input->post('status');
        $this->db->set('reviewed',$status)->where('id' , $ground_id)->update('grounds');

    }
    public function change_gold()
    {
        $this->valid_access([1]);
        $ground_id = $this->input->post('ground_id');
        $status = $this->input->post('status');
        if($status == 1)
        {
            $let_to_silver = $this->db->select('let_to_silver')->from('grounds')->where('id' , $ground_id)->get()->row()->let_to_silver;
            $silver_payed = $this->db->select('payed')->from('debits')->where('ground_id' , $ground_id)->where('type' , 0)->get();
            if($silver_payed->num_rows() > 1)
            {
                $silver_payed = $silver_payed->row()->payed;
            }
            else
            {
                $silver_payed = 1;
            }
            if($let_to_silver == 1 && $silver_payed == 1)
            {
                $this->db->set('let_to_gold',$status)->where('id' , $ground_id)->update('grounds');
                $message = $this->db->select('value')->from('settings')->where('in_group' , 'sms')->where('item' , 'lvl3')->get()->row()->value;
                $phone_number = $this->db->select('phone_number')->from('grounds')->where('id' , $ground_id)->get()->row()->phone_number;
                Send($message , $phone_number,'',FALSE);
            }
        }
        else
        {
            $msg_to_user = $this->db->select('msg_to_user')->from('grounds')->where('id' , $ground_id)->get()->row()->msg_to_user;
            if($msg_to_user == 0)
            $this->db->set('let_to_gold',$status)->where('id' , $ground_id)->update('grounds');
        }

    }
    public function change_msg_to_user()
    {
        $this->valid_access([1]);
        $ground_id = $this->input->post('ground_id');
        $status = $this->input->post('status');
        if($status == 1)
        {
            $let_to_silver = $this->db->select('let_to_silver')->from('grounds')->where('id' , $ground_id)->get()->row()->let_to_silver;
            $let_to_gold = $this->db->select('let_to_gold')->from('grounds')->where('id' , $ground_id)->get()->row()->let_to_gold;
            $silver_payed = $this->db->select('payed')->from('debits')->where('ground_id' , $ground_id)->where('type' , 0)->get();
            $gold_payed = $this->db->select('payed')->from('debits')->where('ground_id' , $ground_id)->where('type' , 1)->get();
            if($silver_payed->num_rows() > 1)
            {
                $silver_payed = $silver_payed->row()->payed;
            }
            else
            {
                $silver_payed = 1;
            }
            if($gold_payed->num_rows() > 1)
            {
                $gold_payed = $gold_payed->row()->payed;
            }
            else
            {
                $gold_payed = 1;
            }
            if($let_to_silver == 1 && $let_to_gold == 1 && $silver_payed == 1 && $gold_payed ==1)
            {
                $this->db->set('msg_to_user',$status)->where('id' , $ground_id)->update('grounds');
                $message = $this->db->select('value')->from('settings')->where('in_group' , 'sms')->where('item' , 'lvl4')->get()->row()->value;
                $phone_number = $this->db->select('phone_number')->from('grounds')->where('id' , $ground_id)->get()->row()->phone_number;
                Send($message , $phone_number,'',FALSE);
            }
        }
        else
        {
            $solved = $this->db->select('solved')->from('grounds')->where('id' , $ground_id)->get()->row()->solved;
            if($solved == 0)
            $this->db->set('msg_to_user',$status)->where('id' , $ground_id)->update('grounds');
        }

    }
    public function change_solved()
    {
        $this->valid_access([1]);
        $ground_id = $this->input->post('ground_id');
        $status = $this->input->post('status');
        if($status == 1)
        {
            $this->db->set('solved',$status)->where('id' , $ground_id)->update('grounds');
            $message = $this->db->select('value')->from('settings')->where('in_group' , 'sms')->where('item' , 'lvl5')->get()->row()->value;
            $phone_number = $this->db->select('phone_number')->from('grounds')->where('id' , $ground_id)->get()->row()->phone_number;
            Send($message , $phone_number,'',FALSE);
        }
        else
        {
            $this->db->set('solved',$status)->where('id' , $ground_id)->update('grounds');
        }

    }
    public function remove_black_list()
    {
        $this->valid_access([1]);
        $black_list_id = $this->input->post('black_list_id');
        $this->db->set(['deleted' =>  1])->where('id' , $black_list_id)->update('black_list');

    }
    public function accept_black_list()
    {
        $this->valid_access([1]);
        $black_list_id = $this->input->post('black_list_id');
        $data = $this->db->select('*')->from('black_list')->where('id',$black_list_id)->where('deleted' , 0)->get()->row();
        $list = str_split('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
        $user_password = '';
        for ($i = 0; $i < 10; $i++)
        {
            $user_password .= $list[mt_rand(0, count($list)-1)];
        }
        $logged_user_id = $this->db->select('id')->from('logged_users')->where('ground_id' , $data->ground_id)->where('status' , 1)->get()->row()->id;
        $this->db->set(['full_name' => $data->full_name ,'bill_number' => $data->bill_number , 'bill_date' => $data->bill_date,'phone_number' => $data->phone_number ,'password' => md5(strtoupper($user_password)) , 'description' => null ])->where('id' , $data->ground_id)->update('grounds');
        $this->db->insert('logged_users' , ['ground_id' => $data->ground_id,'membership_number' => $data->membership_number,'full_name' => $data->full_name ,'bill_number' => $data->bill_number , 'bill_date' => $data->bill_date,'phone_number' => $data->phone_number ,'access_lvl' => 'basic']);
        $new_logged_user_id = $this->db->insert_id();
        $this->db->set('status' , 0)->where('id' , $logged_user_id)->update('logged_users');
        $this->db->where('id' , $black_list_id)->delete('black_list');
        $this->db->set('status' , 0)->where('logged_user_id' , $new_logged_user_id)->update('user_meta');
        $this->db->set('status' , 0)->where('logged_user_id' , $new_logged_user_id)->update('documents');
        $this->db->set('status' , 0)->where('logged_user_id' , $new_logged_user_id)->update('certain_documents');
        $this->db->set('status' , 0)->where('logged_user_id' , $new_logged_user_id)->update('bills_cheques');
        $this->db->set('status' , 0)->where('logged_user_id' , $new_logged_user_id)->update('attorneys');
        $this->db->set('bycot' , 1)->where('ground_id' , $data->ground_id)->update('black_list');
        echo $user_password;

    }
    public function search()
    {
        $data['title'] = 'کاربران یافت شده';
        $data['row_start'] = 0;
        $target = $this->input->get('target');
        $arr = preg_split('/(?<=[0-9])(?=[A-Z]+)/i',$target);
        if(isset($arr[1])){
            $target1 = $arr[0].' '.$arr[1];
        }
        else
        {
            $target1 = NULL;
        }
		$search_1 = $this->db->select('*,logged_users.id as logged_user_id,(select count(*) from black_list where ground_id = grounds.id AND seen = 0 AND deleted = 0 AND bycot = 0) as unseen_black_list')->from('grounds')
            ->join('logged_users' , 'grounds.id = logged_users.ground_id')
            ->where('logged_users.status' , 1)
            ->where('grounds.tracking_code !=' , NULL)
            ->group_start()
            ->where('grounds.membership_number' , $target)
            ->or_where('grounds.membership_number' , $target1)
            ->or_where('grounds.tracking_code' , $target)
            ->group_end()
            ->get()->result();

        $exploded_name = explode(' ' , $target);
        $search_2 = $this->db->select('ground_id')->from('user_meta')
			->where('national_code' , $target)
			->or_group_start()
			->group_start()
			->like('first_name' , $exploded_name[0])
			->or_like('last_name' , $exploded_name[0])
			->group_end();
        if(isset($exploded_name[1]))
		{
			$search_2  = $search_2
				->group_start()
				->like('first_name' , $exploded_name[1])
				->or_like('last_name' , $exploded_name[1])
				->group_end();
		}

		$search_2 = $search_2->group_end()->get()->result();

        if($search_2)
		{
			$search_2 = $this->db->select('*,logged_users.id as logged_user_id,(select count(*) from black_list where ground_id = grounds.id AND seen = 0 AND deleted = 0 AND bycot = 0) as unseen_black_list')->from('grounds')
				->join('logged_users' , 'grounds.id = logged_users.ground_id')
				->where('logged_users.status' , 1)
				->where('grounds.tracking_code !=' , NULL)
				->group_start()
				->where_in('grounds.id' , array_column($search_2 , 'ground_id'))
				->group_end()
				->get()->result();
		}
        else
		{
			$search_2 = [];
		}

        $data['searched_word'] = $target;
        $data['users'] = array_merge($search_1 , $search_2);
        $this->load->library('pagination');

        $config['base_url'] = base_url('admin-dashboard/search');
        $config['total_rows'] = count($data['users']);
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $this->AdminView('users_list' , $data);

    }
    public function solved_users($page)
    {
        $data['title'] = 'کاربران یافت شده';
        $data['row_start'] = $page;
        $user_type = $this->input->post('user_type');
        $data['users'] = $this->db->select('*,logged_users.id as logged_user_id,(select count(*) from black_list where ground_id = grounds.id AND seen = 0 AND deleted = 0 AND bycot = 0) as unseen_black_list')->from('grounds')
            ->join('logged_users' , 'grounds.id = logged_users.ground_id')
            ->where('logged_users.status' , 1)
            ->where('grounds.tracking_code !=' , NULL)
            ->where('grounds.type' , $user_type)
            ->where('grounds.solved' , 1)
            ->limit(10 , $page*10)
            ->get()->result();
        $this->load->library('pagination');
        $config['base_url'] = base_url('admin-dashboard/solved-users');
        $config['total_rows'] = count($data['users']);
        $config['per_page'] = 10;

        $this->pagination->initialize($config);
        $this->AdminView('users_list' , $data);

    }
    public function change_status_bill_cheque()
    {
        $this->valid_access([1]);
        $id = $this->input->post('bill_cheque_id');
        $current_status = $this->db->select('accepted')->from('bills_cheques')->where('id' , $id)->get()->row()->accepted;
        $this->db->set('accepted' , ! $current_status)->where('id' , $id)->update('bills_cheques');
        echo $current_status;
    }
    public function remove_user()
    {
        $this->valid_access([1]);
        $ground_id = $this->input->post('ground_id');
        $this->db->set(['tracking_code' => NULL , 'password' => NULL , 'description' => NULL , 'type' => NULL])->where('id' , $ground_id)->update('grounds');
    }
    public function print_page($ground_id , $logged_user_id)
    {
        $data['title'] = 'پرینت اطلاعات';
        $data['tracking_code'] = $this->db->select('tracking_code')->from('grounds')->where('id', $ground_id)->get()->row()->tracking_code;
        $attorney_count = $this->db->where('status' , 1)->where('parent_id !=' , 0)->where('ground_id' , $ground_id)->where('logged_user_id' , $logged_user_id)->count_all_results('attorneys');
        $data['tracking_code'] = substr($data['tracking_code'] , 0 , 3).sprintf("%02d", $attorney_count).substr($data['tracking_code'] , 5 ,5);
        $ground_detail = $this->db->select('*')->from('grounds')->where('id' , $ground_id)->get()->row();
        if($ground_detail->solved == 1)
        {
            $let_to = 'solved';
            $data['let_to'] = 'solved';
        }
        elseif($ground_detail->msg_to_user == 1)
        {
            $let_to = 'msg_to_user';
            $data['let_to'] = 'msg_to_user';
        }
        elseif ($ground_detail->let_to_gold == 1)
        {
            $let_to = 'let_to_gold';
            $data['let_to'] = 'let_to_gold';
        }
        elseif ($ground_detail->let_to_silver == 1)
        {
            $let_to = 'let_to_silver';
            $data['let_to'] = 'let_to_silver';
        }
        else
        {
            $let_to = 0;
            $data['let_to'] = 0;
        }
        if($let_to === 'let_to_silver')
        {
            $debit = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 0)->get();
            if($debit->num_rows() > 0) {
                $payments = $this->db->select('priority')->from('payments')->where('logged_user_id', $logged_user_id)->where('debit_id', $debit->row()->id)->where('priority !=', NULL)->where('status', 1)->get();
                if ($payments->num_rows() > 0)
                {
                    $data['priority'] = $payments->row()->priority;
                }
                else
                {
                    $data['priority'] = '';
                }

                if ($debit->row()->payed == 1) {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                    $silver_debit_payed = $debit->row()->debit_amount-$debit->row()->accepted_payment;
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount+$silver_debit_payed;
                    }
                    else
                    {
                        $data['total_payments'] = 0+$silver_debit_payed;
                    }
                } else {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->get('bills_cheques')->row()->amount;
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount;
                    }
                    else
                    {
                        $data['total_payments'] = 0;
                    }
                }
            }
            else
            {
                $data['priority'] = '';
                $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                if($total_payments->row()->amount != NULL)
                {
                    $data['total_payments'] = $total_payments->row()->amount;
                }
                else
                {
                    $data['total_payments'] = 0;
                }
            }
        }
        elseif ($let_to === 'let_to_gold')
        {
            $silver_debit = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 0)->get();
            $gold_debit = $this->db->select('*')->from('debits')->where('ground_id' , $ground_id)->where('type' , 1)->get();
            if($gold_debit->num_rows() > 0) {
                $payments = $this->db->select('priority')->from('payments')->where('logged_user_id', $logged_user_id)->where('debit_id', $gold_debit->row()->id)->where('priority !=', NULL)->where('status', 1)->get();
                if ($payments->num_rows() > 0)
                {
                    $data['priority'] = $payments->row()->priority;
                }
                else
                {
                    $data['priority'] = '';
                }
                //var_dump($data['priority']);die;
                if ($gold_debit->row()->payed == 1) {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount+$silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment+$gold_debit->row()->debit_amount;
                    }
                    else
                    {
                        $data['total_payments'] = $silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment+$gold_debit->row()->debit_amount;
                    }
                } else {
                    $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->get('bills_cheques');
                    if($total_payments->row()->amount != NULL)
                    {
                        $data['total_payments'] = $total_payments->row()->amount+$silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment;
                    }
                    else
                    {
                        $data['total_payments'] = $silver_debit->row()->debit_amount-$silver_debit->row()->accepted_payment;
                    }
                }
            }
            else
            {
                $data['priority'] = '';
                $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->where('accepted', 1)->get('bills_cheques');
                if($total_payments->row()->amount != NULL)
                {
                    $data['total_payments'] = $total_payments->row()->amount;
                }
                else
                {
                    $data['total_payments'] = 0;
                }
            }
        }
        else
        {
            $data['priority'] = '';
            $total_payments = $this->db->select_sum('amount')->where('logged_user_id', $logged_user_id)->where('status', 1)->get('bills_cheques');

            if($total_payments->row()->amount != NULL)
            {
                $data['total_payments'] = $total_payments->row()->amount;
            }
            else
            {
                $data['total_payments'] = 0;
            }
        }

        $settings = $this->M_Setting->get_home_setting();
        foreach ($settings as $setting) {
            switch ($setting->item) {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'logo' :
                    $data['logo'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = toPersianNum($setting->value);
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
            }
        }
        $data['membership_number'] = $ground_detail->membership_number;
        if($ground_detail->type == 2)
        {
            $data['type'] = 'کارتی';
            $data['full_info_user'] = $this->M_Ground->get_full_data_card($ground_id);
        }
        elseif($ground_detail->type == 1)
        {
            $data['type'] = 'وکالتی';
            $data['full_info_user'] = $this->M_Ground->get_full_data_attorney($ground_id);
        }
        else
        {
            $data['type'] = 'سند دار';
            $data['full_info_user'] = $this->M_Ground->get_full_data_document($ground_id);
        }
        //var_dump($data['full_info_user']);die;
        $this->AdminView('print',$data);
    }
}
