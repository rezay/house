<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/2/2018
 * Time: 3:59 PM
 */
include APPPATH."controllers/BaseController.php";
class Guide extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    public function site_guide()
    {
        $data['title'] = 'راهنمای سایت';

        $data['guides'] = $this->M_Setting->get_guides();
        $this->AdminView('guides', $data);
    }

    public function update_guide()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $this->db->where('in_group','guide')->where('status' ,1)->where('item' , $posts['item'])->update('settings',['value'=>$posts['guide']]);
        redirect(base_url('admin-dashboard/site-guide'));

    }

}