<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/2/2018
 * Time: 3:59 PM
 */
include APPPATH."controllers/BaseController.php";
class BuildState extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    public function build_states_list()
    {
        $data['title'] = 'لیست وضعیت عمرانی';

		$data['build_states'] = $this->M_BuildState->get_all_build_states();

        $this->AdminView('build_state/build_states', $data);
    }
    public function build_state_by_id ($build_state_id)
    {
        $data['build_state']    = $this->M_BuildState->get_build_state_by_id($build_state_id);
        $data['title']   = 'ویرایش وضعیت عمرانی ';
        $this->AdminView('build_state/edit_build_state', $data);
    }
    public function update_build_state()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $this->db->where('id',$posts['build_state_id']);
        $this->db->update('build_states',['build_state'=>$posts['build_state']]);

        $data['build_states'] = $this->M_BuildState->get_all_build_states();
        $data['title'] = 'وضعیت عمرانی';

        $this->AdminView('build_state/build_states', $data);

    }
    public function add_new_build_state()
    {
        $this->valid_access([1]);
        $data['title'] = 'ثبت وضعیت عمرانی جدید';

        $this->AdminView('build_state/new_build_state' ,$data);
    }
    public function insert_build_state()
    {
        $this->valid_access([1]);
        $posts = $this->input->post();

        $build_state =$posts['build_state'];

        $result=$this->M_BuildState->insert_build_state($build_state);
        if ($result)
        {
            $this->session->set_flashdata('message','وضعیت عمرانی با موفقیت ثبت شد');
        }
        else
        {
            $this->session->set_flashdata('message','مشکلی در ثبت رخ داده است');
        }
        redirect(base_url('admin-dashboard/build-state-list'));

    }
    public function remove_build_state()
    {
        $this->valid_access([1]);
        $build_state_id = $this->input->post('build_state_id');

        $this->M_BuildState->change_status_build_state($build_state_id);
    }

}
