<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/3/2018
 * Time: 11:35 AM
 */
include APPPATH."controllers/BaseController.php";
class Settings extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
        $this->valid_access([1]);
    }

    public function texts_setting()
    {
        $data['title'] = 'تنظیمات';

        $settings = $this->M_Setting->get_home_setting();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'header_first_line' :
                    $data['header_first_line'] = $setting->value;
                    break;
                case 'beside_login' :
                    $data['beside_login'] = $setting->value;
                    break;
                case 'footer_text' :
                    $data['footer_text'] = $setting->value;
                    break;
                case 'emam_text' :
                    $data['emam_text'] = $setting->value;
                    break;
                case 'shomare_sabt' :
                    $data['shomare_sabt'] = $setting->value;
                    break;
                case 'header_third_line' :
                    $data['header_third_line'] = $setting->value;
                    break;
            }
        }

        $this->AdminView('settings/texts_setting',$data);
    }
    public function sms_setting()
    {
        $data['title'] = 'تنظیمات';

        $settings = $this->M_Setting->get_sms_texts();
        foreach($settings as $setting)
        {
            switch ($setting->item)
            {
                case 'lvl1' :
                    $data['lvl1'] = $setting->value;
                    break;
                case 'lvl2' :
                    $data['lvl2'] = $setting->value;
                    break;
                case 'lvl3' :
                    $data['lvl3'] = $setting->value;
                    break;
                case 'lvl4' :
                    $data['lvl4'] = $setting->value;
                    break;
                case 'lvl5' :
                    $data['lvl5'] = $setting->value;
                    break;
            }
        }

        $this->AdminView('settings/sms_texts',$data);
    }

    public function photo_setting()
    {
        $data['title'] = 'تنظیمات';

        $this->AdminView('settings/photos_setting',$data);
    }

    public function new_photo()
    {
        $file_name = $this->input->post('file_name');
        $item = $this->input->post('item');
        $this->db->where('item' , $item)->set('value' , $file_name)->update('settings');
    }
    public function update_texts()
    {
        if($this->input->post('emam_text'))
        {
            $text = json_encode([$this->input->post('emam_text') , $this->input->post('emam')]);
        }
        else
        {
            $text = $this->input->post('text');
        }
        $item = $this->input->post('item');
        $this->db->where('item' , $item)->set('value' , $text)->update('settings');
        redirect(base_url('admin-dashboard/texts-setting'));
    }
    public function update_msg_texts()
    {
        $text = $this->input->post('text');
        $item = $this->input->post('item');
        $this->db->where('item' , $item)->set('value' , $text)->update('settings');
        redirect(base_url('admin-dashboard/sms-setting'));
    }
    public function change_web_status()
    {
        $status = $this->input->post('status');
        $this->db->set('value', $status)->where('in_group', 'web_status')->where('item' , 'web_status')->update('settings');
    }

    public function change_second_admin_access_status()
    {
        $status = $this->input->post('status');
        $this->db->set('value', $status)->where('in_group', 'web_status')->where('item' , 'second_admin_access')->update('settings');
    }

    public function web_status()
    {
        $data['title'] = 'وضعیت وب سایت';
        $data['status'] = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'web_status')->get()->row()->value;
        $this->AdminView('settings/web_status' , $data);
    }

    public function second_admin_access()
    {
        $data['title'] = 'وضعیت دسترسی مدیران سطح دوم';
        $second_admin_access = $this->db->select('value')->from('settings')->where('in_group' , 'web_status')->where('item' , 'second_admin_access')->get();
        if($second_admin_access->num_rows() == 0)
        {
            $second_admin_access_setting = [
                'in_group' => 'web_status',
                'item' => 'second_admin_access',
                'value' => 0
            ];
            $this->db->insert('settings' , $second_admin_access_setting);
            $data['second_admin_access'] = 0;
        }
        else
        {
            $data['second_admin_access'] = $second_admin_access->row()->value;
        }

        $this->AdminView('settings/second_admin_access' , $data);
    }

}