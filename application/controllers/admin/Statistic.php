<?php
/**
 * Created by PhpStorm.
 * User: hassan
 * Date: 5/24/2018
 * Time: 11:15 AM
 */

include APPPATH."controllers/BaseController.php";
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Statistic extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
    }

    private function create_filter_query($posts)
    {
    	if( ! isset($posts['user_type']) || (isset($posts['user_type']) && in_array(2 , $posts['user_type'])))
		{
			$grounds = $this->M_Ground->get_all_grounds(false);
		}
    	else
		{
			$grounds = $this->M_Ground->get_all_grounds(true);
		}

        if(isset($posts['user_type']) && !empty($posts['user_type']))
        {
        	$grounds = $grounds->where_in('grounds.type' , $posts['user_type']);
        }

        if(isset($posts['level']) && $posts['level'])
        {
            if($posts['level'] == 'green')
            {
                $grounds = $grounds
                    ->where('grounds.let_to_silver' , 0)
                    ->where('grounds.let_to_gold' , 0)
                    ->where('grounds.solved' , 0);
            }
            elseif($posts['level'] == 'all')
            {
                // do nothing
            }
            else
            {
                $grounds = $grounds->where($posts['level'] , 1);
            }
        }

        if(isset($posts['attorney_types_id']) && $posts['attorney_types_id'])
        {
            $grounds = $grounds->where_in('grounds.attorney_type_id' , $posts['attorney_types_id']);
        }

		if(isset($posts['user_type']) && ! in_array(2 , $posts['user_type']))
		{
			if(isset($posts['attorney_count']) && $posts['attorney_count'] != NULL && is_array($posts['attorney_count']))
			{
//				$grounds = $grounds->group_start();
				if(in_array(1 ,$posts['attorney_count']))
				{
					$grounds = $grounds->or_having('attorney_count' , 1);
				}
				if(in_array(2 ,$posts['attorney_count']))
				{
					$grounds = $grounds->or_having('attorney_count' , 2);
				}
				if(in_array(3 ,$posts['attorney_count']))
				{
					$grounds = $grounds->or_having('attorney_count >' , 2);
				}
//				$grounds = $grounds->group_end();
			}

		}
        return $grounds;
    }

    public function filter()
    {
		$data['title'] = 'امار کاربران';
		$data['attorney_types'] = $this->M_AttorneyType->get_all_attorney_types();
		$data['filters'] = $this->input->get();

		$all_grounds = $this->create_filter_query($this->input->get())->get();

		$this->session->set_userdata('statistic_filter' , $this->input->get());
		$this->session->set_userdata('grounds' , json_encode($all_grounds->result_array()));

		$data['grounds'] = $all_grounds->result();
		$data['total_rows']  = $all_grounds->num_rows();

		$this->AdminView('statistic/statistic' , $data);
	}

	public function show_all_records($page = 0)
	{
		$data['title'] = 'امار کاربران';
		$this->load->library('pagination');
		$data['row_start'] = $page;
		$config['base_url'] = base_url('admin-dashboard/users-statistics/show-all');

		$config['total_rows'] = count(json_decode($this->session->userdata('grounds')));
		$data['grounds'] = $this->create_filter_query($this->session->userdata('statistic_filter'))->limit(100 , $page)->get()->result();
		$config['per_page'] = 100;

		$this->pagination->initialize($config);
		$this->AdminView('statistic/show_all_records' , $data);

	}

    /**
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
	public function output2()
	{
		$this->load->library("excel");
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		$table_columns = $this->input->post('columns');

		$column = 0;

		foreach($table_columns as $field)
		{
			switch($field){
				case 'membership_number' :
					$field = 'شماره عضویت';
					break;
				case 'attorney_number' :
					$field = 'شماره وکالت/سند';
					break;
				case 'attorney_date' :
					$field = 'تاریخ وکالت/سند';
					break;
				case 'bill_number' :
					$field = 'شماره فیش';
					break;
				case 'bill_date' :
					$field = 'تاریخ فیش';
					break;
				case 'tracking_code' :
					$field = 'کد رهگیری';
					break;
				case 'full_name' :
					$field = 'نام و نام خانوادگی';
					break;
				case 'phone_number' :
					$field = 'شماره تلفن همراه';
					break;
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		//      $grounds = $this->session->userdata('statistic_filter');
		//		$grounds = $this->create_filter_query($this->session->userdata('statistic_filter'))->get();
		$grounds = json_decode($this->session->userdata('grounds'));


		( ! $this->input->post('from_row')) ? $from_row = 1 : $from_row = $this->input->post('from_row');
		( ! $this->input->post('to_row')) ? $to_row = count($grounds) : $to_row = $this->input->post('to_row');
		$excel_row = 2;

		foreach($grounds as $index => $row)
		{
			if($index+1 >= $from_row && $index+1 <= $to_row)
			{
				foreach($this->input->post('columns') as $index => $column)
				{
					$object->getActiveSheet()->setCellValueByColumnAndRow($index, $excel_row, $row->$column);
				}
				$excel_row++;
			}
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Employee_Data.xls"');
		$object_writer->save('php://output');
		redirect(base_url('admin-dashboard/statistic-filter'));
	}

    public function output()
    {
		$sheet = new Spreadsheet();

		$sheet->setActiveSheetIndex(0);

		$table_columns = $this->input->post('columns');

		$column = 1;

		foreach($table_columns as $field)
		{
            switch($field){
                case 'membership_number' :
                    $field = 'شماره عضویت';
                    break;
                case 'attorney_number' :
                    $field = 'شماره وکالت/سند';
                    break;
                case 'attorney_date' :
                    $field = 'تاریخ وکالت/سند';
                    break;
                case 'bill_number' :
                    $field = 'شماره فیش';
                    break;
                case 'bill_date' :
                    $field = 'تاریخ فیش';
                    break;
                case 'tracking_code' :
                    $field = 'کد رهگیری';
                    break;
                case 'full_name' :
                    $field = 'نام و نام خانوادگی';
                    break;
                case 'phone_number' :
                    $field = 'شماره تلفن همراه';
                    break;
            }
			$sheet->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
		$grounds = json_decode($this->session->userdata('grounds'));


		( ! $this->input->post('from_row')) ? $from_row = 1 : $from_row = $this->input->post('from_row');
        ( ! $this->input->post('to_row')) ? $to_row = count($grounds) : $to_row = $this->input->post('to_row');
        $excel_row = 2;

        foreach($grounds as $index => $row)
        {
            if($index+1 >= $from_row && $index+1 <= $to_row)
            {
                foreach($this->input->post('columns') as $index => $column)
                {
                    $sheet->getActiveSheet()->setCellValueByColumnAndRow($index+1, $excel_row, $row->$column);
                }
                $excel_row++;
            }
        }

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Employee_Data.xlsx"');
		header('Cache-Control: max-age=0');

		$writer = new Xlsx($sheet);
		$writer->save('php://output');
        redirect(base_url('admin-dashboard/statistic-filter'));
    }
}
