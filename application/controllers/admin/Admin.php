<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/2/2018
 * Time: 3:59 PM
 */
include APPPATH."controllers/BaseController.php";
class Admin extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isMustBeLogin();
        $this->valid_access([1]);
    }

    public function admins_list()
    {
        $data['title'] = 'لیست ادمین ها';

        $data['admins'] = $this->M_Admin->get_all_admins();
        $this->AdminView('admins', $data);
    }
    public function admin_by_id ($admin_id)
    {
        $data['user']    = $this->M_Admin->get_admin_by_id($admin_id);
        $data['title']   = 'ویرایش گروه IT ';
        $this->AdminView('edit_admins', $data);
    }



    public function update_admins()
    {

        $posts = $this->input->post();

        if ($posts['pass'])
        {
            $this->db->where('id',$posts['user_id']);
            $this->db->update('admins',
                [
                    'fname'=>$posts['fname'],
                    'lname'=>$posts['lname'],
                    'access_lvl' => $posts['access_lvl'],
                    'pass'=>md5($posts['pass'])
                ]);
        }
        else
        {
            $this->db->where('id',$posts['user_id']);
            $this->db->update('admins',
                [
                    'fname'=>$posts['fname'],
                    'lname'=>$posts['lname'],
                    'access_lvl' => $posts['access_lvl'],
                ]);
        }

        $data['admins'] = $this->M_Admin->get_all_admins();
        $data['title'] = 'ادمین ها';

        $this->AdminView('admins', $data);

    }
    public function add_new_admin()
    {
        $data['title'] = 'ثبت ادمین جدید';

        $this->AdminView('new_admin' ,$data);
    }
    public function insert_admin()
    {
        $posts = $this->input->post();

        $fname                   =$posts['fname'];
        $lname                   =$posts['lname'];
        $access_lvl              =$posts['access_lvl'];
        $pass                    =$posts['pass'];
        $username                =$posts['username'];

        $result=$this->M_Admin->insert_admin($fname,$lname,$access_lvl,$pass,$username);
        if ($result){
            $this->session->set_flashdata('message','ادمین با موفقیت ثبت شد');
        }
        else
        {
            $this->session->set_flashdata('message','مشکلی در ثبت رخ داده است');
        }
        redirect(base_url('admin-dashboard/admin-list'));

    }

    public function remove_admin()
    {
        $admin_id = $this->input->post('admin_id');

        $this->M_Admin->change_status_admin($admin_id);
    }

}