<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['pre_controller'] = [
    'class' => 'language_changer',
    'function' => 'persian_to_eng',
    'filename' => 'language_changer.php',
    'filepath' => 'hooks'
];
$hook['post_controller'] = [
    'class' => 'language_changer',
    'function' => 'eng_to_persian',
    'filename' => 'language_changer.php',
    'filepath' => 'hooks'
];
