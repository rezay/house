<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/***********************************admin side **********************************/

$route['admin-dashboard/logout']                              = 'BaseController/logout';
$route['admin-dashboard/login']                               = 'BaseController/login';
$route['admin-dashboard']                                     = 'BaseController/index';
$route['check-login']['POST']                                 = 'BaseController/check_user_pass_login';
$route['admin-dashboard/admin-list']                          = 'admin/Admin/admins_list';
$route['admin-dashboard/add-new-admin']                       = 'admin/Admin/add_new_admin';
$route['insert-admin']                                        = 'admin/Admin/insert_admin';
$route['update-admin']['POST']                                = 'admin/Admin/update_admins';
$route['admin-edit/(:num)']                                   = 'admin/Admin/admin_by_id/$1';
$route['admin-dashboard/add-new-ground']                      = 'admin/Ground/add_new_ground';
$route['admin-dashboard/new-ground-process']                  = 'admin/Ground/new_ground_process';
$route['admin-dashboard/search-ground']                       = 'admin/Ground/search_ground';
$route['admin-dashboard/update-ground']                       = 'admin/Ground/update_ground';

$route['admin-dashboard/document-users/(:any)/(:num)']        = 'admin/Users/document_users/$1/$2';
$route['admin-dashboard/document-users/(:any)']               = 'admin/Users/document_users/$1/0';
$route['admin-dashboard/document-users']                      = 'admin/Users/document_users/all/0';
$route['admin-dashboard/attorney-users/(:any)/(:num)']        = 'admin/Users/attorney_users/$1/$2';
$route['admin-dashboard/attorney-users/(:any)']               = 'admin/Users/attorney_users/$1/0';
$route['admin-dashboard/attorney-users']                      = 'admin/Users/attorney_users/all/0';
$route['admin-dashboard/card-users/(:any)/(:num)']            = 'admin/Users/card_users/$1/$2';
$route['admin-dashboard/card-users/(:any)']                   = 'admin/Users/card_users/$1/0';
$route['admin-dashboard/card-users']                          = 'admin/Users/card_users/all/0';

$route['admin-dashboard/full-info-card-user/(:num)']          = 'admin/Users/full_info_card_user/$1';
$route['admin-dashboard/full-info-attorney-user/(:num)']      = 'admin/Users/full_info_attorney_user/$1';
$route['admin-dashboard/full-info-document-user/(:num)']      = 'admin/Users/full_info_document_user/$1';
$route['admin-dashboard/update-info']                         = 'admin/Users/update_info';
$route['admin-dashboard/update-debits']                       = 'admin/Users/update_debits';
$route['admin-dashboard/print/(:num)/(:num)']                 = 'admin/Users/print_page/$1/$2';
$route['admin-dashboard/solved-users/(:num)']                 = 'admin/Users/solved_users/$1';
$route['admin-dashboard/texts-setting']                       = 'admin/Settings/texts_setting';
$route['admin-dashboard/sms-setting']                         = 'admin/Settings/sms_setting';
$route['admin-dashboard/photo-setting']                       = 'admin/Settings/photo_setting';
$route['admin-dashboard/update-texts']                        = 'admin/Settings/update_texts';
$route['admin-dashboard/update-msg-texts']                    = 'admin/Settings/update_msg_texts';
$route['admin-dashboard/web-status']                          = 'admin/Settings/web_status';
$route['admin-dashboard/second-admin-access']                 = 'admin/Settings/second_admin_access';

$route['admin-dashboard/pelak-list']                          = 'admin/Pelak/pelaks_list';
$route['admin-dashboard/add-new-pelak']                       = 'admin/Pelak/add_new_pelak';
$route['insert-pelak']                                        = 'admin/Pelak/insert_pelak';
$route['update-pelak']['POST']                                = 'admin/Pelak/update_pelaks';
$route['pelak-edit/(:num)']                                   = 'admin/Pelak/pelak_by_id/$1';
$route['admin-dashboard/search']                              = 'admin/Users/search';
$route['admin-dashboard/site-guide']                          = 'admin/Guide/site_guide';
$route['admin-dashboard/update-guide']                        = 'admin/Guide/update_guide';

//*********************************user statistic******************************
$route['admin-dashboard/users-statistics/show-all']           = 'admin/Statistic/show_all_records';
$route['admin-dashboard/users-statistics/show-all/(:num)']    = 'admin/Statistic/show_all_records/$1';
$route['admin-dashboard/users-statistics']                    = 'admin/Statistic/filter';
$route['admin-dashboard/users-statistics-output']             = 'admin/Statistic/output';

//*********************************attorney-types*******************************
$route['admin-dashboard/attorney-type-list']                          = 'admin/AttorneyType/attorney_types_list';
$route['admin-dashboard/add-new-attorney-type']                       = 'admin/AttorneyType/add_new_attorney_type';
$route['insert-attorney-type']                                        = 'admin/AttorneyType/insert_attorney_type';
$route['update-attorney-type']['POST']                                = 'admin/AttorneyType/update_attorney_type';
$route['attorney-type-edit/(:num)']                                   = 'admin/AttorneyType/attorney_type_by_id/$1';

//*********************************payment-title*******************************
$route['admin-dashboard/payment-title-list']                          = 'admin/PaymentTitle/payment_titles_list';
$route['admin-dashboard/add-new-payment-title']                       = 'admin/PaymentTitle/add_new_payment_title';
$route['insert-payment-title']                                        = 'admin/PaymentTitle/insert_payment_title';
$route['update-payment-title']['POST']                                = 'admin/PaymentTitle/update_payment_title';
$route['payment-title-edit/(:num)']                                   = 'admin/PaymentTitle/payment_title_by_id/$1';

//*********************************payment-title*******************************
$route['admin-dashboard/build-state-list']                          = 'admin/BuildState/build_states_list';
$route['admin-dashboard/add-new-build-state']                       = 'admin/BuildState/add_new_build_state';
$route['insert-build-state']                                        = 'admin/BuildState/insert_build_state';
$route['update-build-state']['POST']                                = 'admin/BuildState/update_build_state';
$route['build-state-edit/(:num)']                                   = 'admin/BuildState/build_state_by_id/$1';


/************************************ admin ajax ************************************/

$route['admin-dashboard/remove-pelak']                       = 'admin/Pelak/remove_pelak';
$route['admin-dashboard/remove-attorney-type']               = 'admin/AttorneyType/remove_attorney_type';
$route['admin-dashboard/remove-payment-title']               = 'admin/PaymentTitle/remove_payment_title';
$route['admin-dashboard/remove-build-state']               = 'admin/BuildState/remove_build_state';
$route['admin-dashboard/remove-admin']                       = 'admin/Admin/remove_admin';
$route['upload-new-img']                                     = 'admin/Users/upload_new_image';
$route['black-list-seen/(:num)']                             = 'admin/Users/black_list_seen/$1';
$route['admin-dashboard/new-photo']                          = 'admin/Settings/new_photo';
$route['admin-dashboard/change-web-status']                  = 'admin/Settings/change_web_status';
$route['admin-dashboard/change-second-admin-access-status']  = 'admin/Settings/change_second_admin_access_status';
$route['admin-dashboard/change-reviewed']                    = 'admin/Users/change_reviewed';
$route['admin-dashboard/change-gold']                        = 'admin/Users/change_gold';
$route['admin-dashboard/change-silver']                      = 'admin/Users/change_silver';
$route['admin-dashboard/change-msg-to-user']                 = 'admin/Users/change_msg_to_user';
$route['admin-dashboard/change-solved']                      = 'admin/Users/change_solved';
$route['admin-dashboard/remove-user']                        = 'admin/Users/remove_user';
$route['admin-dashboard/accept-black-list']                  = 'admin/Users/accept_black_list';
$route['admin-dashboard/remove-black-list']                  = 'admin/Users/remove_black_list';
$route['admin-dashboard/change-status-bill-cheque']          = 'admin/Users/change_status_bill_cheque';


/*********************************user side**************************************/
$route['test']                                     = 'basic/test';
$route['attorney-login']                           = 'user/Login/attorney_login';
$route['card-login']                               = 'user/Login/card_login';
$route['document-login']                           = 'user/Login/document_login';
$route['login-by-userpass']                        = 'user/Login/login_by_userpass';
$route['log-out']                                  = 'user/Login/log_out';
$route['print']                                    = 'Basic/print_page';
$route['success-page']                             = 'Basic/success_page';
$route['solved-page']                              = 'Basic/solved_page';
$route['debit-page']                               = 'Basic/debit_page';
$route['pay']                                      = 'Basic/pay';
$route['password_reset']                           = 'Basic/password_reset';

$route['attorney-attorney-form']                   = 'user/Attorney/attorney_form';
$route['attorney-bills-cheques-form']              = 'user/Attorney/bills_cheques_form';
$route['attorney-user-info-form']                  = 'user/Attorney/user_info_form';

$route['card-card-form']                           = 'user/Card/card_form';
$route['card-bills-cheques-form']                  = 'user/Card/bills_cheques_form';
$route['card-user-info-form']                      = 'user/Card/user_info_form';

$route['document-attorney-form']                   = 'user/Document/attorney_form';
$route['document-document-form']                   = 'user/Document/document_form';
$route['document-certain-document-form']           = 'user/Document/certain_document_form';
$route['document-bills-cheques-form']              = 'user/Document/bills_cheques_form';
$route['document-user-info-form']                  = 'user/Document/user_info_form';


/********************************user ajax***************************************************/

$route['attorney-attorney-form-process']             = 'user/Attorney/attorney_form_process';
$route['attorney-bills-cheques-form-process']        = 'user/Attorney/bills_cheques_form_process';
$route['attorney-user-info-form-process']            = 'user/Attorney/user_info_form_process';

$route['card-card-form-process']                     = 'user/Card/card_form_process';
$route['card-bills-cheques-form-process']            = 'user/Card/bills_cheques_form_process';
$route['card-user-info-form-process']                = 'user/Card/user_info_form_process';

$route['document-attorney-form-process']             = 'user/Document/attorney_form_process';
$route['document-document-form-process']             = 'user/Document/document_form_process';
$route['document-certain-document-form-process']     = 'user/Document/certain_document_form_process';
$route['document-bills-cheques-form-process']        = 'user/Document/bills_cheques_form_process';
$route['document-user-info-form-process']            = 'user/Document/user_info_form_process';

$route['enable-status']                              = 'Basic/enable_status';
$route['delete-records']                             = 'Basic/delete_records';
$route['check-share-amount']                         = 'Basic/check_share_amount';




$route['default_controller'] = 'Basic';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
