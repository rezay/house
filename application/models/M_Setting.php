<?php

class M_Setting extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function get_site_title()
    {
        return $this->db->select('value')->from('settings')->where('item','title')->where('status',1)->get()->row();
    }
    public function get_home_setting()
    {
        return $this->db->select('*')->from('settings')->where('in_group','public')->or_where('in_group','home')->where('status',1)->get()->result();
    }
    public function get_sms_texts()
    {
        return $this->db->select('*')->from('settings')->where('in_group','sms')->where('status',1)->get()->result();
    }
    public function get_guides()
    {
        return $this->db->select('*')->from('settings')->where('in_group','guide')->where('status',1)->get()->result();
    }

    /************************************/

    public function change_status_setting($id)
    {
        $this->db->where('id',$id);
        $this->db->update('setting',array('status'=>0));
    }

}