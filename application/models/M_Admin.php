<?php

class M_Admin extends CI_Model {

    public function __construct(){

        parent::__construct();

    }
    public function get_all_admins()
    {
        return $this->db->select('*')->from('admins')->where('status', 1)->get()->result();
    }
    public function is_admin($username,$pass)
    {
        return $this->db->select('*')->from('admins')
            ->where('username',$username)
            ->where('pass',md5($pass))
			->where('status' , 1)
            ->get()->row();
    }
    public function get_admin_by_id($admin_id){

        return  $this->db->select('*')->from('admins')->where('id',$admin_id)->get()->row();
    }

    public function insert_admin($fname,$lname,$access_lvl,$pass,$username)
    {
        $exist = $this->db->select('*')->from('admins')->where('username',$username)->get()->num_rows();
        if($exist){
            return FALSE;
        }
        else
        {
            $pass = md5($pass);
            $this->db->insert('admins', array('fname'=>$fname,'lname'=>$lname,'access_lvl'=>$access_lvl,'pass'=>$pass,'username'=>$username));
            return TRUE;
        }
    }

    public  function change_status_admin ($id)
    {
        $this->db->where('id',$id);
        $this->db->update('admins',array('status'=>0));
    }

}
