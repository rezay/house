<?php

class M_PaymentTitle extends CI_Model {

    public function __construct(){

        parent::__construct();

    }
    public function get_all_payment_titles()
    {
        return $this->db->select('*,(SELECT count(*) FROM bills_cheques WHERE bills_cheques.payment_title_id = payment_titles.id) as count')->from('payment_titles')->where('status', 1)->get()->result();
    }
    public function get_payment_title_by_id($payment_title_id)
    {
        return  $this->db->select('*')->from('payment_titles')->where('id',$payment_title_id)->get()->row();
    }

    public function insert_payment_title($payment_title)
    {
        $exist = $this->db->select('*')->from('payment_titles')->where('payment_title',$payment_title)->get()->num_rows();
        if($exist){
            return FALSE;
        }
        else
        {
            $this->db->insert('payment_titles', array('payment_title'=>$payment_title));
            return TRUE;
        }
    }

    public  function change_status_payment_title ($id)
    {
        $this->db->where('id',$id);
        $this->db->update('payment_titles',array('status'=>0));
    }

}
