<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 4:07 PM
 */
class M_Card extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert_new_card($inputs , $type)
    {

        $card_data = [
            'ground_id' => $this->session->userdata('logged_user')['id'] ,
            'logged_user_id' => $this->session->userdata('logged_user')['log_id'],
            'card_color' => $inputs['card_color'] ,
            'bought_from' => $inputs['bought_from'] ,
            'bought_from_person' => toEnglishNum($inputs['bought_from_person']) ,
            'card_named' => $inputs['card_named'] ,
            'signatured' => $inputs['signatured'] ,
            'card_front_img' => $inputs['card_front_img'] ,
            'card_back_img' => $inputs['card_back_img'] ,
            'status' => 0
        ];
        $this->db->set('status' , 0)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->update('cards');
        $this->db->insert('cards' , $card_data);
        return $this->db->insert_id();
    }

    public function get_card()
    {
        return $this->db->select('*')->from('cards')->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->where('status' , 1)->get()->row();
    }
}