<?php

class M_BuildState extends CI_Model {

    public function __construct(){

        parent::__construct();

    }
    public function get_all_build_states()
    {
        return $this->db->select('*,(SELECT count(*) FROM grounds WHERE grounds.build_state_id = build_states.id) as count')->from('build_states')->where('status', 1)->get()->result();
    }
    public function get_build_state_by_id($build_state_id)
    {
        return  $this->db->select('*')->from('build_states')->where('id',$build_state_id)->get()->row();
    }

    public function insert_build_state($build_state)
    {
        $exist = $this->db->select('*')->from('build_states')->where('build_state',$build_state)->get()->num_rows();
        if($exist){
            return FALSE;
        }
        else
        {
            $this->db->insert('build_states', array('build_state'=>$build_state));
            return TRUE;
        }
    }

    public  function change_status_build_state ($id)
    {
        $this->db->where('id',$id);
        $this->db->update('build_states',array('status'=>0));
    }

}
