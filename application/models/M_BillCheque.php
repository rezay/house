<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 4:07 PM
 */
class M_BillCheque extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insert_new_bills_cheques($inputs , $type)
    {

        $bills_cheques_data = [
            'ground_id' => $this->session->userdata('logged_user')['id'] ,
            'logged_user_id' => $this->session->userdata('logged_user')['log_id'],
            'number' => toEnglishNum($inputs['number']) ,
            'amount' => parse_number(toEnglishNum($inputs['amount'])) ,
            'date' => toEnglishNum($inputs['date']) ,
            'place' => $inputs['place'] ,
            'payer_full_name' => $inputs['payer_full_name'] ,
            'img' => $inputs['img'] ,
            'type' => $inputs['type'] ,
            'status' => 0
        ];
        $this->db->set('status' , 0)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->update('bills_cheques');
        $this->db->insert('bills_cheques' , $bills_cheques_data);
        return $this->db->insert_id();
    }
    public function get_bills_cheques($logged_user_id)
    {
        return $this->db->select('*')->from('bills_cheques')->where('logged_user_id' , $logged_user_id)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('status' ,1)->get()->result();
    }
}