<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 4:07 PM
 */
class M_CertainDocument extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insert_new_certain_document($inputs , $type)
    {

        $certain_document_data = [
            'ground_id' => $this->session->userdata('logged_user')['id'] ,
            'logged_user_id' => $this->session->userdata('logged_user')['log_id'],
            'pacifier' => $inputs['pacifier'] ,
            'motasaleh' => $inputs['motasaleh'] ,
            'document_number' => toEnglishNum($inputs['document_number']) ,
            'document_date' => toEnglishNum($inputs['document_date']) ,
            'document_serial' => toEnglishNum($inputs['document_serial']) ,
            'office_number' => toEnglishNum($inputs['office_number']) ,
            'city' => $inputs['city'] ,
            'img' => $inputs['img'],
            'status' => 0
        ];
        $this->db->set('status' , 0)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->update('certain_documents');
        $this->db->insert('certain_documents' , $certain_document_data);
        return $this->db->insert_id();
    }
    public function get_certain_document($logged_user_id)
    {
        return $this->db->select('*')->from('certain_documents')->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('logged_user_id' , $logged_user_id)->where('status' ,1)->get()->row();
    }
}