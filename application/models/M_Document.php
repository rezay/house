<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 4:07 PM
 */
class M_Document extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insert_new_document($inputs , $type)
    {

        $document_data = [
            'ground_id' => $this->session->userdata('logged_user')['id'] ,
            'logged_user_id' => $this->session->userdata('logged_user')['log_id'],
            'pelak_sabti_number' => $inputs['pelak_sabti_number'] ,
            'ownership_number' => toEnglishNum($inputs['ownership_number']) ,
            'registration_number' => toEnglishNum($inputs['registration_number']) ,
            'office_number' => toEnglishNum($inputs['office_number']) ,
            'page_number' => toEnglishNum($inputs['page_number']) ,
            'full_name' => $inputs['full_name'] ,
            'document_number' => toEnglishNum($inputs['document_number']) ,
            'office' => toEnglishNum($inputs['office']) ,
            'date' => toEnglishNum($inputs['date']) ,
            'page1_img' => $inputs['page1_img'],
            'page2_img' => $inputs['page2_img'],
            'page3_img' => $inputs['page3_img'],
            'status' => 0
        ];
        $this->db->set('status' , 0)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->update('documents');
        $this->db->insert('documents' , $document_data);
        return $this->db->insert_id();
    }
    public function get_document($logged_user_id)
    {
        return $this->db->select('*')->from('documents')->where('logged_user_id' , $logged_user_id)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('status' ,1)->get()->result();
    }
}