<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/3/2018
 * Time: 3:11 PM
 */
class M_LoggedUser extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function check_and_add_to_logged_users($ground_id,$inputs)
    {
        $exist = $this->db->select('*')->from('logged_users')->where('status' ,1)->where('ground_id' ,$ground_id)->where('bill_number' , $inputs['bill_number'])->where('bill_date' , $inputs['bill_date'])->where('phone_number' , $inputs['phone_number'])->get();
        if($exist->num_rows() == 0)
        {
            $this->db->insert('logged_users' ,
                [
                    'ground_id' => $ground_id ,
                    'membership_number' => toEnglishNum($inputs['membership_number']) ,
                    'bill_number' => toEnglishNum($inputs['bill_number']) ,
                    'bill_date' => toEnglishNum($inputs['bill_date']) ,
                    'phone_number' => toEnglishNum($inputs['phone_number']) ,
                    'full_name' => $inputs['f_name']." ".$inputs['l_name'] ,
                ]
            );
            $id = $this->db->insert_id();
            return (object)[
                'id' => $id,
                'ground_id' => $ground_id ,
                'membership_number' => $inputs['membership_number'] ,
                'bill_number' => $inputs['bill_number'] ,
                'bill_date' => $inputs['bill_date'] ,
                'phone_number' => $inputs['phone_number'] ,
                'full_name' => $inputs['f_name']." ".$inputs['l_name'] ,
                'access_lvl' => 'basic'
            ];
        }
        else
        {
            return $exist->row();
        }
    }

    public function move_logged_users_to_black_list()
    {
        $logged_users = $this->db->select('*')->from('logged_users')->where('id !=' , $this->session->userdata('logged_user')['log_id'])->where('ground_id',$this->session->userdata('logged_user')['id'])->get()->result();
        foreach($logged_users as $logged_user)
        {
            $this->db->insert('black_list',[
                'ground_id' => $logged_user->ground_id ,
                'membership_number' => $logged_user->membership_number ,
                'bill_number' => $logged_user->bill_number ,
                'bill_date' => $logged_user->bill_date ,
                'phone_number' => $logged_user->phone_number ,
                'full_name' => $logged_user->full_name ,
                'seen' => 0 ,
                'bycot' => 0
            ]);
            $this->db->set('status' , 0)->where('id' , $logged_user->id)->update('logged_users');
            $this->db->set('status' , 0)->where('logged_user_id' , $logged_user->id)->update('user_meta');
            $this->db->set('status' , 0)->where('ground_id' , $logged_user->ground_id)->where('logged_user_id' , $logged_user->id)->update('documents');
            $this->db->set('status' , 0)->where('ground_id' , $logged_user->ground_id)->where('logged_user_id' , $logged_user->id)->update('certain_documents');
            $this->db->set('status' , 0)->where('ground_id' , $logged_user->ground_id)->where('logged_user_id' , $logged_user->id)->update('bills_cheques');
            $this->db->set('status' , 0)->where('ground_id' , $logged_user->ground_id)->where('logged_user_id' , $logged_user->id)->update('attorneys');
        }
    }

}