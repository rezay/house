<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 4:07 PM
 */
class M_Ground extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $attorneys = [];

    public function get_all_grounds($attorney_count)
    {
    	if($attorney_count)
		{
			return $this->db->select('grounds.*,count(grounds.id) as num_rows,count(attorneys.id) as attorney_count' , FALSE)->from('grounds')
				->join('attorneys' , 'grounds.id = attorneys.ground_id')
				->where('attorneys.status ' , 1)
				->where('grounds.tracking_code !=' , NULL)
				->group_by('grounds.id')
				->order_by('tracking_code' , 'ASC');
		}
    	else
		{
			return $this->db->select('grounds.*,count(grounds.id) as num_rows' , FALSE)->from('grounds')
				->where('grounds.tracking_code !=' , NULL)
				->group_by('grounds.id')
				->order_by('tracking_code' , 'ASC');
		}

    }
	public function get_all_grounds_old()
	{
		return $this->db->select('grounds.*,logged_users.*,count(grounds.id) as num_rows,count(attorneys.id) as attorney_count,logged_users.id as logged_user_id' , FALSE)->from('grounds')
			->join('logged_users' , 'grounds.id = logged_users.ground_id')
			->join('attorneys' , 'grounds.id = attorneys.ground_id')
			->where('attorneys.status' , 1)
			->where('grounds.tracking_code !=' , NULL)
			->where('logged_users.status' , 1)
			->group_by('grounds.id')
			->order_by('tracking_code' , 'ASC');
	}

    public function users_count($type,$user_type)
    {
        $this->db->select('grounds.id,(select count(*) from black_list where ground_id = grounds.id AND seen = 0 AND deleted = 0 AND bycot = 0) as unseen_black_list')->from('grounds')
            ->join('logged_users' , 'grounds.id = logged_users.ground_id');
        switch ($type)
        {
            case 'orange':
                $this->db->having('unseen_black_list !=' , 0)->where('reviewed' , 1);
                break;
            case 'blue':
                $this->db->having('unseen_black_list' , 0)->where('reviewed' , 1);
                break;
            case 'white':
                $this->db->where('reviewed' , 0);
                break;
            default :
                $this->db->where_in('reviewed' , [0,1]);
                break;
        }
        return $this->db
            ->where('grounds.tracking_code !=' , NULL)
            ->where('grounds.type', $user_type)
            ->where('grounds.solved' , 0)
            ->where('logged_users.status' , 1)
            ->get()->num_rows();
    }
    public function get_users($page,$type,$user_type)
    {
        $this->db->select('*,logged_users.id as logged_user_id,(select count(*) from black_list where ground_id = grounds.id AND seen = 0 AND deleted = 0 AND bycot = 0) as unseen_black_list')->from('grounds')
            ->join('logged_users' , 'grounds.id = logged_users.ground_id');
        switch ($type)
        {
            case 'orange':
                $this->db->having('unseen_black_list !=' , 0)->where('reviewed' , 1);
                break;
            case 'blue':
                $this->db->having('unseen_black_list' , 0)->where('reviewed' , 1);
                break;
            case 'white':
                $this->db->where('reviewed' , 0);
                break;
            default :
                $this->db->where_in('reviewed' , [0,1]);
                break;
        }
        return $this->db
            ->where('grounds.tracking_code !=' , NULL)
            ->where('grounds.type', $user_type)
            ->where('grounds.solved' , 0)
            ->where('logged_users.status' , 1)
            ->group_by('grounds.id')
            ->order_by('tracking_code' , 'ASC')
            ->limit(50 , $page)
            ->get()->result();
    }

    public function get_full_data_card($ground_id)
    {
        $info = $this->db->select('*')->from('grounds')
            ->join('logged_users' , 'grounds.id = logged_users.ground_id')
            ->where('grounds.tracking_code !=' , NULL)
            ->where('grounds.type', 2)
            ->where('logged_users.status' , 1)
            ->where('grounds.id',$ground_id)
            ->get()->result();

        $this->get_attorneys($ground_id);
        $logged_user_id = $this->db->select('id')->from('logged_users')->where('ground_id' , $ground_id)->where('status' , 1)->get()->row()->id;

        $user_meta = $this->db->select('*')->from('user_meta')->where('ground_id' , $ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->result();

        $card = $this->db->select('*')->from('cards')->where('ground_id' ,$ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->row();
        $bills_cheques = $this->db->select('*,(SELECT payment_title FROM payment_titles WHERE bills_cheques.payment_title_id = payment_titles.id) as payment_title')->from('bills_cheques')
            ->where('ground_id',$ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->result();
        foreach ($bills_cheques as $index => $b_c)
        {
            if($b_c->type == 0)
            {
                $bills_cheques[$index]->type  = 'فیش';
            }
            else
            {
                $bills_cheques[$index]->type  = 'چک';
            }
        }
        $black_list = $this->db->select('*')->from('black_list')
            ->where('ground_id' , $ground_id)->where('bycot' , 0)->order_by('deleted')->get()->result();

        $bycot_list = $this->db->select('*')->from('black_list')
            ->where('ground_id' , $ground_id)->where('bycot' , 1)->get()->result();

        return
            [
                'info' => $info ,
                'user_meta' => $user_meta ,
                'card' => $card ,
                'bills_cheques' => $bills_cheques ,
                'black_list' => $black_list ,
                'bycot_list' => $bycot_list
            ];
    }
    public function get_full_data_document($ground_id)
    {
        $info = $this->db->select('*')->from('grounds')
            ->join('logged_users' , 'grounds.id = logged_users.ground_id')
            ->where('grounds.tracking_code !=' , NULL)
            ->where('grounds.type', 0)
            ->where('grounds.id',$ground_id)
            ->where('logged_users.status' , 1)
            ->get()->result();
        $logged_user_id = $this->db->select('id')->from('logged_users')->where('ground_id' , $ground_id)->where('status' , 1)->get()->row()->id;

        $user_meta = $this->db->select('*')->from('user_meta')->where('ground_id' , $ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->result();

        $document = $this->db->select('*')->from('documents')
            ->where('ground_id' , $ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->result();
        if($document == NULL)
            $document = [];

        $certain_document = $this->db->select('*')->from('certain_documents')
            ->where('ground_id',$ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->row();
        if($certain_document == NULL)
            $certain_document = [];

        $bills_cheques = $this->db->select('*,(SELECT payment_title FROM payment_titles WHERE bills_cheques.payment_title_id = payment_titles.id) as payment_title')->from('bills_cheques')
            ->where('ground_id',$ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->result();
        foreach ($bills_cheques as $index => $b_c)
        {
            if($b_c->type == 0)
            {
                $bills_cheques[$index]->type  = 'فیش';
            }
            else
            {
                $bills_cheques[$index]->type  = 'چک';
            }
        }

        $this->get_attorneys($ground_id);

        $black_list = $this->db->select('*')->from('black_list')
            ->where('ground_id' , $ground_id)->where('bycot' , 0)->order_by('deleted')->get()->result();

        $bycot_list = $this->db->select('*')->from('black_list')
            ->where('ground_id' , $ground_id)->where('bycot' , 1)->get()->result();

        return
        [
            'info' => $info ,
            'user_meta' => $user_meta ,
            'document' => $document ,
            'certain_document' => $certain_document ,
            'bills_cheques' => $bills_cheques ,
            'attorneys' => $this->attorneys ,
            'black_list' => $black_list ,
            'bycot_list' => $bycot_list
        ];
    }
    public function get_full_data_attorney($ground_id)
    {
        $info = $this->db->select('*')->from('grounds')
            ->join('logged_users' , 'grounds.id = logged_users.ground_id')
            ->where('grounds.tracking_code !=' , NULL)
            ->where('grounds.type', 1)
            ->where('logged_users.status' , 1)
            ->where('grounds.id',$ground_id)
            ->get()->result();

        $user_meta = $this->db->select('*')->from('user_meta')->where('ground_id' , $ground_id)->where('status' , 1)->get()->result();

        $logged_user_id = $this->db->select('id')->from('logged_users')->where('ground_id' , $ground_id)->where('status' , 1)->get()->row()->id;
        $bills_cheques = $this->db->select('*,(SELECT payment_title FROM payment_titles WHERE bills_cheques.payment_title_id = payment_titles.id) as payment_title')->from('bills_cheques')
            ->where('ground_id',$ground_id)->where('logged_user_id' , $logged_user_id)->where('status' , 1)->get()->result();
        foreach ($bills_cheques as $index => $b_c)
        {
            if($b_c->type == 0)
            {
                $bills_cheques[$index]->type  = 'فیش';
            }
            else
            {
                $bills_cheques[$index]->type  = 'چک';
            }
        }

        $this->get_attorneys($ground_id);

        $black_list = $this->db->select('*')->from('black_list')
            ->where('ground_id' , $ground_id)->where('bycot' , 0)->order_by('deleted')->get()->result();

        $bycot_list = $this->db->select('*')->from('black_list')
            ->where('ground_id' , $ground_id)->where('bycot' , 1)->get()->result();

        return
            [
                'info' => $info ,
                'user_meta' => $user_meta ,
                'bills_cheques' => $bills_cheques ,
                'attorneys' => $this->attorneys ,
                'black_list' => $black_list ,
                'bycot_list' => $bycot_list
            ];
    }

    public function get_attorneys($ground_id , $id = 0 )
    {
        $result = $this->db->select('*')->from('attorneys')->where('ground_id', $ground_id)->where('status' , 1)->order_by('parent_id')->get()->result();

        $this->attorneys = $result;
    }
    public function search_for_ground($membership_number , $attorney_number , $attorney_date ,$type)
    {
        $arr = preg_split('/(?<=[0-9])(?=[A-Z]+)/i',$membership_number);
        if(isset($arr[1])){
            $membership_number1 = $arr[0].' '.$arr[1];
        }
        else
        {
           $membership_number1 = NULL;
        }
        $result = $this->db->select('*')->from('grounds')
            ->group_start()
            ->where('membership_number' ,$membership_number)
            ->or_where('membership_number' , $membership_number1)
            ->group_end()
            ->where('attorney_number' ,$attorney_number)
            ->where('attorney_date',$attorney_date)
            ->get();
        $result->type = $type;
        return $result;
    }
    public function search_for_ground_by_userpass($membership_number , $password)
    {
        $arr = preg_split('/(?<=[0-9])(?=[A-Z]+)/i',$membership_number);
        if(isset($arr[1])){
            $membership_number1 = $arr[0].' '.$arr[1];
        }
        else
        {
            $membership_number1 = NULL;
        }
        return $this->db->select('*')->from('grounds')
            ->group_start()
            ->where('membership_number' ,$membership_number)
            ->or_where('membership_number' , $membership_number1)
            ->group_end()
            ->where('password' ,md5($password))
            ->get();
    }
}
