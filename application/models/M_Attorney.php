<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 4:07 PM
 */
class M_Attorney extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert_new_attorney($inputs , $type)
    {
        if($inputs['parent_id'] == 0)
        {
            $delegated = 'شرکت تعاونی مسکن کارگران برق منطقه ای تهران';
        }
        else
        {
            $exist = $this->db->select('attorney')->from('attorneys')->where('ground_id' , $inputs['ground_id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->where('parent_id',$inputs['parent_id']-1)->get();
            if($exist->num_rows() > 0)
            {
                $delegated = $exist->row()->attorney;
            }
            else
            {
                $delegated = '';
            }

        }
        $attorney_data = [
            'ground_id' => $inputs['ground_id'] ,
            'logged_user_id' => $this->session->userdata('logged_user')['log_id'],
            'delegated' => $delegated ,
            'attorney' => $inputs['attorney'] ,
            'attorney_number' => toEnglishNum($inputs['attorney_number']) ,
            'date' => toEnglishNum($inputs['date']) ,
            'attorney_serial' => toEnglishNum($inputs['attorney_serial']) ,
            'office_number' => toEnglishNum($inputs['office_number']) ,
            'city' => $inputs['city'] ,
            'img' => $inputs['img'],
            'parent_id' => $inputs['parent_id'],
            'status' => 0
        ];
        $this->db->set('status' , 0)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->update('attorneys');
        $this->db->insert('attorneys' , $attorney_data);
        return $this->db->insert_id();
    }

    public function get_user_attorneys($logged_user_id)
    {
        $empties = $this->db->select('*')->from('attorneys')->where('logged_user_id' , $logged_user_id)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('status' ,1)->where('delegated' ,'')->get()->result();
        foreach($empties as $empty)
        {
            $delegated= $this->db->select('attorney')->from('attorneys')->where('ground_id' , $empty->ground_id)->where('logged_user_id' , $empty->logged_user_id )->where('status' , 1)->where('parent_id' , $empty->parent_id-1)->get()->row()->attorney;
            $this->db->set('delegated' , $delegated)->where('id' , $empty->id)->update('attorneys');
        }
        return $this->db->select('*')->from('attorneys')->where('logged_user_id' , $logged_user_id)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('status' ,1)->order_by('parent_id')->get()->result();
    }
}