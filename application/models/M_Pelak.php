<?php

class M_Pelak extends CI_Model {

    public function __construct(){

        parent::__construct();

    }
    public function get_all_pelaks()
    {
        return $this->db->select('*')->from('pelaks')->where('status', 1)->get()->result();
    }
    public function get_pelak_by_id($pelak_id){

        return  $this->db->select('*')->from('pelaks')->where('id',$pelak_id)->get()->row();
    }

    public function insert_pelak($pelak)
    {
        $exist = $this->db->select('*')->from('pelaks')->where('pelak',$pelak)->get()->num_rows();
        if($exist){
            return FALSE;
        }
        else
        {
            $this->db->insert('pelaks', array('pelak'=>$pelak));
            return TRUE;
        }
    }

    public  function change_status_pelak ($id)
    {
        $this->db->where('id',$id);
        $this->db->update('pelaks',array('status'=>0));
    }

}