<?php

class M_attorneyType extends CI_Model {

    public function __construct(){

        parent::__construct();

    }
    public function get_all_attorney_types()
    {
        return $this->db->select('*,(SELECT count(*) FROM grounds WHERE grounds.attorney_type_id = attorney_types.id) as count')->from('attorney_types')->where('status', 1)->get()->result();
    }
    public function get_attorney_type_by_id($attorney_type_id)
    {
        return  $this->db->select('*')->from('attorney_types')->where('id',$attorney_type_id)->get()->row();
    }

    public function insert_attorney_type($attorney_type)
    {
        $exist = $this->db->select('*')->from('attorney_types')->where('attorney_type',$attorney_type)->get()->num_rows();
        if($exist){
            return FALSE;
        }
        else
        {
            $this->db->insert('attorney_types', array('attorney_type'=>$attorney_type));
            return TRUE;
        }
    }

    public  function change_status_attorney_type ($id)
    {
        $this->db->where('id',$id);
        $this->db->update('attorney_types',array('status'=>0));
    }

}
