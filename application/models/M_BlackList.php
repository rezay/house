<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/3/2018
 * Time: 3:11 PM
 */
class M_BlackList extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function check_and_add_to_black_list($ground_id,$inputs)
    {
        $black_list = $this->db->select('*')->from('black_list')
            ->where('national_code' ,toEnglishNum($inputs['national_code']))
            ->where('ground_id' ,$ground_id)
            ->get();
        if($black_list->num_rows())
        {
            if($black_list->row()->deleted == 1)
            {
                if($black_list->row()->duplicate_insert_after_evaluation < 3)
                {
                    $this->db->where('id' , $black_list->row()->id)->update('black_list',['duplicate_insert_after_evaluation' => $black_list->row()->duplicate_insert_after_evaluation+1]);
                }
            }
            else
            {
                $this->db->where('id' , $black_list->row()->id)->update('black_list',['duplicate_insert_before_evaluation' => $black_list->row()->duplicate_insert_before_evaluation+1]);
            }
        }
        else
        {
            $this->db->insert('black_list' ,
                [
                    'ground_id' => $ground_id ,
                    'membership_number' => toEnglishNum($inputs['membership_number']) ,
                    'bill_number' => toEnglishNum($inputs['bill_number']) ,
                    'bill_date' => toEnglishNum($inputs['bill_date']) ,
                    'phone_number' => toEnglishNum($inputs['phone_number']) ,
                    'full_name' => $inputs['f_name']." ".$inputs['l_name'] ,
                    'national_code' => $inputs['national_code'],
                    'seen' => 0 ,
                    'bycot' => 0,
                    'duplicate_insert_before_evaluation' => 0,
                    'duplicate_insert_after_evaluation' => 0
                ]
            );
        }
    }

}