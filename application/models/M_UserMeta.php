<?php

/**
 * Created by PhpStorm.
 * User: hass
 * Date: 1/1/2018
 * Time: 4:07 PM
 */
class M_UserMeta extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
        public function insert_new_user($inputs , $type)
    {
        if ($type === 'document')
        {
            $share_amount = $inputs['share_amount'];
            $membership_card_img_front = $inputs['membership_card_img_front'];
            $membership_card_img_back = $inputs['membership_card_img_back'];
        }
        elseif($type === 'attorney')
        {
            $share_amount = $inputs['share_amount'];
            $membership_card_img_front = $inputs['membership_card_img_front'];
            $membership_card_img_back = $inputs['membership_card_img_back'];
        }
        else
        {
            $share_amount = NULL;
            $membership_card_img_front = NULL;
            $membership_card_img_back = NULL;
        }

        $meta_data = [
            'ground_id' => $this->session->userdata('logged_user')['id'],
            'logged_user_id' => $this->session->userdata('logged_user')['log_id'],
            'first_name' => $inputs['first_name'],
            'last_name' => $inputs['last_name'],
            'father_name' => $inputs['father_name'],
            'birth_certif_number' => toEnglishNum($inputs['birth_certif_number']),
            'national_code' => toEnglishNum($inputs['national_code']),
            'birth_date' => toEnglishNum($inputs['birth_date']),
            'address' => $inputs['address'],
            'postal_code' => toEnglishNum($inputs['postal_code']),
            'home_number' => toEnglishNum($inputs['home_number']),
            'phone_number' => toEnglishNum($inputs['phone_number']),
            'share_amount' => toEnglishNum($share_amount) ,
            'birth_certificate_img' => $inputs['birth_certificate_img'],
            'national_card_img' => $inputs['national_card_img'],
            'membership_card_img_front' => $membership_card_img_front,
            'membership_card_img_back' => $membership_card_img_back,
        ];
        $this->db->set('status' , 0)->where('ground_id',$this->session->userdata('logged_user')['id'])->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->update('user_meta');
        $this->db->insert('user_meta' , $meta_data);
        return $this->db->insert_id();
    }

        public function get_users_info()
    {
        return $this->db->select('*')->from('user_meta')
            ->where('logged_user_id' , $this->session->userdata('logged_user')['log_id'])->where('ground_id' , $this->session->userdata('logged_user')['id'])->where('status' ,1)->order_by('user_id')->get()->result();
    }

}