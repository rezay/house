@layout('admin_side/layout')

@section('title')
{{$title}}
@endsection
@section('contents')
    @include('admin_side/slideright')
    <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
        @include('admin_side/navheader')
        @include('admin_side/page-heading')
        <div id="wrapper">
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="middle-box text-center animated fadeInDown">
                    <h1>403</h1>
                    <h3 class="font-bold">شما به این صفحه دسترسی ندارید</h3>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection