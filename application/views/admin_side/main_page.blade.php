@layout('admin_side/layout')

@section('title')
  {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')

        </div>
    </div>
@endsection

@section('scripts')

@endsection