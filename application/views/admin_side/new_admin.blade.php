@layout('admin_side/layout')
@section('title')
    {{$title}}
@endsection
@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-6">
                        <form action="{{base_url('insert-admin')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2"  for="fname">نام:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"  name="fname" id="fname" placeholder="نام را وارد کنید">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2"  for="lname">نام خانوادگی:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"  name="lname" id="lname" placeholder="نام خانوادگی را وارد کنید">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2"  for="access_lvl">سطح دسترسی:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="access_lvl" id="access_lvl" >
                                        <option value="1">سطح اول</option>
                                        <option value="2">سطح دوم</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2"  for="username">نام کاربری:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"  name="username" id="username" placeholder="نام کاربری را وارد کنید">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2"  for="pass">پسورد:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"  name="pass" id="pass" placeholder="پسورد را وارد کنید">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                <button name="submit" class="btn btn-info" style="width:100%" role="button">ثبت</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection

