@layout('admin_side/layout')

@section('title')
    {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن پیام زمان دریافت کد رهگیری</h2>
                            <form action="{{base_url('admin-dashboard/update-msg-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="lvl1">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($lvl1)) echo $lvl1; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن پیام زمان تایید به مرحله نقره ای</h2>
                            <form action="{{base_url('admin-dashboard/update-msg-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="lvl2">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($lvl2)) echo $lvl2; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن پیام زمان تایید به مرحله طلایی</h2>
                            <form action="{{base_url('admin-dashboard/update-msg-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="lvl3">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($lvl3)) echo $lvl3; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن پیام زمان تایید به حضور در دفترخانه</h2>
                            <form action="{{base_url('admin-dashboard/update-msg-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="lvl4">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($lvl4)) echo $lvl4; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن پیام زمان ارسال به بایگانی راکد</h2>
                            <form action="{{base_url('admin-dashboard/update-msg-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="lvl5">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($lvl5)) echo $lvl5; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
