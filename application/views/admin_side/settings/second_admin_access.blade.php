@layout('admin_side/layout')

@section('title')
  {{$title}}
@endsection

@section('contents')
    @include('admin_side/slideright')
    <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
        @include('admin_side/navheader')
        @include('admin_side/page-heading')
        <div id="wrapper">
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{$title}}</h5>
                            </div>

                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                        <tr>
                                            <th>وضعیت</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($second_admin_access == 0)
                                            <td>
                                                <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                   onclick="{{'change_status(1)'}}"> غیر فعال </a>
                                            </td>
                                        @elseif($second_admin_access == 1)
                                            <td>
                                                <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                   onclick="{{'change_status(0)'}}"> فعال </a>
                                            </td>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function change_status(status)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');
            $.post("{{base_url('admin-dashboard/change-second-admin-access-status')}}", {status : status}, function ()
            {
                location.reload();
            });
        }
    </script>

@endsection