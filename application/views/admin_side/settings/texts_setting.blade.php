@layout('admin_side/layout')

@section('title')
    {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">خط اول متن بالای صفحات</h2>
                            <form action="{{base_url('admin-dashboard/update-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="header_first_line">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($header_first_line)) echo $header_first_line; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">خط سوم متن بالای صفحات</h2>
                            <form action="{{base_url('admin-dashboard/update-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="header_third_line">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($header_third_line)) echo $header_third_line; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">شماره ثبت</h2>
                            <form action="{{base_url('admin-dashboard/update-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="shomare_sabt">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($shomare_sabt)) echo $shomare_sabt; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن پایین صفحه</h2>
                            <form action="{{base_url('admin-dashboard/update-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="footer_text">
                                        <input type="text" class="form-control" name="text" id="value" value="<?php if(isset($footer_text)) echo $footer_text; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن بالای صفحه و صاحب متن</h2>
                            <form action="{{base_url('admin-dashboard/update-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="emam_text">
                                        <input type="text" class="form-control" name="emam_text" id="value" value="<?php if(isset(json_decode($emam_text)[0])) echo json_decode($emam_text)[0]; ?>" placeholder="مقدار را وارد کنید">
                                        <input type="text" class="form-control" name="emam" id="value" value="<?php if(isset(json_decode($emam_text)[1])) echo json_decode($emam_text)[1]; ?>" placeholder="مقدار را وارد کنید">
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2 style="color:black;padding-bottom: 20px;">متن کنار فرم ورود</h2>
                            <form action="{{base_url('admin-dashboard/update-texts')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label style="text-align: right" class="control-label col-sm-3"  for="value">مقدار:</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" name="item" value="beside_login">
                                        <textarea name="text" id="value"><?php if(isset($beside_login)) echo $beside_login; ?></textarea>
                                    </div>
                                </div>
                                <button name="submit" class="btn btn-primary btn-rounded btn-block" >ثبت</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
