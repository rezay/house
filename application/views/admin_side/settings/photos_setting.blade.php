@layout('admin_side/layout')

@section('title')
    {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif

            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>تنظیم لوگو سایت <small></small></h5>

                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>ثبت لوگو جدید</h4>

                                    <form enctype="multipart/form-data" id="my-awesome-dropzone" class="dropzone dz-clickable" action="{{base_url().'assets/backend/images/Upload2.php'}}">
                                        <div class="dropzone-previews"></div>
                                        <button type="submit" id="logo" class="btn btn-primary pull-left">ارسال</button>
                                        <div class="dz-default dz-message"><span>عکس را بکشید به این قسمت و رها کنید</span></div></form>
                                    <div>
                                        <div class="m text-right"> </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>تنظیم عکس امام <small></small></h5>

                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>ثبت عکس امام</h4>

                                    <form enctype="multipart/form-data" id="my-awesome-dropzone" class="dropzone dz-clickable" action="{{base_url().'assets/backend/images/Upload2.php'}}">
                                        <div class="dropzone-previews"></div>
                                        <button type="submit" id="emam_img" class="btn btn-primary pull-left">ارسال</button>
                                        <div class="dz-default dz-message"><span>عکس را بکشید به این قسمت و رها کنید</span></div></form>
                                    <div>
                                        <div class="m text-right"> </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){

        Dropzone.options.myAwesomeDropzone = {

            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 10,
            maxFiles: 1,
            url:'{{base_url()}}assets/backend/images/Upload2.php',
            // Dropzone settings
            init: function() {
                var myDropzone = this;
                this.options.url = '{{base_url()}}assets/backend/images/Upload2.php';
                this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                    item = $(this).attr('id');
                });
                this.on("sendingmultiple", function() {
                    console.log('sending');
                });
                this.on("successmultiple", function(files, response) {
                    this.options.url = '{{base_url()}}assets/backend/images/Upload2.php';
                });

                this.on("errormultiple", function(files, response) {
                    console.log('error:',response);
                });
                this.on("success", function(files, response) {
                    insert_image(response,item);
                    console.log(response);
                    this.options.url = '{{base_url()}}assets/backend/images/Upload2.php';
                });
            }
        }
    });


    function insert_image(file_name , item)
    {
        $.post("{{base_url('admin-dashboard/new-photo')}}", {file_name:file_name , item : item}, function (data) {

        });
    }

</script>
@endsection
