@layout('admin_side/layout')

@section('title')
    {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')

            <div class="col-lg-12" >
                @if(isset($message))
                    <div class="alert-danger" style="padding : 10px;">{{$message}}</div>
                @endif
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">اطلاعات اصلی</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">اطلاعات تکمیلی شرکا</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-5">اطلاعات کارت</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-6">مالی</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-9">تعیین بدهی ها</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-7" id="black_list">لیست بررسی سیاه</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-8">لیست بایکوت</a></li>
                        <li>
                            @if($user->reviewed == 0)
                                <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                   onclick="change_reviewed('{{$user->id}}' , 1)"> بررسی نشده </a>
                            @elseif($user->reviewed == 1)
                                <a style="float: right" class="btn btn-sm btn-success inactive-slider"
                                   onclick="change_reviewed('{{$user->id}}' , 0)"> بررسی شده </a>
                            @endif
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <form action="{{base_url('admin-dashboard/update-info')}}" method="post">
                                            <input type="hidden" name="table" value="grounds">
                                            <input type="hidden" name="id" value="{{$ground_id}}">
                                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                <tbody>
                                                <?php
                                                foreach ($full_info_user['info'][0] as $index => $info)
                                                {
                                                    switch ($index)
                                                    {
                                                        case 'personal_photo' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>عکس پرسنلی</td>';
                                                            echo '<td><button type="button" class="btn btn-primary show" data-toggle="modal" name="personal_photo/'.$ground_id.'" data-target="#showimage" id="'.$info.'">مشاهده</button></td>';                                                                                echo '</tr>';
                                                            break;
                                                        case 'membership_number' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>شماره عضویت</td>';
                                                            echo '<td>'.$info.'</td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'attorney_number' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>شماره سند</td>';
                                                            echo '<td>'.$info.'</td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'attorney_date' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>تاریخ سند</td>';
                                                            echo '<td>'.$info.'</td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'bill_number' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>شماره فیش</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="bill_number" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'bill_date' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>تاریخ فیش</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="bill_date" value="'.$info.'" placeholder="xxxx/xx/xx" class="date_txt"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'full_name' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>نام نام خانوادگی</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="full_name" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'phone_number' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>شماره موبایل کاربر</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0)" type="text" name="phone_number" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'description' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>توضیحات</td>';
                                                            echo '<td>'.$info.'</td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'tracking_code' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>کد رهگیری</td>';
                                                            echo '<td id="tracking_code">'.$info.'</td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'build_state_id' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>وضعیت عمرانی</td>';
                                                            echo '<td><select style="border : 0;background-color:rgba(1,1,1,0);width : 50%" name="build_state_id"><option value="0"></option>';
                                                            foreach ($build_states as $build_state)
                                                            {
                                                                echo '<option value="'.$build_state->id.'" '.($build_state->id == $info ? "selected":"").'>'.$build_state->build_state.'</option>';
                                                            }
                                                            echo '</select></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'delivery_date' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>تاریخ تحویل زمین</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0)" type="text" name="delivery_date" value="'.$info.'" placeholder="xxxx/xx/xx" class="date_txt"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'ground_number' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>شماره قطعه</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="ground_number" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'attorney_type_id' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>نوع وکالت</td>';
                                                            echo '<td><select style="border : 0;background-color:rgba(1,1,1,0);width : 50%" name="attorney_type_id"><option value="0"></option>';
                                                            foreach ($attorney_types as $attorney_type)
                                                            {
                                                                echo '<option value="'.$attorney_type->id.'" '.($attorney_type->id == $info ? "selected":"").'>'.$attorney_type->attorney_type.'</option>';
                                                            }
                                                            echo '</select></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'password' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>رمز عبور</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="password"></td>';
                                                            echo '</tr>';
                                                            break;
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                            <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"><strong>اعمال تغییرات</strong></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            @foreach($full_info_user['user_meta'] as $index => $value)
                                                <li class="<?php if($index == 0){ echo 'active' ;} ?>"><a data-toggle="tab" href="#sub-tab-{{$index}}"> شریک{{$index+1}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            @foreach($full_info_user['user_meta'] as $index => $value)
                                                <div id="sub-tab-{{$index}}" class="tab-pane <?php if($index == 0){ echo 'active'; } ?>">
                                                    <div class="panel-body">
                                                        <div class="ibox-content">
                                                            <div class="table-responsive">
                                                                <form action="{{base_url('admin-dashboard/update-info')}}" method="post">
                                                                    <input type="hidden" name="table" value="user_meta">
                                                                    <input type="hidden" name="user_id" value="{{$value->user_id}}">
                                                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                                        <tbody>
                                                                        <?php
                                                                        foreach ($full_info_user['user_meta'][$index] as $index => $info)
                                                                        {
                                                                            switch ($index)
                                                                            {
                                                                                case 'first_name' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>نام</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="first_name" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'last_name' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>نام خانوادگی</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="last_name" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'father_name' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>نام پدر</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="father_name" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'birth_certif_number' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>شماره شناسنامه</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="birth_certif_number" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'national_code' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>کد ملی</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="national_code" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'birth_date' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>تاریخ تولد</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="birth_date" value="'.$info.'" placeholder="xxxx/xx/xx" class="date_txt"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'address' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>ادرس</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="address" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'postal_code' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>کد پستی</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="postal_code" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'home_number' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>تلفن ثابت</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="home_number" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'phone_number' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>تلفن همراه</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="phone_number" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'birth_certificate_img' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>عکس شناسنامه</td>';
                                                                                    echo '<td><button type="button" class="btn btn-primary show" data-toggle="modal" name="birth_certificate_img/'.$value->user_id.'" data-target="#showimage" id="'.$info.'">مشاهده</button></td>';                                                                                echo '</tr>';
                                                                                    break;
                                                                                case 'national_card_img' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>عکس کارت ملی</td>';
                                                                                    echo '<td><button type="button" class="btn btn-primary show" data-toggle="modal" name="national_card_img/'.$value->user_id.'" data-target="#showimage" id="'.$info.'">مشاهده</button></td>';                                                                                echo '</tr>';
                                                                                    break;
                                                                                case 'membership_card_img' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>عکس کارت عضویت</td>';
                                                                                    echo '<td><button type="button" class="btn btn-primary show" data-toggle="modal" name="membership_card_img/'.$value->user_id.'" data-target="#showimage" id="'.$info.'">مشاهده</button></td>';                                                                                echo '</tr>';
                                                                                    break;
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"><strong>اعمال تغییرات</strong></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <form action="{{base_url('admin-dashboard/update-info')}}" method="post">
                                            <input type="hidden" name="table" value="cards">
                                            <input type="hidden" name="id" value="{{$full_info_user['card']->id}}">
                                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                <tbody>
                                                <?php
                                                foreach ($full_info_user['card'] as $index => $info)
                                                {
                                                    switch ($index)
                                                    {
                                                        case 'card_color' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>رنگ کارت</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="card_color" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'bought_from' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>خریداری شده از :</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="bought_from" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'bought_from_person' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>نام نام خانوادگی شخص :</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="bought_from" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'card_named' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>به نام شده</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="card_named" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'signatured' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>چند امضا</td>';
                                                            echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="signatured" value="'.$info.'"></td>';
                                                            echo '</tr>';
                                                            break;
                                                        case 'card_front_img' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>تصویر پشت کارت</td>';
                                                            echo '<td><button type="button" class="btn btn-primary show" data-toggle="modal" name="card_front_img/'.$full_info_user['info'][0]->id.'" data-target="#showimage" id="'.$info.'">مشاهده</button></td>';                                                            echo '</tr>';
                                                            break;
                                                        case 'card_back_img' :
                                                            echo '<tr class="gradeX">';
                                                            echo '<td>تصویر روی کارت</td>';
                                                            echo '<td><button type="button" class="btn btn-primary show" data-toggle="modal" name="card_back_img/'.$full_info_user['info'][0]->id.'" data-target="#showimage" id="'.$info.'">مشاهده</button></td>';                                                            echo '</tr>';
                                                            break;
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                            <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"><strong>اعمال تغییرات</strong></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-6" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            @foreach($full_info_user['bills_cheques'] as $index => $value)
                                                <li class="<?php if($index == 0){ echo 'active' ;} ?>"><a data-toggle="tab" href="#sub3-tab-{{$index}}"> {{$index+1}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            @foreach($full_info_user['bills_cheques'] as $index => $value)
                                                <div id="sub3-tab-{{$index}}" class="tab-pane <?php if($index == 0){ echo 'active'; } ?>">
                                                    <div class="panel-body">
                                                        <div class="ibox-content">
                                                            <div class="table-responsive">
                                                                <form action="{{base_url('admin-dashboard/update-info')}}" method="post">
                                                                    <input type="hidden" name="table" value="bills_cheques">
                                                                    <input type="hidden" name="id" value="{{$value->id}}">
                                                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                                        <tbody>
                                                                        <?php
                                                                        foreach ($full_info_user['bills_cheques'][$index] as $index => $info)
                                                                        {
                                                                            switch ($index)
                                                                            {
                                                                                case 'number' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>به شماره</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="number" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'amount' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>مبلغ(ریال)</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="amount" value="'.number_format($info,0,'','/').'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'place' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>محل پرداخت</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="place" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'date' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>تاریخ </td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="date" value="'.$info.'" placeholder="xxxx/xx/xx" class="date_txt"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'payer_full_name' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>پرداخت کننده</td>';
                                                                                    echo '<td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="payer_full_name" value="'.$info.'"></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'payment_title_id' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>عنوان پرداختی</td>';
                                                                                    echo '<td><select style="border : 0;background-color:rgba(1,1,1,0);width : 50%" name="payment_title_id"><option value="0"></option>';
                                                                                    foreach ($payment_titles as $payment_title)
                                                                                    {
                                                                                        echo '<option value="'.$payment_title->id.'" '.($payment_title->id == $info ? "selected":"").'>'.$payment_title->payment_title.'</option>';
                                                                                    }
                                                                                    echo '</select></td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'img' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>عکس </td>';
                                                                                    echo '<td><button type="button" class="btn btn-primary show" data-toggle="modal" name="img/'.$value->id.'" data-target="#showimage" id="'.$info.'">مشاهده</button></td>';                                                                                echo '</tr>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                                case 'type' :
                                                                                    echo '<tr class="gradeX">';
                                                                                    echo '<td>نوع</td>';
                                                                                    echo '<td>'.$info.'</td>';
                                                                                    echo '</tr>';
                                                                                    break;
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs change_status_bill_cheque" id="{{$value->id}}"><strong><?php if($value->accepted == 1){echo 'لغو صحت پرداخت';}else{echo 'تایید صحت پرداخت';} ?></strong></button>
                                                                    <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"><strong>اعمال تغییرات</strong></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-9" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#sub9-tab-1"> تعیین مبلغ اولیه (نقره ای)</a></li>
                                            <li><a data-toggle="tab" href="#sub9-tab-2"> تعیین مبلغ تسویه حساب (طلایی)</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="sub9-tab-1" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="ibox-content">
                                                        <div class="table-responsive">
                                                            <form action="{{base_url('admin-dashboard/update-debits')}}" method="post">
                                                                <input type="hidden" name="type" value="0">
                                                                <input type="hidden" name="ground_id" value="{{$ground_id}}">
                                                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                                    <tbody>
                                                                    @if( ! empty($silver_debits))
                                                                        @foreach($silver_debits as $debit)
                                                                            <tr class="gradeX">
                                                                                <td>مبلغ اولیه(ریال)</td>
                                                                                <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="silver_debit" value="{{number_format($debit->debit_amount,0,'','/')}}"></td>
                                                                            </tr>

                                                                            <tr class="gradeX">
                                                                                <td>جمع واریزی به تایید رسیده عضو(ریال)</td>
                                                                                <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="accepted_payment" value="{{number_format($debit->accepted_payment,0,'','/')}}"></td>
                                                                            </tr>
                                                                            <tr class="gradeX">
                                                                                <td>وضعیت پرداخت</td>
                                                                                <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" disabled value="<?php if($debit->payed == 1){ echo 'پرداخت شده';}else{ echo 'پرداخت نشده';} ?>"></td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <tr class="gradeX">
                                                                            <td>مبلغ اولیه(ریال)</td>
                                                                            <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="silver_debit"></td>
                                                                        </tr>

                                                                        <tr class="gradeX">
                                                                            <td>جمع واریزی به تایید رسیده عضو(ریال)</td>
                                                                            <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="accepted_payment"></td>
                                                                        </tr>
                                                                    @endif
                                                                    </tbody>
                                                                </table>
                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"><strong>اعمال تغییرات</strong></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="sub9-tab-2" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox-content">
                                                        <div class="table-responsive">
                                                            <form action="{{base_url('admin-dashboard/update-debits')}}" method="post">
                                                                <input type="hidden" name="type" value="1">
                                                                <input type="hidden" name="ground_id" value="{{$ground_id}}">
                                                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                                    <tbody>
                                                                    @if( ! empty($gold_debits))
                                                                        @foreach($gold_debits as $debit)
                                                                            <tr class="gradeX">
                                                                                <td>مبلغ قابل پرداخت(ریال)</td>
                                                                                <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="gold_debit" value="{{number_format($debit->debit_amount,0,'','/')}}"></td>
                                                                            </tr>
                                                                            <tr class="gradeX">
                                                                                <td>وضعیت پرداخت</td>
                                                                                <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" disabled value="<?php if($debit->payed == 1){ echo 'پرداخت شده';}else{ echo 'پرداخت نشده';} ?>"></td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <tr class="gradeX">
                                                                            <td>مبلغ قابل پرداخت(ریال)</td>
                                                                            <td><input style="border : 0;background-color:rgba(1,1,1,0);width : 80%" type="text" name="gold_debit" value=""></td>
                                                                        </tr>
                                                                    @endif
                                                                    </tbody>
                                                                </table>
                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"><strong>اعمال تغییرات</strong></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-7" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                            <tr>
                                                <th style="text-align:center">شماره عضویت</th>
                                                <th style="text-align:center">شماره وکالت</th>
                                                <th style="text-align:center">شماره فیش</th>
                                                <th style="text-align:center">نام و نام خانوادگی</th>
                                                <th style="text-align:center">شماره تلفن همراه</th>
                                                <th style="text-align:center">کد ملی</th>
                                                <th style="text-align:center">تایید</th>
                                                <th style="text-align:center">حذف</th>
                                                <th style="text-align:center">تعداد ورود قبل از بررسی</th>
                                                <th style="text-align:center">تعداد ورود بعد از بررسی</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($full_info_user['black_list'] as $blacklist)
                                                <tr class="gradeX" style="text-align:center;background-color : <?php echo ($blacklist->deleted)? 'rgba(80,80,80,1);color:white':'none'; ?>" >
                                                    <td>{{$blacklist->membership_number}}</td>
                                                    <td></td>
                                                    <td>{{$blacklist->bill_number}}</td>
                                                    <td>{{$blacklist->full_name}}</td>
                                                    <td>{{$blacklist->phone_number}}</td>
                                                    <td>{{$blacklist->national_code}}</td>
                                                    <td>
                                                        @if( ! $blacklist->deleted)
                                                            <a class="btn btn-sm btn-success inactive-slider show_password" data-toggle="modal" data-target="#show_password"
                                                                onclick="accept_black_list({{$blacklist->id}})"> تایید </a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if( ! $blacklist->deleted)
                                                            <a class="btn btn-sm btn-danger"
                                                                onclick="remove_black_list({{$blacklist->id}})"> حذف </a>
                                                        @endif
                                                    </td>
                                                    <td <?php echo ($blacklist->duplicate_insert_before_evaluation)? 'style="font-size : 16pt;color : red"' : '' ; ?> >{{$blacklist->duplicate_insert_before_evaluation}}</td>
                                                    <td <?php echo ($blacklist->duplicate_insert_after_evaluation)? 'style="font-size : 16pt;color : red"' : '' ; ?> >{{$blacklist->duplicate_insert_after_evaluation}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-8" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                            <tr>
                                                <th>شماره عضویت</th>
                                                <th>شماره وکالت</th>
                                                <th>شماره فیش</th>
                                                <th>نام کاربری</th>
                                                <th>شماره تلفن همراه</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($full_info_user['bycot_list'] as $bycot_list)
                                                <tr class="gradeX">
                                                    <td>{{$bycot_list->membership_number}}</td>
                                                    <td></td>
                                                    <td>{{$bycot_list->bill_number}}</td>
                                                    <td>{{$bycot_list->full_name}}</td>
                                                    <td>{{$bycot_list->phone_number}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal fade" id="showimage" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">بستن</span></button>عنوان</h4>
                </div>
                <div class="modal-body">
                    <img src="" title="" class="img-responsive" id="imagetag">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">بستن</button>
                    <form id="img_form" method="post" enctype="multipart/form-data">
                        <input type="file" name="new_img" id="image_file">
                        <button type="submit" class="btn btn-white change_img">تغییر عکس</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal fade" id="show_password" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">بستن</span></button>رمز عبور جدید کاربر تایید شده</h4>
                </div>
                <div class="modal-body">
                    <h2 id="password_holder"></h2>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">بستن</button>
                    <button type="button" class="btn btn-white" id="redirect">پاک شود</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var img_info = null;
        var column = null;
        var condition_value = null;
        var last_image = null;
        $(document).ready(function(){
            var tracking_code = $("#tracking_code");
            tracking_code.html(tracking_code.text().substr(0,3)+'<span style="color:red">'+tracking_code.text().substr(3,2)+'</span>'+tracking_code.text().substr(5));
            $(".show").click(function(){
                last_image = $(this);
                condition_value = $(this).attr('name').split('/')[1];
                img_info = $(this).attr('id');
                column = $(this).attr('name').split('/')[0];
                $("#imagetag").attr('src','<?php echo IMAGES_URL; ?>'+$(this).attr('id'));

            });
            $(".change_status_bill_cheque").click(function(e){
                e.preventDefault();
                var th = $(this);
                $.ajax({
                    url: "<?php echo base_url('admin-dashboard/change-status-bill-cheque'); ?>",
                    type: "POST",
                    data:{bill_cheque_id : th.attr('id')},
                    success: function(response)
                    {
                        if(response == 0) {
                            th.html('لغو تایید پرداخت');
                        }
                        else
                        {
                            th.html('تایید صحت پرداخت');
                        }
                    }
                });
            });

            $("#img_form").submit(function(e){
                e.preventDefault();
                var data = new FormData(this);
                data.append('column',column);
                data.append('folder',img_info.split("/")[0]);
                data.append('condition_value' , condition_value);
                $.ajax({
                    url: "<?php echo base_url('upload-new-img'); ?>",
                    type: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        $("#imagetag").attr('src','<?php echo IMAGES_URL; ?>'+data);
                        $("#image_file").val(null);
                        last_image.attr('id' , data);
                    }
                });
            });
            $("#black_list_disabled").click(function(){

                $.post("<?php echo base_url('black-list-seen/').$ground_id; ?>" , function(response){

                });
            });
            $("#redirect").click(function(){
                //location.replace("{{base_url('admin-dashboard/card-users/0')}}");
            });
        });
        function change_reviewed(ground_id ,status)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');
            $.post("{{base_url('admin-dashboard/change-reviewed')}}", {ground_id : ground_id , status : status}, function ()
            {
                location.reload();
            });
        }
        function remove_black_list(black_list_id)
        {
            $.post("{{base_url('admin-dashboard/remove-black-list')}}", {black_list_id : black_list_id }, function ()
            {
                location.reload();
            });
        }
        function accept_black_list(black_list_id)
        {
            $.post("{{base_url('admin-dashboard/accept-black-list')}}", {black_list_id : black_list_id }, function ($data)
            {
                $("#password_holder").text($data);
                $(".show_password").removeAttr('onclick');
            });
        }
    </script>
@endsection

