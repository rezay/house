<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>فرم پرینت</title>
    <link rel="stylesheet" href="{{base_url('plugin/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{base_url('plugin/bootstrap/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{base_url('plugin/fonts/fonts.css')}}">
    <link rel="stylesheet" href="{{base_url('assets/home/css/style1.css')}}">
</head>
<body>
<section id="print">
    <div class="container">
        <h3 style="font-size: 16pt;text-align: center">بسمه تعالی</h3>
        <div class="header alert alert-info">
            <div class="code">{{$header_first_line}} ( {{$header_third_line}} ) </div>
            <div class="code">
                <span>شماره </span>:<span>{{toPersianNum($membership_number)}} ({{$type}})</span>
            </div>
            <div class="code">
                <span>کد رهگیری </span>:<span>{{toPersianNum($tracking_code)}}</span>
            </div>
        </div>
        <div class="information">
			<div class="row">
				<div style="height:230px;width:160px;border:solid black 1px" class="col-xs-2 col-xs-offset-10"><img src="{{IMAGES_URL.$full_info_user['info'][0]->personal_photo}}" width="160" height="230" alt="عکس پرسنلی"></div>
			</div>
				@foreach($full_info_user['user_meta'] as $info)
                <div>
					<h3>مشخصات : </h3>
                    <div class="info">
                        <div><span>نام و نام خانوادگی :  </span><span> {{$info->first_name." ".$info->last_name}}</span></div>
                        <div><span>فرزند : </span><span>{{$info->father_name}} </span></div>
                        <div><span>شماره شناسنامه : </span><span>{{toPersianNum($info->birth_certif_number)}}</span></div>
                        <div><span>کد ملی : </span><span>{{toPersianNum($info->national_code)}}</span></div>
                        <div><span>میزان سهم : </span><span>{{toPersianNum($info->share_amount)}}</span></div>
                        <div><span>آدرس : </span><span>{{$info->address}}</span></div>
                        <div class="phone"><span>تلفن ثابت و همراه : </span><span>{{toPersianNum(explode('-' , $info->home_number))[1].'-'.toPersianNum(explode('-' , $info->home_number))[0]}}</span> - <span>{{toPersianNum($info->phone_number)}}</span> </div>
                    </div>
                </div>
            @endforeach
            @if($type === 'کارتی')
                <div>
                    <h3>مشخصات کارت : </h3>
                    <div class="info">
                        <div><span>رنگ کارت :  </span><span> {{$full_info_user['card']->card_color}}</span></div>
                        <div><span>خریداری شده از : </span><span>{{$full_info_user['card']->bought_from}} </span></div>
                        <div><span>نام نام خانوادگی شخص : </span><span>{{$full_info_user['card']->bought_from_person}} </span></div>
                        <div><span>کارت بنام : </span><span>{{toPersianNum($full_info_user['card']->card_named)}}</span></div>
                        <div><span>وضعیت امضا : </span><span>{{toPersianNum($full_info_user['card']->signatured)}}</span></div>
                    </div>
                </div>
            @endif
            @if( ! empty($full_info_user['attorneys']))
                @foreach($full_info_user['attorneys'] as $index => $info)
                    <div>
                        <h3>وکالت ها : </h3>
                        <div class="info second-line">
                            <h4>وکالت {{$index+1}} : </h4>
                            <div><span>موکل : </span><span>{{$info->delegated}}</span></div>
                            <div><span>وکیل : </span><span>{{$info->attorney}}</span></div>
                            <div><span>شماره وکالت : </span><span>{{toPersianNum($info->attorney_number)}}</span></div>
                            <div><span>تاریخ وکالت : </span><span>{{toPersianNum($info->date)}}</span></div>
                            <div><span>سریال وکالت : </span><span>{{toPersianNum($info->attorney_serial)}}</span></div>
                            <div><span>دفتر خانه : </span><span>{{toPersianNum($info->office_number)}}</span></div>
                            <div><span>شهر : </span><span>{{$info->city}}</span></div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if( ! empty($full_info_user['certain_document']))
                <div>
                    <h3>بنچاق : </h3>
                    <div class="info">
                        <div><span>مصالح : </span><span>{{$full_info_user['certain_document']->pacifier}}</span></div>
                        <div><span>متصالح : </span><span>{{$full_info_user['certain_document']->motasaleh}}</span></div>
                        <div><span>شماره سند : </span><span>{{toPersianNum($full_info_user['certain_document']->document_number)}}</span></div>
                        <div><span>تاریخ سند : </span><span>{{toPersianNum($full_info_user['certain_document']->document_date)}}</span></div>
                        <div><span>سریال سند : </span><span>{{toPersianNum($full_info_user['certain_document']->document_serial)}}</span></div>
                        <div><span>دفتر خانه : </span><span>{{toPersianNum($full_info_user['certain_document']->office_number)}}</span></div>
                        <div><span>شهر : </span><span>{{$full_info_user['certain_document']->city}}</span></div>
                    </div>
                </div>
            @endif
            @if( ! empty($full_info_user['document']))
                @foreach($full_info_user['document'] as $index => $info)
                    <div>
                        <h3><span>سند ثبتی : </span><span>{{toPersianNum($info->pelak_sabti_number)}}</span></h3>
                        <div class="info">
                            <div><span> شماره دفترچه مالکیت : </span><span>{{toPersianNum($info->ownership_number)}}</span></div>
                            <div><span> شماره ثبت : </span><span>{{toPersianNum($info->registration_number)}}</span></div>
                            <div><span>شماره دفتر :  </span><span>{{toPersianNum($info->office_number)}}</span></div>
                            <div><span> شماره صفحه : </span><span>{{toPersianNum($info->page_number)}}</span></div>
                            <div><span> نام و نام خانوادگی : </span><span>{{$info->full_name}}</span></div>
                            <div>
                                <span> طبق سند شماره : </span><span>{{toPersianNum($info->document_number)}} </span>
                                <span>تاریخ : </span><span>{{toPersianNum($info->date)}}</span>
                                <span>دفترخانه : </span><span>{{toPersianNum($info->office)}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div>
                <h3>اسناد واریزی : </h3>
				<table class="table table-bordered">
				<tr>
					<th>ردیف</th>
					<th>شماره فیش / چک</th>
					<th>مبلغ فیش / چک</th>
					<th>تاریخ واریزی</th>
					<th>محل پرداخت</th>
					<th>واریز کننده</th>
					<th>عنوان پرداختی</th>
				</tr>
                @foreach($full_info_user['bills_cheques'] as $index => $info)
                    @if((int)$info->accepted == 1 || $let_to === 0)
						<tr>
							<td>واریزی{{$index+1}}</td>
							<td>{{toPersianNum($info->number)}}</td>
							<td>{{toPersianNum(number_format($info->amount,0,'','/'))}} ریال </td>
							<td>{{toPersianNum($info->date)}}</td>
							<td>{{$info->place}}</td>
							<td>{{$info->payer_full_name}}</td>
							<td>{{$info->payment_title}}</td>
						</tr>
                    @endif
                @endforeach
				</table>
                <div class="print">
                    <label>
                        جمع مبلغ واریزی :
                        <input type="text" style="text-align: center" value="{{toPersianNum(number_format($total_payments,0,'','/'))}}" disabled><span>ریال </span>
                    </label>
                    @if($priority != '')
                        <label>
                            @if($let_to === 'let_to_silver')
اولویت شما در مرحله نقره ای:
                            @else
  اولویت شما در مرحله طلایی:
                            @endif
                            <input type="text" style="text-align: center" value="{{toPersianNum($priority)}}" disabled>
                        </label>
                    @endif
                    <label>
                        شماره قطعه:
                        <input type="text" style="text-align: center" value="{{toPersianNum($full_info_user['info'][0]->ground_number)}}" disabled>
                    </label>
					<label>
						تاریخ تحویل زمین:
						<input type="text" style="text-align: center" value="{{toPersianNum($full_info_user['info'][0]->delivery_date)}}" disabled>
					</label>
                    <!--<button onclick="prn()">چاپ</button> -->
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.print();

</script>
</body>
</html>
