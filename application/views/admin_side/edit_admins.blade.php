@layout('admin_side/layout')
@section('title')
    {{$title}}
@endsection
@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-4">
                        <form action="{{base_url('update-admin')}}" class="form-horizontal"
                              method="post" enctype="multipart/form-data">
                            <input name="user_id" value="{{$user->id}}" type="hidden">

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="fname">نام:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="{{$user->fname}}"  name="fname" id="fname" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="lname">نام خانوادگی:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="{{$user->lname}}" name="lname" id="lname" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2"  for="access_lvl">سطح دسترسی:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="access_lvl" id="access_lvl" >
                                        <option value="1" {{($user->access_lvl == 1) ? 'selected' : ''}}>سطح اول</option>
                                        <option value="2" {{($user->access_lvl == 2) ? 'selected' : ''}}>سطح دوم</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pass">پسورد:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="pass" id="pass" >
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">ثبت</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection