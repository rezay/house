<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="{{base_url('admin-dashboard/search')}}">
                <div class="form-group">
                    <input type="text" placeholder="جستجو" class="form-control" <?php echo (isset($searched_word) ?  'value = "'.$searched_word.'"' : '') ?> name="target" id="top-search">
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-left">
            <li>
                <span class="m-r-sm text-muted welcome-message">به بخش مدیریت اپلیکیشن  خوش آمدید</span>
            </li>

            <li>
                <a href="{{base_url().'admin-dashboard/logout'}}">
                    <i class="fa fa-sign-out"></i> خروج
                </a>
            </li>

        </ul>
    </nav>
</div>
