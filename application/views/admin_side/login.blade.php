@layout('admin_side/layout')
@section('title')
    {{$page_title}}
@endsection
@section('contents')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h6 class="logo-name">{{$title}}</h6>
            </div>
            <h3>مدیریت اپلیکیشن</h3>
            <p> پشتیبانی 24 ساعته توسط تیم شرکت تابان شهر
            </p>
            <p>برای دیدن فعالیت ها وارد شوید.</p>
            <div class="m-t" role="form">
                <div class="form-group">
                    <input id="username_admin" type="text" class="form-control" placeholder="نام کاربری" >
                </div>
                <div class="form-group">
                    <input id="pass_admin" type="password" class="form-control" placeholder="رمز عبور">
                </div>
                <button onclick="login();" class="btn btn-primary block full-width m-b">ورود</button>

                <a target="" href="http://www.tabaneshahr.com"><small>تابان شهر</small></a>
            </div>
            <p class="m-t"> <small>کلیه حقوق محفوظ است</strong> TabaneShahr.com &copy; 1396-1397</small> </p>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    function login()
    {
        var username = $('#username_admin').val();
        var pass  = $('#pass_admin').val();
        if(!username || !pass)
            return ;

        $.post("{{base_url('check-login')}}", {username:username,pass:pass}, function (data) {

            if(data === 'ok')
            {
                console.log(data);
                window.location.href = "{{base_url('admin-dashboard')}}";
            }
            else
            {

            }

        });
    }
</script>
@endsection


