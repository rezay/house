<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        @yield('title')
    </title>
    <link href="{{base_url().'assets/backend/'}}css/bootstrap.min.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/bootstrap.rtl.min.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Gritter -->
    <link href="{{base_url().'assets/backend/'}}js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/animate.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/style.rtl.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{base_url().'assets/backend/'}}css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/animate.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/style.rtl.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="{{base_url().'assets/backend/'}}css/plugins/chosen/chosen.css" rel="stylesheet">

    <style>
        .numbers
        {
            color : black ;
            padding-left : 3px;
            padding-right : 3px;
            height : 20px;
            font-size : 9pt;
            line-height : 10px;
            transform: scale(1);
            transition: transform 0.6s ease-out;
        }
        .numbers:hover
        {
            transform: scale(1.6);
        }
    </style>
</head>

<body>

@yield('contents')


<script src="{{base_url().'assets/backend/'}}js/jquery-2.1.1.js"></script>
<script src="{{base_url().'assets/backend/'}}js/bootstrap.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/flot/jquery.flot.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/flot/jquery.flot.spline.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/flot/jquery.flot.resize.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/flot/jquery.flot.pie.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/peity/jquery.peity.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/demo/peity-demo.js"></script>
<script src="{{base_url().'assets/backend/'}}js/rada.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/pace/pace.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/gritter/jquery.gritter.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/demo/sparkline-demo.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/chartJs/Chart.min.js"></script>
<!-- <script src="{{base_url().'assets/backend/'}}js/plugins/toastr/toastr.min.js"></script> -->
<script src="{{base_url().'assets/backend/'}}js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="{{base_url().'assets/backend/'}}js/myjs.js"></script>
<!-- <script src="{{base_url().'assets/backend/'}}js/bootstrapValidator.js"></script> -->
<script src="{{base_url().'assets/backend/'}}js/plugins/dropzone/dropzone.js"></script>
<script src="{{base_url().'assets/backend/'}}js/plugins/chosen/chosen.jquery.js"></script>
<script>
    $(document).ready(function(){
        $(".date_txt").keyup(function(e){

            if (e.keyCode != 8){
                if ($(this).val().length == 4){
                    $(this).val($(this).val() + "/");
                } else if ($(this).val().length == 7){
                    $(this).val($(this).val() + "/");
                }
            } else {
                var temp = $(this).val();

                if ($(this).val().length == 5){
                    $(this).val(temp.substring(0,4));
                } else if ($(this).val().length == 8){
                    $(this).val(temp.substring(0,7));
                }
            }
        });
    })
</script>

@yield('scripts')
</body>
</html>
