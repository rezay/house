@layout('admin_side/layout')

@section('title')
    {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        @foreach($guides as $index => $guide)
                            @if($guide->item == 'user_info')
                                <?php $title = 'راهنمای اطلاعات فردی'; ?>
                            @elseif($guide->item == 'card_info')
                                <?php $title = 'راهنمای اطلاعات کارت'; ?>
                            @elseif($guide->item == 'attorney')
                                <?php $title = 'راهنمای اطلاعات وکالت'; ?>
                            @elseif($guide->item == 'certain_document')
                                <?php $title = 'راهنمای اطلاعات سند قطعی(بنچاق)'; ?>
                            @elseif($guide->item == 'document')
                                <?php $title = 'راهنمای اطلاعات سند ثبتی'; ?>
                            @elseif($guide->item == 'bill_cheque')
                                <?php $title = 'راهنمای اطلاعات فیش و چک ها'; ?>
                            @endif
                            <li <?php if($index == 0){echo 'class="active"';} ?> ><a data-toggle="tab" href="#tab-{{$index+1}}">{{$title}}</a></li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($guides as $index => $guide)
                            <div id="tab-{{$index+1}}" class="tab-pane <?php if($index == 0){echo 'active';} ?>">
                                <div class="panel-body">
                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <form action="{{base_url('admin-dashboard/update-guide')}}" method="post">
                                                <input type="hidden" name="item" value="{{$guide->item}}">
                                                <h4 class="alert alert-info">هر یک گزینه ها را با یک خط تیره(-) جدا کنید .</h4>
                                                <label>متن راهنما</label>
                                                <textarea class="form-control" name="guide" placeholder="متن راهنما" style="width : 100%;height:150px;padding : 10px 20px;font-size: 13pt">{{$guide->value}}</textarea>
                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"style="margin-top : 10px"><strong>اعمال تغییرات</strong></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
