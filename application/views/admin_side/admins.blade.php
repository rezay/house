@layout('admin_side/layout')
@section('title')
    {{$title}}
@endsection
@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>لیست مدیران</h5>
                            </div>

                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                        <tr>
                                            <th>نام</th>
                                            <th>نام خانوادگی</th>
                                            <th>نام کاربری</th>
                                            <th>سطح دسترسی</th>
                                            <th>ویرایش اطلاعات</th>
                                            <th> حذف </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($admins as $admin)
                                                @if($admin->status==1)
                                                    <tr class="gradeX">
                                                        <td>{{$admin->fname}}</td>
                                                        <td>{{$admin->lname}}</td>
                                                        <td>{{$admin->username}}</td>
                                                        <td>{{$admin->access_lvl}}</td>
                                                        <td>
                                                            <a href="{{base_url().'admin-edit/'}}{{$admin->id}}" class="btn btn-sm btn-success  inactive-slider">ویرایش</a>
                                                        </td>
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                                @if($this->session->userdata('logged_admin')['id'] != $admin->id)
                                                                    onclick="changeStatus({{$admin->id}})"
                                                                @endif > حذف </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    function changeStatus(admin_id)
    {
        $.post("{{base_url('admin-dashboard/remove-admin')}}", {admin_id : admin_id}, function ()
        {
            location.reload();
        });
    }
</script>
@endsection


