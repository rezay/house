@layout('admin_side/layout')
@section('title')
    {{$title}}
@endsection
@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-6">
                        @if($this->session->flashdata('admin_messages'))
                            <div class="alert alert-info">{{$this->session->flashdata('admin_messages')}}</div>
                        @endif
                        <form action="{{base_url('admin-dashboard/new-ground-process')}}" class="form-horizontal"
                              method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="membership_number">شماره عضویت:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="membership_number" id="membership_number" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="attorney_number">شماره وکالت:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="attorney_number" id="attorney_number" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="attorney_date">تاریخ وکالت:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"  name="attorney_date" id="attorney_date" placeholder="سال(xxxx)/ماه(xx)/روز(xx)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="type">نوع داده:</label>
                                <div class="col-sm-10">
                                    <select name="type" class="form-control" id="type">
                                        <option value="0">سند دار</option>
                                        <option value="1">وکالتی</option>
                                        <option value="2">کارتی</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">ثبت</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection