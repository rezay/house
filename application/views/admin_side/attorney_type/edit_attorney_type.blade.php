@layout('admin_side/layout')
@section('title')
    {{$title}}
@endsection
@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-4">
                        <form action="{{base_url('update-attorney-type')}}" class="form-horizontal"
                              method="post" enctype="multipart/form-data">
                            <input name="attorney_type_id" value="{{$attorney_type->id}}" type="hidden">

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="attorney_type">پلاک:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="{{$attorney_type->attorney_type}}" name="attorney_type" id="attorney_type" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">ثبت</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection