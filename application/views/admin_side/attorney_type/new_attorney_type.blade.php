@layout('admin_side/layout')
@section('title')
    {{$title}}
@endsection
@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-6">
                        <form action="{{base_url('insert-attorney-type')}}" class="form-horizontal" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="control-label col-sm-2"  for="pass">نوع وکالت:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"  name="attorney_type" id="attorney_type">
                                </div>
                            </div>

                            <button name="submit" class="btn btn-info" role="button">ثبت</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection

