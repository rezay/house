@layout('admin_side/layout')
@section('title')
    {{$title}}
@endsection
@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>لیست نوع وکالت ها</h5>
                            </div>

                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                        <tr>
                                            <th>کد ردیف</th>
                                            <th>نوع وکالت</th>
                                            <th>تعداد</th>
                                            <th>ویرایش اطلاعات</th>
                                            <th> حذف </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($attorney_types as $attorney_type)
                                                @if($attorney_type->status==1)
                                                    <tr class="gradeX">
                                                        <td>{{$attorney_type->id}}</td>
                                                        <td>{{$attorney_type->attorney_type}}</td>
                                                        <td>{{$attorney_type->count}}</td>
                                                        <td>
                                                            @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                                                                <a href="{{base_url().'attorney-type-edit/'}}{{$attorney_type->id}}" class="btn btn-sm btn-success  inactive-slider">ویرایش</a>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                                                                <a style="float: right" class="btn btn-sm btn-danger inactive-slider" onclick="changeStatus({{$attorney_type->id}})"> حذف </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    function changeStatus(attorney_type_id)
    {
        $.post("{{base_url('admin-dashboard/remove-attorney-type')}}", {attorney_type_id : attorney_type_id}, function ()
        {
            location.reload();
        });
    }
</script>
@endsection


