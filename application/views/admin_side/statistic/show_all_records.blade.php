@layout('admin_side/layout')

@section('title')
	{{$title}}
@endsection

@section('contents')
	<div id="wrapper">
		@include('admin_side/slideright')
		<div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
			@include('admin_side/navheader')
			@include('admin_side/page-heading')
			@if($this->session->flashdata('message'))
				<p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
			@endif
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-content">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover dataTables-example" >
										<thead>
										<tr>
											<th>ردیف</th>
											<th>شماره عضویت</th>
											<th>نام نام خانوادگی</th>
											<th>شماره تلفن همراه</th>
											<th>شماره وکالت/سند</th>
											<th>تاریخ وکالت/سند</th>
											<th>شماره فیش</th>
											<th>تاریخ فیش</th>
											<th>کد رهگیری</th>
										</tr>
										</thead>
										<tbody>
											@foreach ($grounds as $index => $ground)
												<tr class="gradeX">
													<td>{{$row_start+$index+1}}</td>
													<td>{{$ground->membership_number}}</td>
													<td>{{$ground->full_name}}</td>
													<td>{{$ground->phone_number}}</td>
													<td>{{$ground->attorney_number}}</td>
													<td>{{$ground->attorney_date}}</td>
													<td>{{$ground->bill_number}}</td>
													<td>{{$ground->bill_date}}</td>
													<td class="tracking_code">{{$ground->tracking_code}}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div style="direction : ltr ; text-align:center ;width : 200px;margin : 0 auto;font-size : 12pt">{{$this->pagination->create_links()}}</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

@endsection
