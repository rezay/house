@layout('admin_side/layout')

@section('title')
    {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>تفکیک عضویت ها</h5>
                            </div>

                            <div class="ibox-content">
                                <form action="{{base_url('admin-dashboard/users-statistics')}}" method="get">
                                    <div class="form-group">
                                        <h3>نوع وکالت</h3>
                                        <div>
                                            @foreach($attorney_types as $index => $attorney_type)
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="attorney_types_id[]" {{(isset($filters['attorney_types_id']) && ! empty($filters['attorney_types_id']) && in_array($attorney_type->id , $filters['attorney_types_id']) ? "checked" : '')}} value="{{$attorney_type->id}}" > {{$attorney_type->attorney_type}}
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h3>نوع کاربری</h3>
                                        <div>
                                            <label class="checkbox-inline">
                                                <input name="user_type[]" type="checkbox" {{(isset($filters['user_type']) && ! empty($filters['user_type']) && in_array(0 , $filters['user_type']) ? "checked" : '')}} value="0" id="inlineCheckbox1"> سند دار
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="user_type[]" type="checkbox" {{(isset($filters['user_type']) && ! empty($filters['user_type']) && in_array(1 , $filters['user_type']) ? "checked" : '')}} value="1" id="inlineCheckbox2"> وکالتی
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="user_type[]" type="checkbox" {{(isset($filters['user_type']) && ! empty($filters['user_type']) && in_array(2 , $filters['user_type']) ? "checked" : '')}} value="2" id="inlineCheckbox3"> کارتی
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="attorney_count">
                                        <h3>وکالت</h3>
                                        <div>
                                            <label class="checkbox-inline">
                                                <input name="attorney_count[]" type="checkbox" {{(isset($filters['attorney_count']) && ! empty($filters['attorney_count']) && in_array(1 , $filters['attorney_count']) ? "checked" : '')}} value="1"> اصیل
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="attorney_count[]" type="checkbox" {{(isset($filters['attorney_count']) && ! empty($filters['attorney_count']) && in_array(2 , $filters['attorney_count']) ? "checked" : '')}} value="2"> اصیل + یک وکالت
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="attorney_count[]" type="checkbox" {{(isset($filters['attorney_count']) && ! empty($filters['attorney_count']) && in_array(3 , $filters['attorney_count']) ? "checked" : '')}} value="3"> اصیل + بیش از یک وکالت
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h3>مرحله</h3>
                                        <div>
                                            <label class="checkbox-inline">
                                                <input name="level" type="radio" checked value="all" id="inlineCheckbox1"> همه مراحل
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="level" type="radio" {{((isset($filters['level']) && 'green' == $filters['level']) ? "checked" : '')}} value="green" id="inlineCheckbox1"> سبز
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="level" type="radio" {{((isset($filters['level']) && 'let_to_silver' == $filters['level']) ? "checked" : '')}} value="let_to_silver" id="inlineCheckbox2"> نقره ای
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="level" type="radio" {{((isset($filters['level']) && 'let_to_gold' == $filters['level']) ? "checked" : '')}} value="let_to_gold" id="inlineCheckbox3"> طلایی
                                            </label>
                                            <label class="checkbox-inline">
                                                <input name="level" type="radio" {{((isset($filters['level']) && 'solved' == $filters['level']) ? "checked" : '')}} value="solved" id="inlineCheckbox4"> تسویه حساب شده
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">نمایش</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <form action="{{base_url('admin-dashboard/users-statistics-output')}}" method="post" class="form form-horizontal" style="padding:2%">

                                    <div class="form-group">

                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="membership_number" id="inlineCheckbox2"> شماره عضویت
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="full_name" id="inlineCheckbox3"> نام نام خانوادگی
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="phone_number" id="inlineCheckbox4"> شماره تلفن همراه
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="attorney_number" id="inlineCheckbox5"> شماره وکالت/سند
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="attorney_date" id="inlineCheckbox6"> تاریخ وکالت/سند
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="bill_number" id="inlineCheckbox7"> شماره فیش
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="bill_date" id="inlineCheckbox8"> تاریخ فیش
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="columns[]" checked value="tracking_code" id="inlineCheckbox9">کد رهگیری
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <input class="form-control col-sm-4" type="text" name="from_row" placeholder="از ردیف">
                                            <input class="form-control col-sm-4" type="text" name="to_row" placeholder="تا ردیف">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-default pull-left" type="submit">خروجی اکسل</button>
                                    </div>

                                </form>
                                <hr>

                                <div class="table-responsive">
                                    <span>تعداد :‌</span><span>{{$total_rows}}</span>
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>شماره عضویت</th>
                                            <th>نام نام خانوادگی</th>
                                            <th>شماره تلفن همراه</th>
                                            <th>شماره وکالت/سند</th>
                                            <th>تاریخ وکالت/سند</th>
                                            <th>شماره فیش</th>
                                            <th>تاریخ فیش</th>
                                            <th>کد رهگیری</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for ($i = 0 ; $i < 50 && $i < $total_rows; $i++)
                                            <tr class="gradeX">
                                                <td>{{$i+1}}</td>
                                                <td>{{$grounds[$i]->membership_number}}</td>
                                                <td>{{$grounds[$i]->full_name}}</td>
                                                <td>{{$grounds[$i]->phone_number}}</td>
                                                <td>{{$grounds[$i]->attorney_number}}</td>
                                                <td>{{$grounds[$i]->attorney_date}}</td>
                                                <td>{{$grounds[$i]->bill_number}}</td>
                                                <td>{{$grounds[$i]->bill_date}}</td>
                                                <td class="tracking_code">{{$grounds[$i]->tracking_code}}</td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
								<a href="{{base_url('admin-dashboard/users-statistics/show-all')}}" target="_blank">نمایش همه نتایج</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            if($("#inlineCheckbox3").is(":checked"))
            {
                $("#attorney_count").css('display' , 'none');
                $("input[name = 'attorney_count[]']").attr('disabled' , 'disabled');
            }
            else
            {
                $("#attorney_count").css('display' , 'block');
                $("input[name = 'attorney_count[]']").removeAttr('disabled');
            }
            $("#inlineCheckbox3").change(function () {

                if($("#inlineCheckbox3").is(":checked"))
                {
                    $("#attorney_count").css('display' , 'none');
                    $("input[name = 'attorney_count[]']").attr('disabled' , 'disabled');
                }
                else
                {
                    $("#attorney_count").css('display' , 'block');
                    $("input[name = 'attorney_count[]']").removeAttr('disabled');
                }
            });
        });
    </script>
@endsection
