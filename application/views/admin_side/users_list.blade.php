@layout('admin_side/layout')

@section('title')
    {{$title}}
@endsection

@section('contents')
    <div id="wrapper">
        @include('admin_side/slideright')
        <div id="page-wrapper" class="gray-bg dashbard-1" style="overflow: auto ; overflow-x: hidden">
            @include('admin_side/navheader')
            @include('admin_side/page-heading')
            @if($this->session->flashdata('message'))
                <p style="font-size: 20px;color: #ed5565">{{$this->session->flashdata('message')}}</p>
            @endif
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{$title}}</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <form action="{{base_url('admin-dashboard/solved-users/0')}}" method="post" class="col-md-2">
                                        @if(!empty($users))
                                            <input type="hidden" name="user_type" value="{{$users[0]->type}}">
                                            <button class="btn btn-default">لیست بایگانی راکد</button>
                                        @endif
                                        @if(isset($base_url))
                                            </form>
                                            <form action="{{$base_url.'/orange/0'}}" method="get" class="col-md-1">
                                                <button class="btn btn-default">نارنجی</button>
                                            </form>
                                            <form action="{{$base_url.'/white/0'}}" method="get" class="col-md-1">
                                                <button class="btn btn-default">سفید</button>
                                            </form>
                                            <form action="{{$base_url.'/blue/0'}}" method="get" class="col-md-1">
                                                <button class="btn btn-default">ابی</button>
                                            </form>
                                        @endif
                                </div>


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>شماره عضویت</th>
                                            <th>نام نام خانوادگی</th>
                                            <th>شماره تلفن همراه</th>
                                            <th>شماره وکالت/سند</th>
                                            <th>شماره فیش</th>
                                            <th>کد رهگیری</th>
                                            <th>لیست بررسی سیاه (افراد جدید)</th>
                                            <th>مشاهده اطلاعات</th>
                                            <th>تایید بررسی</th>
                                            @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                                                <th>تایید به مرحله نقره ای</th>
                                                <th>تایید به مرحله طلایی</th>
                                                <th>حضور در دفتر خانه</th>
                                                <th>ارسال به بایگانی راکد</th>
                                                <th>حذف کد رهگیری</th>
                                            @endif
                                            <th>پرینت اطلاعات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($users as $index => $user)
                                            <tr class="gradeX" style="<?php if($user->reviewed == 1 && $user->unseen_black_list != 0){ echo 'background-color: orange; color : white'; }elseif($user->reviewed == 1 && $user->unseen_black_list == 0){echo 'background-color: blue; color : white';}else{ echo '';} ?>" >
                                                <td>{{$row_start+$index+1}}</td>
                                                <td>{{$user->membership_number}}</td>
                                                <td>{{$user->full_name}}</td>
                                                <td>{{$user->phone_number}}</td>
                                                <td>{{$user->attorney_number}}<br>{{$user->attorney_date}}</td>
                                                <td>{{$user->bill_number}}</td>
                                                <td class="tracking_code">{{$user->tracking_code}}</td>
                                                <td style="<?php if($user->unseen_black_list > 0){echo 'color:red;text-align:center;font-size:16pt';} ?>" >{{$user->unseen_black_list}}</td>
                                                @if($user->type == 0)
                                                    <td>
                                                        @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]) || $user->unseen_black_list == 0)
                                                            <a href="{{base_url('admin-dashboard/full-info-document-user/'.$user->ground_id)}}" class="btn btn-sm btn-success  inactive-slider">بیشتر</a>
                                                        @endif
                                                    </td>
                                                @elseif($user->type == 1)
                                                    <td>
                                                        @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]) || $user->unseen_black_list == 0)
                                                            <a href="{{base_url('admin-dashboard/full-info-attorney-user/'.$user->ground_id)}}" class="btn btn-sm btn-success  inactive-slider">بیشتر</a>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td>
                                                        @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]) || $user->unseen_black_list == 0)
                                                            <a href="{{base_url('admin-dashboard/full-info-card-user/'.$user->ground_id)}}" class="btn btn-sm btn-success  inactive-slider">بیشتر</a>
                                                        @endif
                                                    </td>
                                                @endif

                                                @if($user->reviewed == 0)
                                                    <td>
                                                        @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]) || $user->unseen_black_list == 0)
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider" onclick="{{'change_reviewed('.$user->ground_id.',1)'}}"> خیر </a>
                                                        @endif
                                                    </td>
                                                @elseif($user->reviewed == 1)
                                                    <td>
                                                        @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]) || $user->unseen_black_list == 0)
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider" onclick="{{'change_reviewed('.$user->ground_id.',0)'}}"> بله </a>
                                                        @endif
                                                    </td>
                                                @endif
                                                @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                                                    @if($user->let_to_silver == 0)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_let_to_silver('.$user->ground_id.',1)'}}"> خیر </a>
                                                        </td>
                                                    @elseif($user->let_to_silver == 1)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_let_to_silver('.$user->ground_id.',0)'}}"> بله </a>
                                                        </td>
                                                    @endif

                                                    @if($user->let_to_gold == 0)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_let_to_gold('.$user->ground_id.',1)'}}"> خیر </a>
                                                        </td>
                                                    @elseif($user->let_to_gold == 1)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_let_to_gold('.$user->ground_id.',0)'}}"> بله </a>
                                                        </td>
                                                    @endif

                                                    @if($user->msg_to_user == 0)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_msg_to_user('.$user->ground_id.',1)'}}"> خیر </a>
                                                        </td>
                                                    @elseif($user->msg_to_user == 1)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_msg_to_user('.$user->ground_id.',0)'}}"> بله </a>
                                                        </td>
                                                    @endif

                                                    @if($user->solved == 0)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_solved('.$user->ground_id.',1)'}}"> خیر </a>
                                                        </td>
                                                    @elseif($user->solved == 1)
                                                        <td>
                                                            <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                               onclick="{{'change_solved('.$user->ground_id.',0)'}}"> بله </a>
                                                        </td>
                                                    @endif
                                                    <td>
                                                        <a style="float: right" class="btn btn-sm btn-danger inactive-slider"
                                                           onclick="{{'remove_user('.$user->ground_id.')'}}"> حذف </a>
                                                    </td>
                                                @endif
                                                <td>
                                                    <a style="float: right" class="btn btn-sm btn-default inactive-slider" href="{{base_url('admin-dashboard/print/'.$user->ground_id.'/'.$user->logged_user_id)}}" target="_blank"> <span class="fa fa-print"></span> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div style="direction : ltr ; text-align:center ;width : 200px;margin : 0 auto;font-size : 12pt">{{$this->pagination->create_links()}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(".tracking_code").each(function ()
            {
                $(this).html($(this).text().substr(0,3)+'<span style="color:red">'+$(this).text().substr(3,2)+'</span>'+$(this).text().substr(5));
            }
        );
        function change_reviewed(ground_id,status)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');
            $.post("{{base_url('admin-dashboard/change-reviewed')}}", {ground_id : ground_id , status : status}, function ()
            {
                location.reload();
            });
        }
        function change_let_to_gold(ground_id,status)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');
            $.post("{{base_url('admin-dashboard/change-gold')}}", {ground_id : ground_id , status : status}, function ()
            {
                location.reload();
            });
        }
        function change_let_to_silver(ground_id,status)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');
            $.post("{{base_url('admin-dashboard/change-silver')}}", {ground_id : ground_id , status : status}, function ()
            {
                location.reload();
            });
        }
        function change_msg_to_user(ground_id,status)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');
            $.post("{{base_url('admin-dashboard/change-msg-to-user')}}", {ground_id : ground_id , status : status}, function ()
            {
                location.reload();
            });
        }
        function change_solved(ground_id,status)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');

            $.post("{{base_url('admin-dashboard/change-solved')}}", {ground_id : ground_id , status : status}, function ()
            {
                location.reload();
            });
        }
        function remove_user(ground_id)
        {
            $(".btn").css('opacity' , '0.3').attr('onclick' , '');

            $.post("{{base_url('admin-dashboard/remove-user')}}", {ground_id : ground_id}, function ()
            {
                location.reload();
            });
        }
    </script>
@endsection
