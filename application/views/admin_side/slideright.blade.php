<?php
    $document_users_count = $this->db->where('tracking_code !=' , NULL)->where('type' , 0)->count_all_results('grounds');
    $attorney_users_count = $this->db->where('tracking_code !=' , NULL)->where('type' , 1)->count_all_results('grounds');
    $card_users_count = $this->db->where('tracking_code !=' , NULL)->where('type' , 2)->count_all_results('grounds');
    $all_users_count = $this->db->where('tracking_code !=' , NULL)->count_all_results('grounds');
?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"></div>
            </li>

            <li>
                <a href="{{base_url('admin-dashboard')}}"><i class="fa fa-home"></i> <span class="nav-label">صفحه اصلی</span></a>
            </li>

            @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
            <li>
                <a href="#"><i class="glyphicon glyphicon-user"></i> <span class="nav-label"> گروه IT </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{base_url('admin-dashboard/admin-list')}}">لیست گروه IT</a></li>

                    <li><a href="{{base_url('admin-dashboard/add-new-admin')}}">ثبت مدیر جدید</a></li>
                    <li><a href="{{base_url('admin-dashboard/second-admin-access')}}"><span class="nav-label">دسترسی مدیران سطح دوم</span></a></li>
                </ul>
            </li>
            @endif

            <li>
                <a href="#"><i class="glyphicon fa fa-users"></i> <span class="nav-label"> لیست کاربران </span> <span class="fa arrow"></span><span class="label label-warning pull-left numbers" style="margin-left : 10px;">{{$all_users_count}}</span></a>

                <ul class="nav nav-second-level collapse">
                    <li><a href="{{base_url('admin-dashboard/document-users/all/0')}}"> کاربران سند دار <span class="label label-warning pull-left numbers">{{$document_users_count}}</span></a></li>
                    <li><a href="{{base_url('admin-dashboard/attorney-users/all/0')}}"><span class="label label-warning pull-left numbers">{{$attorney_users_count}}</span> کاربران وکالتی </a></li>
                    <li><a href="{{base_url('admin-dashboard/card-users/all/0')}}"> کاربران کارتی <span class="label label-warning pull-left numbers">{{$card_users_count}}</span></a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="glyphicon fa fa-ticket"></i> <span class="nav-label">  پلاک ها </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{base_url('admin-dashboard/pelak-list')}}">لیست  پلاک ها </a></li>
                    @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                        <li><a href="{{base_url('admin-dashboard/add-new-pelak')}}">ثبت پلاک جدید</a></li>
                    @endif
                </ul>
            </li>
            <li>
                <a href="#"><i class="glyphicon fa fa-ticket"></i> <span class="nav-label">  نوع وکالت اولیه </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{base_url('admin-dashboard/attorney-type-list')}}">لیست  انواع وکالت اولیه ها </a></li>
                    @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                        <li><a href="{{base_url('admin-dashboard/add-new-attorney-type')}}">ثبت نوع جدید وکالت</a></li>
                    @endif
                </ul>
            </li>
			<li>
				<a href="#"><i class="glyphicon fa fa-ticket"></i> <span class="nav-label">  عناوین پرداخت </span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="{{base_url('admin-dashboard/payment-title-list')}}">لیست عناوین پرداخت </a></li>
					@if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
						<li><a href="{{base_url('admin-dashboard/add-new-payment-title')}}">ثبت عنوان پرداخت جدید</a></li>
					@endif
				</ul>
			</li>
			<li>
				<a href="#"><i class="glyphicon fa fa-ticket"></i> <span class="nav-label">  وضعیت عمرانی </span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="{{base_url('admin-dashboard/build-state-list')}}">لیست وضعیت عمرانی </a></li>
					@if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
						<li><a href="{{base_url('admin-dashboard/add-new-build-state')}}">ثبت وضعیت عمرانی جدید</a></li>
					@endif
				</ul>
			</li>
            <li>
                <a href="#"><i class="glyphicon fa fa-info"></i> <span class="nav-label">  داده های شرکت </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{base_url('admin-dashboard/search-ground')}}">جستجوی داده </a></li>
                    <li><a href="{{base_url('admin-dashboard/add-new-ground')}}">ثبت داده جدید</a></li>

                </ul>
            </li>
            @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                <li>
                    <a href="{{base_url('admin-dashboard/site-guide')}}"><i class="glyphicon fa fa-book"></i> <span class="nav-label"> راهنمای سایت </span></a>
                </li>
            @endif
            <li>
                <a href="{{base_url('admin-dashboard/users-statistics')}}"><i class="glyphicon fa fa-check"></i> <span class="nav-label">امارگیری و خروجی اکسل</span></a>
            </li>

            @if(valid_access($this->session->userdata('logged_admin')['access_lvl'] , [1]))
                <li>
                    <a href="#"><i class="glyphicon fa fa-cogs"></i> <span class="nav-label"> تنظیمات </span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{base_url('admin-dashboard/texts-setting')}}">تغییر متون صفحه</a></li>
                        <li><a href="{{base_url('admin-dashboard/sms-setting')}}">تغییر متون پیام ها</a></li>
                        <li><a href="{{base_url('admin-dashboard/photo-setting')}}">تغییر عکس های سایت</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{base_url('admin-dashboard/web-status')}}"><i class="glyphicon fa fa-power-off"></i> <span class="nav-label"> وضعیت وب سایت ON/OFF</span></a>
                </li>
            @endif
        </ul>
    </div>
</nav>

