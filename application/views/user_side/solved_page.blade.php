@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section id="middle_part">
        <div class="log-out">
            <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
            <a href="{{base_url('log-out')}}"><button style="background-color: #ec5656">خروج</button></a>
        </div>
    </section>
    <section id="message">
        <div id="tabSix" class="content active">
            <h2 class="animated bounceIn">{{$message}} </h2>
        </div>
    </section>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
@endsection