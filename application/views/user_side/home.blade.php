@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('logo')
    <img src="{{base_url('assets/backend/images/logo/'.$logo)}}" alt="لوگو">
@endsection

@section('header_first_line')
    {{$header_first_line}}
@endsection

@section('shomare_sabt')
    {{$shomare_sabt}}
@endsection

@section('emam_part')
    <img class="col-xs-6 col-md-6 col-md-push-3 col-sm-8 col-sm-push-3" src="{{base_url('assets/backend/images/logo/').$emam_img}}">
    <div class="col-sm-12 col-xs-6">
        <p>{{json_decode($emam_text)[0]}}</p>
        <p>{{json_decode($emam_text)[1]}}</p>
    </div>
@endsection

@section('contents')
    <section id="middle_part">
        <div class="container">
            <div class="col-md-5 col-sm-6">
                <div class="right">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">وکالتی</a></li>
                        <li><a href="#tab2" data-toggle="tab">سند دار</a></li>
                        <li><a href="#tab3" data-toggle="tab">کارتی</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active in" id="tab1">
                            <form method="post" action="{{base_url('attorney-login')}}">
                                @if($this->session->flashdata('attorney_errors'))
                                    <div style="color:red">{{$this->session->flashdata('attorney_errors')}}</div>
                                @endif
                                <?php if($this->session->flashdata('attorney_inputs_history')){ $attorney_inputs_history = $this->session->flashdata('attorney_inputs_history'); } ?>
                                <label> شماره عضویت :</label><br>
                                <input type="text" name="membership_number" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['membership_number']; } ?>"><br>
                                <div class="fieldwrapper">
                                    <div>
                                        <label>شماره وکالت :</label><br>
                                        <input type="text" name="attorney_number" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['attorney_number']; } ?>">
                                    </div>
                                    <div>
                                        <label>تاریخ وکالت :</label><br>
                                        <input type="text" name="attorney_date" class="date_txt" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['attorney_date']; } ?>" placeholder="xxxx/xx/xx">
                                    </div>
                                </div>
                                <div class="fieldwrapper">
                                    <div>
                                        <label>شماره فیش :</label><br>
                                        <input type="text" name="bill_number" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['bill_number']; } ?>">
                                    </div>
                                    <div>
                                        <label>تاریخ فیش :</label><br>
                                        <input type="text" name="bill_date" class="date_txt" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['bill_date']; } ?>" placeholder="xxxx/xx/xx">
                                    </div>
                                </div>
                                <div class="fieldwrapper">
                                    <div>
                                        <label>نام :</label><br>
                                        <input type="text" name="f_name" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['f_name']; } ?>">
                                    </div>
                                    <div>
                                        <label>نام خانوادگی :</label><br>
                                        <input type="text" name="l_name" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['l_name']; } ?>">
                                    </div>
                                </div>
                                <label>تلفن همراه :</label><br>
                                <input type="text" name="phone_number" value="<?php if(isset($attorney_inputs_history)){ echo $attorney_inputs_history['phone_number']; } ?>">
                                    @if($this->session->flashdata('black_list'))
                                        <label>کد ملی :</label><br>
                                        <input type="text" name="national_code" placeholder="کد ملی">
                                    @endif
                                <button>تایید</button>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="tab2">
                            <form method="post" action="{{base_url('document-login')}}">
                                @if($this->session->flashdata('document_errors'))
                                    <div style="color:red">{{$this->session->flashdata('document_errors')}}</div>
                                @endif
                                <?php if($this->session->flashdata('document_inputs_history')){ $document_inputs_history = $this->session->flashdata('document_inputs_history'); } ?>
                                    <label> شماره عضویت :</label><br>
                                    <input type="text" name="membership_number" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['membership_number']; } ?>"><br>
                                    <div class="fieldwrapper">
                                        <div>
                                            <label>شماره وکالت :</label><br>
                                            <input type="text" name="attorney_number" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['attorney_number']; } ?>">
                                        </div>
                                        <div>
                                            <label>تاریخ وکالت :</label><br>
                                            <input type="text" name="attorney_date" class="date_txt" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['attorney_date']; } ?>" placeholder="xxxx/xx/xx">
                                        </div>
                                    </div>
                                    <div class="fieldwrapper">
                                        <div>
                                            <label>شماره فیش :</label><br>
                                            <input type="text" name="bill_number" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['bill_number']; } ?>">
                                        </div>
                                        <div>
                                            <label>تاریخ فیش :</label><br>
                                            <input type="text" name="bill_date" class="date_txt" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['bill_date']; } ?>" placeholder="xxxx/xx/xx">
                                        </div>
                                    </div>
                                    <div class="fieldwrapper">
                                        <div>
                                            <label>نام :</label><br>
                                            <input type="text" name="f_name" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['f_name']; } ?>">
                                        </div>
                                        <div>
                                            <label>نام خانوادگی :</label><br>
                                            <input type="text" name="l_name" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['l_name']; } ?>">
                                        </div>
                                    </div>
                                    <label>تلفن همراه :</label><br>
                                    <input type="text" name="phone_number" value="<?php if(isset($document_inputs_history)){ echo $document_inputs_history['phone_number']; } ?>">
                                    @if($this->session->flashdata('black_list'))
                                        <label>کد ملی :</label><br>
                                        <input type="text" name="national_code" placeholder="کد ملی">
                                    @endif
                                    <button>تایید</button>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="tab3">
                            <form method="post" action="{{base_url('card-login')}}">
                                @if($this->session->flashdata('card_errors'))
                                    <div style="color:red">{{$this->session->flashdata('card_errors')}}</div>
                                @endif
                                <?php if($this->session->flashdata('card_inputs_history')){ $card_inputs_history = $this->session->flashdata('cart_inputs_history'); } ?>
                                <label> شماره عضویت :</label><br>
                                <input type="text" name="membership_number" value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['membership_number']; } ?>"><br>
                                <div class="fieldwrapper">
                                    <div>
                                        <label>شماره وکالت :</label><br>
                                        <input type="text" name="attorney_number" disabled value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['attorney_number']; } ?>">
                                    </div>
                                    <div>
                                        <label>تاریخ وکالت :</label><br>
                                        <input type="text" name="attorney_date" class="date_txt" disabled value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['attorney_date']; } ?>" placeholder="xxxx/xx/xx">
                                    </div>
                                </div>
                                <div class="fieldwrapper">
                                    <div>
                                        <label>شماره فیش :</label><br>
                                        <input type="text" name="bill_number" value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['bill_number']; } ?>">
                                    </div>
                                    <div>
                                        <label>تاریخ فیش :</label><br>
                                        <input type="text" name="bill_date" class="date_txt" value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['bill_date']; } ?>" placeholder="xxxx/xx/xx">
                                    </div>
                                </div>
                                <div class="fieldwrapper">
                                    <div>
                                        <label>نام :</label><br>
                                        <input type="text" name="f_name" value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['f_name']; } ?>">
                                    </div>
                                    <div>
                                        <label>نام خانوادگی :</label><br>
                                        <input type="text" name="l_name" value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['l_name']; } ?>">
                                    </div>
                                </div>
                                <label>تلفن همراه :</label><br>
                                <input type="text" name="phone_number" value="<?php if(isset($card_inputs_history)){ echo $card_inputs_history['phone_number']; } ?>">
                                    @if($this->session->flashdata('black_list'))
                                        <label>کد ملی :</label><br>
                                        <input type="text" name="national_code" placeholder="کد ملی">
                                    @endif
                                <button>تایید</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-sm-6  left">
                <h3>فرم ثبت نام در سامانه جامع</h3>
                <h3><span>{{$header_first_line}}</span></h3>
                <p style="height : 300px ; overflow-y: auto">{{$beside_login}}</p>
                <div class="link"><img src="{{base_url('assets/home/img/alert')}}"><a href="#"  data-toggle="modal" data-target="#exampleModal">در صورت اخذ رمز عبور بر روی لینک کلیک نمایید...</a></div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLabel">ورود به سامانه</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @if($this->session->flashdata('userpass_login_error'))
                                    <div style="color:red">{{$this->session->flashdata('userpass_login_error')}}</div>
                                @endif
                                <form method="post" action="{{base_url('login-by-userpass')}}" id="login_form">
                                    <label>شماره عضویت :</label><br>
                                    <input type="text" name="membership_number"><br>
                                    <label>رمز عبور :‌ </label><br>
                                    <input type="password" name="password"><br>
                                    <a href="#" style="text-decoration: none;" id="password_reset">رمز عبور خود را فراموش کرده اید؟</a>
                                    <button class="submit pull-left" type="submit">ورود</button>
                                    <div class="clear"></div>
                                </form>
                                <form id="phone_number_form" style="display: none">

                                    <label>شماره تلفن همراه :</label><br>
                                    <input type="text" name="phone_number" id="phone_number"><br>
                                    <span id="message" style="color: red"></span><br>
                                    <button class="submit pull-left" id="phone_number_submit" type="submit">تایید</button>
                                    <div class="clear"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    @if($this->session->flashdata('userpass_login_error') || 1==1)
        <script>
            $(document).ready(function(){
                $(".model-body").css('display','block');
                $("#password_reset").click(function(e){
                    e.preventDefault();
                    $("#login_form").css('display','none');
                    $('#phone_number_form').css('display' , 'block')
                });
                $("#phone_number_submit").click(function(e){
                    e.preventDefault();
                    $("#message").text('لطفا منتظر بمانید ...');
                    $("#phone_number_submit").css('display','none');
                    $.ajax({
                        url: "{{base_url('password_reset')}}",
                        type: 'post',
                        data : {phone_number : $("#phone_number").val()},
                        success : function(response){
                            if(response == 0)
                            {
                                $("#message").text('شماره تلفن وارد شده اشتباه است');
                                $("#phone_number_submit").css('display','block');
                            }
                            else
                            {
                                $("#phone_number_form").text('رمز عبور جدید به شماره تلفن وارد شده ارسال شد');
                                setTimeout(function(){location.reload()} , 4000);
                            }
                        }

                    });
                    $("#login_form").css('display','none');
                    $('#phone_number_form').css('display' , 'block')
                });
            });
        </script>
    @endif
@endsection