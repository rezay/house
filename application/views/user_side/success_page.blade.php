@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section id="middle_part">
        <div class="log-out">
            <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
            <a href="{{base_url('log-out')}}"><button>خروج</button></a>
        </div>
        @if($this->session->userdata('logged_user')['type'] == 2)
            <div class="menu">
                <a href="{{base_url('card-user-info-form')}}" class="arrow_box tab" data-tab="tabOne">
                    مشخصات فردی
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('card-card-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات کارت
                    </a>
                @endif
                @if(in_array('card' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('card-bills-cheques-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic','users','card'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','card'])  || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box active tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
        @elseif($this->session->userdata('logged_user')['type'] == 1)
            <div class="menu">
                <a href="{{base_url('attorney-user-info-form')}}" class="arrow_box tab" data-tab="tabOne">
                    مشخصات فردی
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('attorney-attorney-form')}}" class="arrow_box tab" data-tab="tabTwo">
                        اطلاعات وکالت
                    </a>
                @endif
                @if(in_array('attorneys' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('attorney-bills-cheques-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic','users','attorneys'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','attorneys']) || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box active tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
        @else
            <div class="menu">
                <a href="{{base_url('document-user-info-form')}}" class="arrow_box tab" data-tab="tabOne">
                    اطلاعات شرکا
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-attorney-form')}}" class="arrow_box tab" data-tab="tabTwo">
                        اطلاعات وکالت
                    </a>
                @endif
                @if(in_array('attorneys' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-certain-document-form')}}" class="arrow_box tab" data-tab="tabThree">
                        اطلاعات سند قطعی(بنچاق)
                    </a>
                @endif
                @if(in_array('attorneys' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-document-form')}}" class="arrow_box tab" data-tab="tabFour">
                        اطلاعات سند ثبتی
                    </a>
                @endif
                @if(in_array('documents' , explode(',',$this->session->userdata('logged_user')['access_lvl'])) || in_array('certain_documents' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-bills-cheques-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic' , 'users','attorneys','certain_documents'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','attorneys','certain_documents']) || count(array_intersect(['basic','users','attorneys','documents'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','attorneys','documents']) || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box active tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
        @endif

    </section>
    <section id="message">
        <div id="tabSix" class="content active">
            <h2 class="animated bounceIn">عملیات ثبت با موفقیت انجام شد </h2>
            <h4>
                <span>کد رهگیری </span> : <span>{{$ground->tracking_code}}</span>
            </h4>
            @if($ground->description != NULL)
                <textarea placeholder="پیام جهت اطلاع تعاونی" disabled="">{{$ground->description}}</textarea>
            @endif
            <div class="wrapper">
                <button id="print">پرینت اطلاعات</button>
            </div>
        </div>
    </section>
    @if(isset($user_password))
        <section id="user_password">
            <p>رمز عبور شما در ۱۲ ساعت اینده به شما sms خواهد شد  </p>
            <img src="{{base_url('assets/home/img/alert')}}"><p>توجه داشته باشید در صورت دریافت نکردن رمز عبور از بازیابی رمز عبور استفاده کنید</p>
            <div class="button">
                <div><a href="#"><button class="ok" id="user_password_done">بستن</button></a></div>
            </div>
        </section>
    @endif
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    <script>
        $(document).ready(function(){
            $("#print").click(function () {
                window.open("{{base_url('print')}}","_blank");
            });
        @if(isset($user_password))
            $("#user_password").css('display', 'block');
            $("#user_password_done").click(function (e) {
                e.preventDefault();
                $("#user_password").css('display', 'none');
            });
        @endif
        });
    </script>
@endsection