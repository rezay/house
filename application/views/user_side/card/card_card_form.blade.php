@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section id="middle_part">
        <div class="container">
            <div class="log-out">
                <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
                <a href="{{base_url('log-out')}}"><button>خروج</button></a>
            </div>
            <div class="menu">
                <a href="{{base_url('card-user-info-form')}}" class="arrow_box tab" data-tab="tabOne">
                    مشخصات فردی
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('card-card-form')}}" class="arrow_box active tab" data-tab="tabFive">
                        اطلاعات کارت
                    </a>
                @endif
                @if(in_array('card' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('card-bills-cheques-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic','users','card'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','card'])  || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
            <div id="tabFour" class="content active">
                <div class="form">
                    <div class="head">
                        <div class="col-sm-6 col-xs-6">
                            <h3>اطلاعات کارت </h3>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="body">
                        @if(isset($card_info))
                            <form>
                                <input type="hidden" name="id" value="{{$card_info->id}}">
                                <div></div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>رنگ کارت : </label><br>
                                        <select name="card_color">
                                            <option>سبز</option>
                                            <option>زرد</option>
                                            <option>صورتی</option>
                                            <option>سبز ابی</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>خریداری شده از :‌ </label><br>
                                        <select name="bought_from" id="bought_from">
                                            <option>تعاونی</option>
                                            <option>دلال</option>
                                            <option>شخص</option>
                                        </select>                                    </div>
                                    <div>
                                        <label>نام و نام خوانوادگی شخص :‌ </label><br>
                                        <input type="text" name="bought_from_person" id="bought_from_person" value="<?php echo $card_info->bought_from_person; ?>">
                                    </div>

                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>کارت به نام شده : </label><br>
                                        <select name="card_named">
                                            <option>بله</option>
                                            <option>خیر</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label>امضا شده  : </label><br>

                                        <select name="signatured">
                                            <option>خیر</option>
                                            <option> ۲ امضا</option>
                                            <option> ۳ امضا</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>تصویر روی کارت : </label><br>
                                        <input type="file" name="card_front_img" class="base4 form-control form-input form-style-base">
                                        @if($card_info->card_front_img != '' && $card_info->card_front_img != NULL)
                                            <img src="{{IMAGES_URL.$card_info->card_front_img}}" style="width:200px">
                                        @endif
                                    </div>
                                    <div>
                                        <label>تصویر پشت کارت : </label><br>
                                        <input type="file" name="card_back_img" class="base4 form-control form-input form-style-base">
                                        @if($card_info->card_back_img != '' && $card_info->card_back_img != NULL)
                                            <img src="{{IMAGES_URL.$card_info->card_back_img}}" style="width:200px">
                                        @endif
                                    </div>
                                </div>
                            </form>
                        @else
                            <form>
                                <div></div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>رنگ کارت : </label><br>
                                        <select name="card_color">
                                            <option>سبز</option>
                                            <option>زرد</option>
                                            <option>صورتی</option>
                                            <option>سبز ابی</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>خریداری شده از :‌ </label><br>
                                        <select name="bought_from" id="bought_from">
                                            <option>تعاونی</option>
                                            <option>دلال</option>
                                            <option>شخص</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label>نام و نام خوانوادگی شخص/دلال :‌ </label><br>
                                        <input type="text" name="bought_from_person" id="bought_from_person" value="تعاونی">
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>کارت به نام شده : </label><br>
                                        <select name="card_named">
                                            <option>بله</option>
                                            <option>خیر</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label>امضا شده  : </label><br>

                                        <select name="signatured">
                                            <option>خیر</option>
                                            <option> ۲ امضا</option>
                                            <option> ۳ امضا</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>تصویر روی کارت : </label><br>
                                        <input type="file" name="card_front_img" class="base4 form-control form-input form-style-base">
                                    </div>
                                    <div>
                                        <label>تصویر پشت کارت : </label><br>
                                        <input type="file" name="card_back_img" class="base4 form-control form-input form-style-base">
                                    </div>
                                </div>
                            </form>
                        @endif
                        <div class="danger">
                            <div class="flex">
                                <img src="{{base_url('assets/home/img/alert')}}">
                                <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                            </div>
                        </div>
                    </div>
                    @if(isset($card_info))
                        <button class="add" id="send" onclick="send('edit')">تایید</button>
                    @else
                        <button class="add" id="send" onclick="send('new')">تایید</button>
                    @endif
                </div>
            </div>
            <div class="guide">
                <h4>لطفا به راهنمای فرم توجه فرمایید</h4>
                <hr>
                <ul>
                    @foreach(explode('-' ,$guide) as $g)
                        <li>{{$g}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    <div id="mask" style="z-index:10000;text-align:center;display : none; position: fixed;top:0;left:0;width:100%;height:100%;background-color:rgba(1,1,1,0.7)">
        <h3 style="color : white;margin-top:10%">لطفا منتظر بمانید</h3>
        <section id="model">
            <p>آیا تصاویر چک ها و پیش پرداخت ها در دست می باشد‌؟ </p>
            <div class="button">
                <div><a href="#"><button class="no" onclick="next_lvl(0)">خیر</button></a></div>
                <div><a href="#"><button class="yes" onclick="next_lvl(1)">بله</button></a></div>
            </div>
            <div><textarea id="description" style="display : none;width : 100%" placeholder="توضیحات"></textarea></div>
        </section>
    </div>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    <script>
        $(document).ready(function(){

        });

        function send(type){
            var user_ids =[];
            var error = false;

            $("#mask").css('display','block');
            $(".danger").html('');
            var count = 0;
            var uniqueId = function() {
                return Math.random().toString(36).substr(2, 16);
            };
            $("form").each(function () {

                var form = $(this).serializeArray();
                var data = new FormData();
                data.append('card_front_img',$(this).find("input[name='card_front_img']").prop('files')[0]);
                data.append('card_back_img',$(this).find("input[name='card_back_img']").prop('files')[0]);
                data.append('func' , type);
                for(var x=0 ; x < form.length ; x++)
                {
                    data.append(form[x].name , form[x].value);
                }
                $.ajax({
                    url: "{{base_url('card-card-form-process')}}",
                    type: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        count++;
                        response = JSON.parse(response);
                        if(response.status === true) {
                            user_ids.push(response.result);
                            if (count === $("form").toArray().length) {
                                if(error === false)
                                {
                                    enable_status('cards' , user_ids);
                                    if(type === 'edit')
                                    {
                                        setTimeout(function () {
                                            location.reload();
                                        },1000);

                                    }
                                    else
                                    {
                                        $("#mask").children("h3").css('display', 'none');
                                        $("#mask").children("#model").css('display', 'block');
                                    }
                                }
                                else
                                {
                                    delete_records('cards' , user_ids);
                                }
                            }
                        }
                        else
                        {
                            error = true;
                            if(response.status === 0)
                            {
                                for(var message in response.result){
                                    $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result[message]+'</p></div>');
                                }
                            }
                            else
                            {
                                $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result+'</p></div>');
                            }
                            $("#mask").css('display','none');
                            if (count === $("form").toArray().length) {
                                delete_records('cards' , user_ids);
                            }
                        }
                    }
                });
            });
        }
        function enable_status(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('enable-status')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function delete_records(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('delete-records')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function next_lvl(user_response) {
            if(user_response == 1)
            {
                location.replace("{{base_url('card-bills-cheques-form')}}");
            }
            else
            {
                $("#model p").text("اگر توضیحات جهت اطلاع به تعاونی دارید وارد کنید");
                $(".no").attr('onclick' ,'next_lvl2(0)');
                $(".yes").attr('onclick' ,'next_lvl2(1)').text("تایید");
                $("#description").css('display' , 'block');
                //location.replace("{{base_url('success-page')}}");
            }
        }
        function next_lvl2(user_response) {
            if(user_response == 1)
            {
                $.post("{{base_url('success-page')}}",{description : $("#description").val()} , function(response){
                    setTimeout(function(){ location.replace("{{base_url('success-page')}}"); },4000);
                    $("#mask").children("h3").text('یادداشت شما ارسال شد در حال انتقال به صفحه بعدی');
                    $("#mask").children("#model").css('display', 'none');
                    $("#mask").children("h3").css('display', 'block');
                });
            }
            else
            {
                location.replace("{{base_url('success-page')}}");
            }
        }

        $('.tab').click(function(){
            var tab_id = $(this).attr('data-tab');
            console.log("tab_id",tab_id);
            $('.tab').removeClass('active');
            $('.content').removeClass('active');
            $(this).addClass('active');
            $("#"+tab_id).addClass('active animated fadeIn');
        });
        $("#bought_from").change(function(){
            if($(this).val() === 'تعاونی')
            {
                $("#bought_from_person").val('تعاونی');
            }
        });

    </script>
@endsection