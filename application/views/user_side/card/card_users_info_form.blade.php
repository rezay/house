@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section id="middle_part">
        <div class="container">
            <div class="log-out">
                <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
                <a href="{{base_url('log-out')}}"><button>خروج</button></a>
            </div>
            <div class="menu">
                <a href="{{base_url('card-user-info-form')}}" class="arrow_box active tab" data-tab="tabOne">
                    مشخصات فردی
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('card-card-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات کارت
                    </a>
                @endif
                @if(in_array('card' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('card-bills-cheques-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic','users','card'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','card'])  || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
            <div id="tabOne" class="content active">
                <div class="form">
                    <ul class="nav nav-tabs" id="tabs1">
                        @if( ! empty($user_info))
                            @foreach($user_info as $index => $info)
                                <li class="<?php if($index == 0) echo 'active'; ?> "><a href="#tabOne{{$index+1}}" data-toggle="tab"> شریک {{$index+1}}<i class="fa fa-times-circle"></i></a></li>
                            @endforeach
                        @else
                            <li class="active"><a href="#tabOne1" data-toggle="tab">شریک ۱<i class="fa fa-times-circle"></i></a></li>
                        @endif
                        <i class="fa fa-plus-circle fa-2x"></i>
                    </ul>
                    <div id="myTabContent1" class="tab-content">
                        @if( ! empty($user_info))
                            @foreach($user_info as $index => $info)
                                <div class="tab-pane <?php if($index == 0) echo 'active'; ?> in" id="tabOne{{$index+1}}">
                                    <div id="append1">
                                        <div class="head">
                                            <div class="col-sm-6 col-xs-6">
                                                <h3>مشخصات فردی</h3>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                                            </div>
                                        </div>
                                        <div class="line"></div>
                                        <div class="body">
                                            <form>
                                                <input type="hidden" name="user_id" value="{{$info->user_id}}">
                                                <div></div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>نام :‌ </label><br>
                                                        <input type="text" name="first_name" value="{{$info->first_name}}">
                                                    </div>
                                                    <div>
                                                        <label> نام خانوادگی : </label><br>
                                                        <input type="text" name="last_name" value="{{$info->last_name}}">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>نام پدر : </label><br>
                                                        <input type="text" name="father_name" value="{{$info->father_name}}">
                                                    </div>
                                                    <div>
                                                        <label>شماره شناسنامه : </label><br>
                                                        <input type="text" name="birth_certif_number" value="{{$info->birth_certif_number}}">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>کد ملی  : </label><br>
                                                        <input type="text" name="national_code" value="{{$info->national_code}}">
                                                    </div>
                                                    <div>
                                                        <label>تاریخ تولد : </label><br>
                                                        <input type="text" name="birth_date" class="date_txt" value="{{$info->birth_date}}" placeholder="xxxx/xx/xx">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>نشانی محل سکونت :‌ </label><br>
                                                        <input type="text" name="address" value="{{$info->address}}">
                                                    </div>
                                                    <div>
                                                        <label>کد پستی محل سکونت : </label><br>
                                                        <input type="text" name="postal_code" value="{{$info->postal_code}}">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>تلفن ثابت :‌ </label><br>
                                                        <input type="text" name="home_number" value="{{$info->home_number}}">
                                                    </div>
                                                    <div>
                                                        <label>تلفن همراه : </label><br>
                                                        <input type="text" name="phone_number" value="{{$info->phone_number}}">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>تصویر شناسنامه : </label><br>
                                                        <input type="file" name="birth_certificate_img" class="base0 form-control form-input form-style-base">
                                                        @if($info->birth_certificate_img != '' && $info->birth_certificate_img != NULL)
                                                            <img src="{{IMAGES_URL.$info->birth_certificate_img}}" style="width:200px">
                                                        @endif
                                                    </div>
                                                    <div>
                                                        <label>تصویر کارت ملی  : </label><br>
                                                        <input type="file" name="national_card_img" class="base1 form-control form-input form-style-base">
                                                        @if($info->national_card_img != '' && $info->national_card_img != NULL)
                                                            <img src="{{IMAGES_URL.$info->national_card_img}}" style="width:200px">
                                                        @endif
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="danger">
                                                <div class="flex">
                                                    <img src="{{base_url('assets/home/img/alert')}}">
                                                    <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(($index+1) == count($user_info))
                                        <button class="add" id="send" onclick="send('edit')">تایید</button>
                                    @endif
                                </div>
                            @endforeach
                        @else
                            <div class="tab-pane active in" id="tabOne1">
                                <div id="append1">
                                    <div class="head">
                                        <div class="col-sm-6 col-xs-6">
                                            <h3>مشخصات فردی</h3>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                                        </div>
                                    </div>
                                    <div class="line"></div>
                                    <div class="body">
                                        <form>
                                            <div></div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>نام :‌ </label><br>
                                                    <input type="text" name="first_name">
                                                </div>
                                                <div>
                                                    <label> نام خانوادگی : </label><br>
                                                    <input type="text" name="last_name">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>نام پدر : </label><br>
                                                    <input type="text" name="father_name">
                                                </div>
                                                <div>
                                                    <label>شماره شناسنامه : </label><br>
                                                    <input type="text" name="birth_certif_number">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>کد ملی  : </label><br>
                                                    <input type="text" name="national_code">
                                                </div>
                                                <div>
                                                    <label>تاریخ تولد : </label><br>
                                                    <input type="text" name="birth_date" class="date_txt" placeholder="xxxx/xx/xx">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>نشانی محل سکونت :‌ </label><br>
                                                    <input type="text" name="address">
                                                </div>
                                                <div>
                                                    <label>کد پستی محل سکونت : </label><br>
                                                    <input type="text" name="postal_code">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>تلفن ثابت :‌ </label><br>
                                                    <input type="text" name="home_number" placeholder="کد شهر - شماره تلفن">
                                                </div>
                                                <div>
                                                    <label>تلفن همراه : </label><br>
                                                    <input type="text" name="phone_number">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>تصویر شناسنامه : </label><br>
                                                    <input type="file" name="birth_certificate_img" class="base0 form-control form-input form-style-base">
                                                </div>
                                                <div>
                                                    <label>تصویر کارت ملی  : </label><br>
                                                    <input type="file" name="national_card_img" class="base1 form-control form-input form-style-base">
                                                </div>

                                            </div>
                                        </form>
                                        <div class="danger">
                                            
                                            <div class="flex">
                                                <img src="{{base_url('assets/home/img/alert')}}">
                                                <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="add" id="send" onclick="send('new')">تایید</button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="guide">
                <h4>لطفا به راهنمای فرم توجه فرمایید</h4>
                <hr>
                <ul>
                    @foreach(explode('-' ,$guide) as $g)
                        <li>{{$g}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>

    <div id="mask" style="z-index:10000;text-align:center;display : none; position: fixed;top:0;left:0;width:100%;height:100%;background-color:rgba(1,1,1,0.7)">
        <h3 style="color : white;margin-top:10%">لطفا منتظر بمانید</h3>
    </div>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    <script>
        $(document).ready(function(){

        });

        function send(type){
            var user_ids =[];
            var error = false;
            var count = 0;
            $("#mask").css('display','block');
            $(".danger").html('');
            var uniqueId = function() {
                return Math.random().toString(36).substr(2, 16);
            };
            $("form").each(function (index) {

                var form = $(this).serializeArray();
                var data = new FormData();
                data.append('birth_certificate_img',$(this).find("input[name='birth_certificate_img']").prop('files')[0]);
                data.append('national_card_img',$(this).find("input[name='national_card_img']").prop('files')[0]);
                data.append('func' , type);
                for(var x=0 ; x < form.length ; x++)
                {
                    data.append(form[x].name , form[x].value);
                }
                setTimeout(function(){
                $.ajax({
                    url: "{{base_url('card-user-info-form-process')}}",
                    type: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        count++;
                        response = JSON.parse(response);
                        if(response.status === true) {
                            user_ids.push(response.result);
                            if (count === $("form").toArray().length) {
                                if(error === false)
                                {
                                    enable_status('user_meta' , user_ids);
                                    if(type === 'edit')
                                    {
                                        setTimeout(function () {
                                            location.reload();
                                        },1000);
                                    }
                                    else
                                    {
                                        $("#mask").children("h3").css('display', 'none');
                                        setTimeout(function(){ location.replace("{{base_url('card-card-form')}}"); } , 2000);
                                    }
                                }
                                else
                                {
                                    $("#mask").css('display','none');
                                    delete_records('user_meta' , user_ids);
                                }
                            }
                        }
                        else
                        {
                            error = true;
                            if(response.status === 0)
                            {
                                for(var message in response.result){
                                    $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result[message]+'</p></div>');
                                }
                            }
                            else
                            {
                                $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result+'</p></div>');
                            }
                            if (count === $("form").toArray().length) {
                                $("#mask").css('display','none');
                                delete_records('user_meta' , user_ids);
                            }
                        }
                    }
                });
                } , (parseInt(index)*500));
            });
        }
        function enable_status(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('enable-status')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function delete_records(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('delete-records')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }

        $('.tab').click(function(){
            var tab_id = $(this).attr('data-tab');
            console.log("tab_id",tab_id);
            $('.tab').removeClass('active');
            $('.content').removeClass('active');
            $(this).addClass('active');
            $("#"+tab_id).addClass('active animated fadeIn');
        });

    </script>

@endsection