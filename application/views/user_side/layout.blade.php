<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{base_url('plugin/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{base_url('plugin/bootstrap/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{base_url('plugin/fonts/fonts.css')}}">
    <link rel="stylesheet" href="{{base_url('plugin/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{base_url('plugin/animation/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{base_url('assets/home/css/style.css')}}">
    <style>
        .arrow_box:hover{
            text-decoration: none;
        }
    </style>
    <title>
        @yield('title')
    </title>

</head>

<body>
<section id="header">
    <div class="container header">
        <div class="col-sm-3 logo">
            <img src="{{base_url('assets/backend/images/logo/'.$logo)}}" class="img-rounded" alt="لوگو">
        </div>
        <div class="col-sm-6 title">
            <h3>{{$header_first_line}}</h3>
            <div>
                 <span>شماره ثبت </span> : <span>{{$shomare_sabt}}</span>
            </div>
            <h4>
                <span>  پروژه </span> :‌ <span>{{$header_third_line}}</span>
            </h4>
        </div>
        <div class="col-sm-3 left">
            <img class="col-xs-6 col-md-6 col-md-push-6 col-sm-8 col-sm-push-3" src="{{base_url('assets/home/img/').$emam_img}}">
            <div class="col-sm-12 col-xs-6 col-md-push-3">
                <p>{{json_decode($emam_text)[0]}}</p>
                <p>{{json_decode($emam_text)[1]}}</p>
            </div>
        </div>
    </div>
    <div class="flag"></div>
</section>

@yield('contents')

<section id="footer">
    <div class="footer">
        <div class="copyright col-sm-7">
            <span>کلیه حقوق این وب سایت متعلق به </span>
            <span><span>@yield('footer_text')</span></span>
        </div>
        <div class="taban col-sm-5">
            <span style="opacity : 0.1"><a href="http://www.tabaneshahr.com/">طراحی سایت</a></span>
        </div>
    </div>
</section>
@yield('scripts')
</body>
</html>

