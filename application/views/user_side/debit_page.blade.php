@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section <?php if(isset($debit->debit_amount) && $debit->payed == 0){echo 'id="middle_part"';}else{echo 'id="message"';} ?>>
        <div id="container">
            <div class="log-out">
                <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
                <a href="{{base_url('log-out')}}"><button style="background-color: #ec5656">خروج</button></a>
            </div>
            <div id="tabThree" class="content active">
                <div class="form form1">
                    @if($this->session->userdata('logged_user')['let_to'] == 'let_to_silver')
                        @if(isset($debit->debit_amount) && $debit->payed == 0)
                            <div class="head">
                                <div class="col-sm-6 col-xs-6">
                                    <h3>پرداخت واریزی اولیه</h3>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <h3><span>مرحله نقره ای</span><i class="fa fa-circle" style="color: silver"></i></h3>
                                </div>
                            </div>
                            <div class="body">
                                <form method="post" action="{{base_url('pay')}}">
                                    <input type="hidden" name="type" value="0">
                                    <div class="field-wrapper">
                                        <div>
                                            <label>مبلغ اولیه :‌ </label><br>
                                            <input type="text" disabled value="{{number_format($debit->debit_amount,0,'','/')}}">
                                        </div>
                                    </div>
                                    <div class="field-wrapper">
                                        <div>
                                            <label>جمع واریزی به تایید رسیده :‌ </label><br>
                                            <input type="text" disabled value="{{number_format($debit->accepted_payment,0,'','/')}}">
                                        </div>
                                    </div>
                                    <div class="field-wrapper">
                                        <div>
                                            <label>مبلغ قابل پرداخت :‌ </label><br>
                                            <input type="text" disabled value="{{number_format($debit->debit_amount-$debit->accepted_payment,0,'','/')}}">
                                        </div>
                                    </div>
                                    <button class="add" style="background-color: #0d8ddb" id="pay">پرداخت</button>
                                </form>
                                <div class="danger">
                                    <div class="flex">
                                        <img src="{{base_url('assets/home/img/alert')}}">
                                        <p>مبالغ به ریال میباشد . </p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div id="tabSix" class="content active">
                                <h2 class="animated bounceIn">تصویه حساب شما در مرحله نقره ای کامل است میتوانید پرینت اطلاعات خود را دریافت کنید .</h2>

                                <div class="wrapper">
                                    <button id="print">پرینت اطلاعات</button>
                                </div>
                            </div>
                        @endif
                    @else
                        @if(isset($debit->debit_amount) && $debit->payed == 0)
                            <div class="head">
                                <div class="col-sm-6 col-xs-6">
                                    <h3>تصویه حساب نهایی با پروژه</h3>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <h3><span>مرحله طلایی</span><i class="fa fa-circle" style="color: gold"></i></h3>
                                </div>
                            </div>
                            <div class="body">
                                <form method="post" action="{{base_url('pay')}}">
                                    <input type="hidden" name="type" value="1">
                                    <div class="field-wrapper">
                                        <div>
                                            <label>مبلغ قابل پرداخت :‌ </label><br>
                                            <input type="text" disabled value="{{number_format($debit->debit_amount,0,'','/')}}">
                                        </div>
                                    </div>
                                    <button class="add" style="background-color: #0d8ddb" id="pay">پرداخت</button>
                                </form>
                                <div class="danger">
                                    <div class="flex">
                                        <img src="{{base_url('assets/home/img/alert')}}">
                                        <p>مبالغ به ریال میباشد . </p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div id="tabSix" class="content active">
                                <h2 class="animated bounceIn">تصویه حساب شما در مرحله طلایی کامل است میتوانید پرینت اطلاعات خود را دریافت کنید .</h2>

                                <div class="wrapper">
                                    <button id="print">پرینت اطلاعات</button>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    <script>
        $(document).ready(function(){
            $("#print").click(function () {
                window.open("{{base_url('print')}}","_blank");
            });
        });
    </script>
@endsection