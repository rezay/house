@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section id="middle_part">
        <div class="container">
            <div class="log-out">
                <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
                <a href="{{base_url('log-out')}}"><button>خروج</button></a>
            </div>
            <div class="menu">
                <a href="{{base_url('document-user-info-form')}}" class="arrow_box tab" data-tab="tabOne">
                    مشخصات فردی
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-attorney-form')}}" class="arrow_box tab" data-tab="tabTwo">
                        اطلاعات وکالت
                    </a>
                @endif
                @if(in_array('attorneys' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-certain-document-form')}}" class="arrow_box active tab" data-tab="tabThree">
                        اطلاعات بنچاق
                    </a>
                @endif
                @if(in_array('attorneys' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-document-form')}}" class="arrow_box tab" data-tab="tabFour">
                        اطلاعات سند ثبتی
                    </a>
                @endif
                @if(in_array('documents' , explode(',',$this->session->userdata('logged_user')['access_lvl'])) || in_array('certain_documents' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('document-bills-cheques-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic' , 'users','attorneys','certain_documents'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','attorneys','certain_documents']) || count(array_intersect(['basic','users','attorneys','documents'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','attorneys','documents']) || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
            <div id="tabThree" class="content active">
                <div class="form">
                    <div class="head">
                        <div class="col-sm-6 col-xs-6">
                            <h3>بنچاق</h3>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="body">
                        @if(isset($certain_document_info))
                            <form>
                                <input type="hidden" name="id" value="{{$certain_document_info->id}}">
                                <div></div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>مصالح :‌ </label><br>
                                        <input type="text" name="pacifier" value="{{$certain_document_info->pacifier}}">
                                    </div>
                                    <div>
                                        <label>متصالح :‌ </label><br>
                                        <input type="text" name="motasaleh" value="{{$certain_document_info->motasaleh}}">
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>شماره سند : </label><br>
                                        <input type="text" name="document_number" value="{{$certain_document_info->document_number}}">
                                    </div>
                                    <div>
                                        <label>تاریخ سند : </label><br>
                                        <input type="text" name="document_date" class="date_txt" placeholder="xxxx/xx/xx" value="{{$certain_document_info->document_date}}">
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>سریال سند  : </label><br>
                                        <input type="text" name="document_serial" value="{{$certain_document_info->document_serial}}">
                                    </div>
                                    <div>
                                        <label>شماره دفترخانه : </label><br>
                                        <input type="text" name="office_number" value="{{$certain_document_info->office_number}}">
                                    </div>
                                </div>
                                <div class="field-wrapper">

                                    <div>
                                        <label>شهر : </label><br>
                                        <input type="text" name="city" value="{{$certain_document_info->city}}">
                                    </div>
                                    <div>
                                        <label>تصویر سند قطعی  : </label><br>
                                        <input type="file" name="img" class="base1 form-control form-input form-style-base">
                                        @if($certain_document_info->img != '' && $certain_document_info->img != NULL)
                                            <img src="{{IMAGES_URL.$certain_document_info->img}}" style="width:200px">
                                        @endif
                                    </div>
                                </div>
                            </form>
                        @else
                            <form>
                                <div></div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>مصالح :‌ </label><br>
                                        <input type="text" name="pacifier">
                                    </div>
                                    <div>
                                        <label>متصالح :‌ </label><br>
                                        <input type="text" name="motasaleh">
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>شماره سند : </label><br>
                                        <input type="text" name="document_number">
                                    </div>
                                    <div>
                                        <label>تاریخ سند : </label><br>
                                        <input type="text" name="document_date" class="date_txt" placeholder="xxxx/xx/xx">
                                    </div>
                                </div>
                                <div class="field-wrapper">
                                    <div>
                                        <label>سریال سند  : </label><br>
                                        <input type="text" name="document_serial">
                                    </div>
                                    <div>
                                        <label>شماره دفترخانه : </label><br>
                                        <input type="text" name="office_number">
                                    </div>
                                </div>
                                <div class="field-wrapper">

                                    <div>
                                        <label>شهر : </label><br>
                                        <input type="text" name="city">
                                    </div>
                                    <div>
                                        <label>تصویر سند قطعی  : </label><br>
                                        <input type="file" name="img" class="base1 form-control form-input form-style-base">
                                    </div>
                                </div>
                            </form>
                        @endif

                        <div class="danger">

                            <div class="flex">
                                <img src="{{base_url('assets/home/img/alert')}}">
                                <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                            </div>
                        </div>
                    </div>
                    @if(isset($certain_document_info))
                        <button class="add" id="send" onclick="send('edit')">تایید</button>
                    @else
                        <button class="add" id="send" onclick="send('new')">تایید</button>
                    @endif
                </div>
            </div>
            <div class="guide">
                <h4>لطفا به راهنمای فرم توجه فرمایید</h4>
                <hr>
                <ul>
                    @foreach(explode('-' ,$guide) as $g)
                        <li>{{$g}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    <div id="mask" style="z-index:10000;text-align:center;display : none; position: fixed;top:0;left:0;width:100%;height:100%;background-color:rgba(1,1,1,0.7)">
        <h3 style="color : white;margin-top:10%">لطفا منتظر بمانید</h3>
        <section id="model">
            <p>آیا سند دفترچه ای برای شما صادر شده است ؟ </p>
            <div class="button">
                <div><a href="#"><button class="no" onclick="next_lvl(0)">خیر</button></a></div>
                <div><a href="#"><button class="yes" onclick="next_lvl(1)">بله</button></a></div>
            </div>
        </section>
    </div>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    <script>
        $(document).ready(function(){

        });

        function send(type){
            var user_ids =[];
            var error = false;

            $("#mask").css('display','block');
            $(".danger").html('');
            var count = 0;
            var uniqueId = function() {
                return Math.random().toString(36).substr(2, 16);
            };
            $("form").each(function () {

                var form = $(this).serializeArray();
                var data = new FormData();
                data.append('img',$(this).find("input[name='img']").prop('files')[0]);
                data.append('func' , type);
                for(var x=0 ; x < form.length ; x++)
                {
                    data.append(form[x].name , form[x].value);
                }
                $.ajax({
                    url: "{{base_url('document-certain-document-form-process')}}",
                    type: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        count++;
                        response = JSON.parse(response);
                        if(response.status === true) {
                            user_ids.push(response.result);
                            if (count === $("form").toArray().length) {
                                if(error === false)
                                {
                                    enable_status('certain_documents' , user_ids);
                                    if(type === 'edit')
                                    {
                                        setTimeout(function () {
                                            location.reload();
                                        },1000);

                                    }
                                    else
                                    {
                                        $("#mask").children("h3").css('display', 'none');
                                        $("#mask").children("#model").css('display', 'block');
                                    }
                                }
                                else
                                {
                                    delete_records('certain_documents' , user_ids);
                                }
                            }
                        }
                        else
                        {
                            error = true;
                            if(response.status === 0)
                            {
                                for(var message in response.result){
                                    $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result[message]+'</p></div>');
                                }
                            }
                            else
                            {
                                $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result+'</p></div>');
                            }                            $("#mask").css('display','none');
                            if (count === $("form").toArray().length) {
                                delete_records('certain_documents' , user_ids);
                            }
                        }
                    }
                });
            });
        }
        function enable_status(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('enable-status')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function delete_records(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('delete-records')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function next_lvl(user_response) {
            if(user_response == 1)
            {
                location.replace("{{base_url('document-document-form')}}");
            }
            else
            {
                $("#mask").children("#model").children("p").text('آیا تصاویر چک ها و پیش پرداخت ها درست می باشد‌؟ ');
                $("#mask").children("#model").children(".yes").attr('onclick','next_lvl2(1)');
                $("#mask").children("#model").children(".no").attr('onclick','next_lvl2(0)');
                location.replace("{{base_url('document-bills-cheques-form')}}");
            }
        }
        function next_lvl2(user_response) {
            if(user_response == 1)
            {
                location.replace("{{base_url('document-bills-cheques-form')}}");
            }
            else
            {
                location.replace("{{base_url('success-page')}}");
            }
        }

        $('.tab').click(function(){
            var tab_id = $(this).attr('data-tab');
            console.log("tab_id",tab_id);
            $('.tab').removeClass('active');
            $('.content').removeClass('active');
            $(this).addClass('active');
            $("#"+tab_id).addClass('active animated fadeIn');
        });

    </script>
@endsection