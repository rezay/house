@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section id="middle_part">
        <div class="container">
            <div class="log-out">
                <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
                <a href="{{base_url('log-out')}}"><button>خروج</button></a>
            </div>
            <div class="menu">
                <a href="{{base_url('attorney-user-info-form')}}" class="arrow_box tab" data-tab="tabOne">
                    مشخصات فردی
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('attorney-attorney-form')}}" class="arrow_box tab" data-tab="tabTwo">
                        اطلاعات وکالت
                    </a>
                @endif
                @if(in_array('attorneys' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('attorney-bills-cheques-form')}}" class="arrow_box active tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic','users','attorneys'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','attorneys']) || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
            <div id="tabFive" class="content active">
                <div class="form">
                    <ul class="nav nav-tabs" id="tabs2">
                        @if( ! empty($bills_cheques_info))
                            @foreach($bills_cheques_info as $index => $info)
                                <li class="<?php if($index == 0) echo 'active'; ?> "><a href="#tabFive{{$index+1}}" data-toggle="tab"> فرم {{$index+1}}<i class="fa fa-times-circle"></i></a></li>
                            @endforeach
                        @else
                            <li class="active"><a href="#tabFive1" data-toggle="tab">فرم ۱<i class="fa fa-times-circle"></i></a></li>
                        @endif
                        <i class="fa fa-plus-circle fa-2x"></i>
                    </ul>
                    <div id="myTabContent2" class="tab-content">
                        @if( ! empty($bills_cheques_info))
                            @foreach($bills_cheques_info as $index => $info)
                                <div class="tab-pane <?php if($index == 0) echo 'active'; ?> in" id="tabFive{{$index+1}}">
                                    <div id="append2">
                                        <div class="head">
                                            <div class="col-sm-6 col-xs-6">
                                                <h3>چک یا فیش</h3>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                                            </div>
                                        </div>
                                        <div class="line"></div>
                                        <div class="body">
                                            <form>
                                                <input type="hidden" name="id" value="{{$info->id}}">
                                                <select class="selected" name="type">
                                                    <option value="1" <?php if($info->type == 1){echo 'selected'; } ?> >چک</option>
                                                    <option value="0" <?php if($info->type == 0){echo 'selected'; } ?> >فیش</option>
                                                </select>
                                                <div class="check">
                                                    <div class="field-wrapper">
                                                        <div>
                                                            <label>شماره  :‌ </label><br>
                                                            <input type="text" name="number" value="{{$info->number}}">
                                                        </div>
                                                        <div>
                                                            <label>مبلغ(ریال)  : </label><br>
                                                            <input type="text" name="amount" value="{{number_format($info->amount,0,'','/')}}">
                                                        </div>
                                                    </div>
                                                    <div class="field-wrapper">
                                                        <div>
                                                            <label>تاریخ : </label><br>
                                                            <input type="text" name="date" class="date_txt" value="{{$info->date}}" placeholder="xxxx/xx/xx">
                                                        </div>
                                                        <div>
                                                            <label>محل پرداخت : </label><br>
                                                            <input type="text" name="place" value="{{$info->place}}">
                                                        </div>
                                                    </div>
                                                    <div class="field-wrapper">
                                                        <div>
                                                            <label> پرداخت کننده : </label><br>
                                                            <input type="text" name="payer_full_name" value="{{$info->payer_full_name}}">
                                                        </div>
                                                        <div>
                                                            <label>  تصویر  : </label><br>
                                                            <input type="file" name="img" class="base4 form-control form-input form-style-base">
                                                            @if($info->img != '' && $info->img != NULL)
                                                                <img src="{{IMAGES_URL.$info->img}}" style="width:200px">
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="danger">
                                                <div class="flex">
                                                    <img src="{{base_url('assets/home/img/alert')}}">
                                                    <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(($index+1) == count($bills_cheques_info))
                                        <button class="add" id="send" onclick="send('edit')">تایید</button>
                                    @endif
                                </div>
                            @endforeach
                        @else
                            <div class="tab-pane active in" id="tabFive1">
                                <div id="append2">
                                    <div class="head">
                                        <div class="col-sm-6 col-xs-6">
                                            <h3>چک یا فیش</h3>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                                        </div>
                                    </div>
                                    <div class="line"></div>
                                    <div class="body">
                                        <form>
                                            <select class="selected" name="type">
                                                <option value="1">چک</option>
                                                <option value="0">فیش</option>
                                            </select>
                                            <div class="check">
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>شماره  :‌ </label><br>
                                                        <input type="text" name="number">
                                                    </div>
                                                    <div>
                                                        <label>مبلغ(ریال)  : </label><br>
                                                        <input type="text" name="amount">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>تاریخ : </label><br>
                                                        <input type="text" name="date" class="date_txt" placeholder="xxxx/xx/xx">
                                                    </div>
                                                    <div>
                                                        <label>محل پرداخت : </label><br>
                                                        <input type="text" name="place">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label> پرداخت کننده : </label><br>
                                                        <input type="text" name="payer_full_name">
                                                    </div>
                                                    <div>
                                                        <label>  تصویر  : </label><br>
                                                        <input type="file" name="img" class="base4 form-control form-input form-style-base">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="danger">
                                            
                                            <div class="flex">
                                                <img src="{{base_url('assets/home/img/alert')}}">
                                                <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="add" id="send" onclick="send('new')">تایید</button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="guide">
                <h4>لطفا به راهنمای فرم توجه فرمایید</h4>
                <hr>
                <ul>
                    @foreach(explode('-' ,$guide) as $g)
                        <li>{{$g}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    <div id="mask" style="z-index:10000;text-align:center;display : none; position: fixed;top:0;left:0;width:100%;height:100%;background-color:rgba(1,1,1,0.7)">
        <h3 style="color : white;margin-top:10%">لطفا منتظر بمانید</h3>
        <section id="model">
            <p>اگر توضیحات جهت اطلاع به تعاونی دارید وارد کنید </p>
            <div class="button">
                <div><a href="#"><button class="no" onclick="next_lvl(0)">خیر</button></a></div>
                <div><a href="#"><button class="yes" onclick="next_lvl(1)">تایید</button></a></div>
            </div>
            <div><textarea id="description" style="width : 100%" placeholder="توضیحات"></textarea></div>
        </section>
    </div>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    <script>
        $(document).ready(function(){

        });

        function send(type){
            var user_ids =[];
            var error = false;

            $("#mask").css('display','block');
            $(".danger").html('');
            var count = 0;
            var uniqueId = function() {
                return Math.random().toString(36).substr(2, 16);
            };
            $("form").each(function (index) {

                var form = $(this).serializeArray();
                var data = new FormData();
                data.append('img',$(this).find("input[name='img']").prop('files')[0]);
                data.append('func' , type);
                for(var x=0 ; x < form.length ; x++)
                {
                    data.append(form[x].name , form[x].value);
                }
                setTimeout(function(){
                $.ajax({
                    url: "{{base_url('attorney-bills-cheques-form-process')}}",
                    type: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        count++;
                        response = JSON.parse(response);
                        if(response.status === true) {
                            user_ids.push(response.result);
                            if (count === $("form").toArray().length) {
                                if(error === false)
                                {
                                    enable_status('bills_cheques' , user_ids);
                                    if(type === 'edit')
                                    {
                                        setTimeout(function () {
                                            location.reload();
                                        },1000);

                                    }
                                    else
                                    {
                                        $("#mask").children("h3").css('display', 'none');
                                        $("#mask").children("#model").css('display', 'block');
                                    }
                                }
                                else
                                {
                                    $("#mask").css('display','none');
                                    delete_records('bills_cheques' , user_ids);
                                }
                            }
                        }
                        else
                        {
                            error = true;
                            if(response.status === 0)
                            {
                                for(var message in response.result){
                                    $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result[message]+'</p></div>');
                                }
                            }
                            else
                            {
                                $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result+'</p></div>');
                            }                            if (count === $("form").toArray().length) {
                            $("#mask").css('display','none');
                            delete_records('bills_cheques' , user_ids);
                        }
                        }
                    }
                });
                } , (parseInt(index)*500));
            });
        }
        function enable_status(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('enable-status')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function delete_records(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('delete-records')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function next_lvl(user_response) {
            if(user_response == 1)
            {
                $.post("{{base_url('success-page')}}",{description : $("#description").val()} , function(response){
                    setTimeout(function(){ location.replace("{{base_url('success-page')}}"); },4000);
                    $("#mask").children("h3").text('یادداشت شما ارسال شد در حال انتقال به صفحه بعدی');
                    $("#mask").children("#model").css('display', 'none');
                    $("#mask").children("h3").css('display', 'block');
                });
            }
            else
            {
                location.replace("{{base_url('success-page')}}");
            }
        }

        $('.tab').click(function(){
            var tab_id = $(this).attr('data-tab');
            console.log("tab_id",tab_id);
            $('.tab').removeClass('active');
            $('.content').removeClass('active');
            $(this).addClass('active');
            $("#"+tab_id).addClass('active animated fadeIn');
        });

    </script>
@endsection