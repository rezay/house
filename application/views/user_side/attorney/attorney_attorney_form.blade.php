@layout('user_side/layout')
@section('title')
    {{$title}}
@endsection

@section('contents')
    <section id="middle_part">
        <div class="container">
            <div class="log-out">
                <span>شماره عضویت :</span><h3>{{toPersianNum($this->session->userdata('logged_user')['membership_number'])}}</h3>
                <a href="{{base_url('log-out')}}"><button>خروج</button></a>
            </div>
            <div class="menu">
                <a href="{{base_url('attorney-user-info-form')}}" class="arrow_box tab" data-tab="tabOne">
                    مشخصات فردی
                </a>
                @if(in_array('users' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('attorney-attorney-form')}}" class="arrow_box active tab" data-tab="tabTwo">
                        اطلاعات وکالت
                    </a>
                @endif
                @if(in_array('attorneys' , explode(',',$this->session->userdata('logged_user')['access_lvl'])))
                    <a href="{{base_url('attorney-bills-cheques-form')}}" class="arrow_box tab" data-tab="tabFive">
                        اطلاعات مالی
                    </a>
                @endif
                @if(count(array_intersect(['basic','users','attorneys'], explode(',',$this->session->userdata('logged_user')['access_lvl']))) == count(['basic','users','attorneys']) || isset($tracking_code))
                    <a href="{{base_url('success-page')}}" class="arrow_box tab" data-tab="tabSix">
                        دریافت کد رهگیری
                    </a>
                @endif
            </div>
            <div id="tabTwo" class="content active">
                <div class="form">
                    <ul class="nav nav-tabs" id="tabs">
                        @if( ! empty($attorney_info))
                            @foreach($attorney_info as $index => $info)
                                <li class="<?php if($index == 0) echo 'active'; ?> "><a href="#tab{{$index+1}}" data-toggle="tab"> وکالت {{$index+1}}<i class="fa fa-times-circle"></i></a></li>
                            @endforeach
                        @else
                            <li class="active"><a href="#tab1" data-toggle="tab"> وکالت ۱<i class="fa fa-times-circle"></i></a></li>
                        @endif
                        <i class="fa fa-plus-circle fa-2x"></i>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        @if( ! empty($attorney_info))
                            @foreach($attorney_info as $index => $info)
                                <div class="tab-pane <?php if($index == 0) echo 'active'; ?> in" id="tab{{$index+1}}">
                                    <div id="append">
                                        <div class="head">
                                            <div class="col-sm-6 col-xs-6">
                                                <h3>وکالت</h3>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                                            </div>
                                        </div>
                                        <div class="line"></div>
                                        <div class="body">
                                            <form id="form1" class="attorney_forms">
                                                <input type="hidden" name="id" value="{{$info->id}}">
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>موکل :‌ </label><br>
                                                        <input type="text" name="delegated" value="{{$info->delegated}}" readonly>
                                                    </div>
                                                    <div>
                                                        <label>وکیل : </label><br>
                                                        <input type="text" name="attorney" value="{{$info->attorney}}">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>شماره وکالت ‌: </label><br>
                                                        <input type="text" name="attorney_number" value="{{$info->attorney_number}}">
                                                    </div>
                                                    <div>
                                                        <label>تاریخ وکالت : </label><br>
                                                        <input type="text" name="date" class="date_txt" value="{{$info->date}}" placeholder="xxxx/xx/xx">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>سریال وکالت ‌: </label><br>
                                                        <input type="text" name="attorney_serial" value="{{$info->attorney_serial}}">
                                                    </div>
                                                    <div>
                                                        <label>شماره دفتر خانه  : </label><br>
                                                        <input type="text" name="office_number" value="{{$info->office_number}}">
                                                    </div>
                                                </div>
                                                <div class="field-wrapper">
                                                    <div>
                                                        <label>شهر دفتر خانه : </label><br>
                                                        <input type="text" name="city" value="{{$info->city}}">
                                                    </div>
                                                    <div>
                                                        <label>تصویر وکالت : </label><br>
                                                        <input type="file" name="img" class="base4 form-control form-input form-style-base">
                                                        @if($info->img != '' && $info->img != NULL)
                                                            <img src="{{IMAGES_URL.$info->img}}" style="width:200px">
                                                        @endif
                                                    </div>
                                                </div>

                                            </form>
                                            <div class="danger">
                                                <div class="flex">
                                                    <img src="{{base_url('assets/home/img/alert')}}">
                                                    <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(($index+1) == count($attorney_info))
                                        <button class="add" id="send" onclick="send('edit')">تایید</button>
                                    @endif
                                </div>
                            @endforeach
                        @else
                            <div class="tab-pane active in" id="tab1">
                                <div id="append">
                                    <div class="head">
                                        <div class="col-sm-6 col-xs-6">
                                            <h3>وکالت</h3>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <h3><span>مرحله سبز</span><i class="fa fa-circle"></i></h3>
                                        </div>
                                    </div>
                                    <div class="line"></div>
                                    <div class="body">
                                        <form id="form1" class="attorney_forms">
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>موکل :‌ </label><br>
                                                    <input type="text" name="delegated" value="شرکت تعاونی مسکن کارگران برق منطقه ای تهران" readonly>
                                                </div>
                                                <div>
                                                    <label>وکیل : </label><br>
                                                    <input type="text" name="attorney">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>شماره وکالت ‌: </label><br>
                                                    <input type="text" name="attorney_number" value="{{$this->session->userdata('logged_user')['attorney_number']}}">
                                                </div>
                                                <div>
                                                    <label>تاریخ وکالت : </label><br>
                                                    <input type="text" name="date"  class="date_txt" value="{{$this->session->userdata('logged_user')['attorney_date']}}" placeholder="xxxx/xx/xx">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>سریال وکالت ‌: </label><br>
                                                    <input type="text" name="attorney_serial">
                                                </div>
                                                <div>
                                                    <label>شماره دفتر خانه  : </label><br>
                                                    <input type="text" name="office_number">
                                                </div>
                                            </div>
                                            <div class="field-wrapper">
                                                <div>
                                                    <label>شهر دفتر خانه : </label><br>
                                                    <input type="text" name="city">
                                                </div>
                                                <div>
                                                    <label>تصویر وکالت : </label><br>
                                                    <input type="file" name="img" class="base4 form-control form-input form-style-base">
                                                </div>
                                            </div>
                                        </form>
                                        <div class="danger">
                                            
                                            <div class="flex">
                                                <img src="{{base_url('assets/home/img/alert')}}">
                                                <p>حجم تصاویر انتخابی نباید بیشتر از ۳۰۰KB باشد . ! </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="add" id="send" onclick="send('new')">تایید</button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="guide">
                <h4>لطفا به راهنمای فرم توجه فرمایید</h4>
                <hr>
                <ul>
                    @foreach(explode('-' ,$guide) as $g)
                        <li>{{$g}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>

    <div id="mask" style="z-index:10000;text-align:center;display : none; position: fixed;top:0;left:0;width:100%;height:100%;background-color:rgba(1,1,1,0.7)">
        <h3 style="color : white;margin-top:10%">لطفا منتظر بمانید</h3>
        <section id="model">
            <p>آیا تصاویر چک ها و پیش پرداخت ها در دست می باشد‌؟</p>
            <div class="button">
                <div><a href="#"><button class="no" onclick="next_lvl(0)">خیر</button></a></div>
                <div><a href="#"><button class="yes" onclick="next_lvl(1)">بله</button></a></div>
            </div>
            <div><textarea id="description" style="display : none;width : 100%" placeholder="توضیحات"></textarea></div>
        </section>
    </div>
@endsection

@section('footer_text')
    {{$footer_text}}
@endsection

@section('scripts')
    <script src="{{base_url('plugin/jquery-2.1.1.js')}}"></script>
    <script src="{{base_url('plugin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets/home/js/script.js')}}"></script>
    <script>
        $(document).ready(function(){

        });

        function send(type){
            var user_ids =[];
            var error = false;

            $("#mask").css('display','block');
            $(".danger").html('');
            var count = 0;
            var parent_count = 0;
            var uniqueId = function() {
                return Math.random().toString(36).substr(2, 16);
            };
            $("form").each(function (index) {

                var form = $(this).serializeArray();
                var data = new FormData();
                data.append('img',$(this).find("input[name='img']").prop('files')[0]);
                data.append('func' , type);
                for(var x=0 ; x < form.length ; x++)
                {
                    data.append(form[x].name , form[x].value);
                }
                data.append('parent_id' , parent_count);
                setTimeout(function(){
                $.ajax({
                    url: "{{base_url('attorney-attorney-form-process')}}",
                    type: "POST",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        count++;
                        response = JSON.parse(response);
                        if(response.status === true) {
                            user_ids.push(response.result);
                            if (count === $("form").toArray().length) {
                                if(error === false)
                                {
                                    enable_status('attorneys' , user_ids);
                                    if(type === 'edit')
                                    {
                                        setTimeout(function () {
                                            location.reload();
                                        },1000);

                                    }
                                    else
                                    {
                                        $("#mask").children("h3").css('display', 'none');
                                        $("#mask").children("#model").css('display', 'block');
                                    }
                                }
                                else
                                {
                                    delete_records('attorneys' , user_ids);
                                    $("#mask").css('display', 'none');
                                }
                            }
                        }
                        else
                        {
                            error = true;
                            if(response.status === 0)
                            {
                                for(var message in response.result){
                                    $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result[message]+'</p></div>');
                                }
                            }
                            else
                            {
                                $(".danger").append('<div class="flex"><img src="{{base_url('assets/home/img/alert')}}"><p>'+response.result+'</p></div>');
                            }
                            if (count === $("form").toArray().length) {
                                $("#mask").css('display','none');
                                delete_records('attorneys' , user_ids);
                            }
                        }
                    }
                });
                } , (parseInt(index)*500));
                parent_count++;
            });
        }
        function enable_status(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('enable-status')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function delete_records(table , user_ids) {
            //console.log(user_ids);
            var data = {table : table , user_ids : JSON.stringify(user_ids)};
            $.ajax({
                url: "{{base_url('delete-records')}}",
                type: "POST",
                data : data,
                success: function (response) {

                }
            });
        }
        function next_lvl(user_response) {
            if(user_response == 1)
            {
                location.replace("{{base_url('attorney-bills-cheques-form')}}");
            }
            else
            {
                $("#modal p").text("اگر توضیحات جهت اطلاع به تعاونی دارید وارد کنید");
                $(".no").attr('onclick' ,'next_lvl2(0)');
                $(".yes").attr('onclick' ,'next_lvl2(1)').text("تایید");
                $("#description").css('display' , 'block');
                //location.replace("");
            }
        }
        function next_lvl2(user_response) {
            if(user_response == 1)
            {
                $.post("{{base_url('success-page')}}",{description : $("#description").val()} , function(response){
                    setTimeout(function(){ location.replace("{{base_url('success-page')}}"); },4000);
                    $("#mask").children("h3").text('یادداشت شما ارسال شد در حال انتقال به صفحه بعدی');
                    $("#mask").children("#model").css('display', 'none');
                    $("#mask").children("h3").css('display', 'block');
                });
            }
            else
            {
                location.replace("{{base_url('success-page')}}");
            }
        }

        $('.tab').click(function(){
            var tab_id = $(this).attr('data-tab');
            console.log("tab_id",tab_id);
            $('.tab').removeClass('active');
            $('.content').removeClass('active');
            $(this).addClass('active');
            $("#"+tab_id).addClass('active animated fadeIn');
        });

    </script>
@endsection