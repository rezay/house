<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		            = 'پر کردن فیلد {field} الزامی است .';
$lang['form_validation_isset']			            = 'فیلد {field} نباید خالی باشد .';
$lang['form_validation_valid_email']		        = 'فیلد {field} باید یک ایمیل صحیح باشد .';
$lang['form_validation_valid_emails']		        = 'فیلد {field} باید شامل ایمیل های صحیح باشد .';
$lang['form_validation_valid_url']		            = 'فیلد {field} باید یک ادرس درست باشد .';
$lang['form_validation_valid_ip']		            = 'فیلد {field} باید یک IP درست باشد .';
$lang['form_validation_min_length']		            = 'فیلد {field} باید حداقل {param} کاراکتر باشد .';
$lang['form_validation_max_length']		            = 'فیلد {field} نباید بیشتر از {param} کاراکتر باشد .';
$lang['form_validation_exact_length']		        = 'فیلد {field} باید دقیقا {param} کاراکتر باشد .';
$lang['form_validation_alpha']			            = 'فیلد {field} باید فقط شامل کاراکتر حرفی باشد.';
$lang['form_validation_alpha_numeric']		        = 'فیلد {field} باید فقط شامل کاراکتر حرفی وعددی باشد.';
$lang['form_validation_alpha_numeric_spaces']	    = 'فیلد {field} باید فقط شامل کاراکتر حرفی و عددی و فاصله باشد.';
$lang['form_validation_alpha_dash']		            = 'فیلد {field} باید فقط شامل کاراکتر حرفی وعددی و - و _ باشد.';
$lang['form_validation_numeric']		            = 'فیلد {field} باید فقط شامل کاراکتر عددی  باشد.';
$lang['form_validation_is_numeric']		            = 'فیلد {field} باید فقط شامل کاراکتر عددی  باشد.';
$lang['form_validation_integer']		            = 'فیلد {field} باید فقط شامل کاراکتر عدد صحیح  باشد.';
$lang['form_validation_regex_match']		        = ' فیلد{field} در فرمت صحیح وارد نشده است.';
$lang['form_validation_matches']		            = 'فیلد {field} با {param} هم خوانی ندارد.';
$lang['form_validation_differs']		            = 'فیلد {field} باید با {param} متفاوت باشد.';
$lang['form_validation_is_unique'] 		            = 'فیلد {field} باید منحصر به فرد باشد.';
$lang['form_validation_is_natural']		            = 'فیلد {field} باید فقط شامل کاراکتر عدد طبیعی باشد.';
$lang['form_validation_is_natural_no_zero']	        = 'فیلد {field} باید فقط شامل کاراکتر عدد طبیعی به جز  صفر باشد.';
$lang['form_validation_decimal']		            = 'فیلد {field} باید فقط شامل کاراکتر عدد اعشاری باشد.';
$lang['form_validation_less_than']		            = 'فیلد {field} باید کوچکتر از {param} باشد.';
$lang['form_validation_less_than_equal_to']	        = 'فیلد {field} باید کوچکتر مساوی {param} باشد.';
$lang['form_validation_greater_than']		        = 'فیلد {field} باید بزرگتر از {param} باشد.';
$lang['form_validation_greater_than_equal_to']	    = 'فیلد {field} باید بزرگتر مساوی {param} باشد.';
$lang['form_validation_error_message_not_set']	    = 'خطا در وارد کردن مقدار {field}';
$lang['form_validation_in_list']		            = 'فیلد {field} باید در لیست {param} موجود باشد.';
