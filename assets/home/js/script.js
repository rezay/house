$(document).ready(function () {
    $(".date_txt").keyup(function(e){

        if (e.keyCode != 8){
            if ($(this).val().length == 4){
                $(this).val($(this).val() + "/");
            } else if ($(this).val().length == 7){
                $(this).val($(this).val() + "/");
            }
        } else {
            var temp = $(this).val();

            if ($(this).val().length == 5){
                $(this).val(temp.substring(0,4));
            } else if ($(this).val().length == 8){
                $(this).val(temp.substring(0,7));
            }
        }
    });
    $('.arrow_box').on('click' , function () {
        $(this).closest('.menu').find('div.arrow_box').removeClass('active');
        if(!$(this).hasClass('active')){
            $(this).addClass('active');
        }
    });
    $('.check').show();
    $('.bill').hide();
    $(document).on('change','.selected',function () {
        if(this.value === 'check'){
            $(this).closest('form').find('.check').show().addClass('animated fadeIn');
            $(this).closest('form').find('.bill').hide();
        }
        else if(this.value === 'bill'){
            $(this).closest('form').find('.bill').show().addClass('animated fadeIn');
            $(this).closest('form').find('.check').hide();
        }
    });
    var number = '';
    $(".tab-pane").each(function () {
       number = $(this).attr('id');
    });

    var nextTab = parseInt(number.substr(-1 , 1))+1;
    var nextTab1 = parseInt(number.substr(-1 , 1))+1;
    var nextTab2 = parseInt(number.substr(-1 , 1))+1;
    $(document).on('click', '.fa-plus-circle', function() {
        var tabname = $(this).closest('.content').attr('id');
        //console.log(tabname);
        if(tabname === 'tabOne'){
            var currentTab1 = $('#tabs1 li').size()-1;
            $('<li><a href="#tabOne'+nextTab1+'" data-toggle="tab">فرم '+nextTab1+'<i class="fa fa-times-circle"></i></a></li>').appendTo('#tabs1');
            $('<div class="tab-pane  in" id="tabOne'+nextTab1+'"></div>').appendTo('#myTabContent1');
            $("#append1").clone().appendTo("#tabOne"+nextTab1);
            $("#tabOne"+nextTab1).find('input').val('');
            $("#tabOne"+nextTab1).find('img').remove();
            $('#tabOne a:last').tab('show');
            var clone1 = $("#tabOne").find('.add').clone();
            $("#tabOne").find('.add').remove();
            clone1.appendTo($("#myTabContent1 div.tab-pane:last-child"));
            nextTab1+=1;
        }
        else if(tabname === 'tabTwo'){
            var currentTab = $('#tabs li').size()-1;
            $('<li><a href="#tab'+nextTab+'" data-toggle="tab">فرم '+nextTab+'<i class="fa fa-times-circle"></i></a></li>').appendTo('#tabs');
            $('<div class="tab-pane  in" id="tab'+nextTab+'"></div>').appendTo('#myTabContent');
            $("#append").clone().appendTo("#tab"+nextTab);
            $("#tab"+nextTab).find('input').val('');
            $("#tab"+nextTab).find('img').remove();
            $('#tabs a:last').tab('show');
            var clone = $('#tabTwo').find('.add').clone();
            $('#tabTwo').find('.add').remove();
            clone.appendTo($("#myTabContent div.tab-pane:last-child"));
            nextTab+=1;
        }
        else if(tabname === 'tabFive'){
            var currentTab2 = $('#tabs2 li').size()-1;
            $('<li><a href="#tabFive'+nextTab2+'" data-toggle="tab">فرم '+nextTab2+'<i class="fa fa-times-circle"></i></a></li>').appendTo('#tabs2');
            $('<div class="tab-pane in" id="tabFive'+nextTab2+'"></div>').appendTo('#myTabContent2');
            $("#append2").clone().appendTo("#tabFive"+nextTab2);
            $("#tabFive"+nextTab2).find('input').val('');
            $("#tabFive"+nextTab2).find('img').remove();
            $('#tabFive a:last').tab('show');
            var clone2 = $("#tabFive").find('.add').clone();
            $("#tabFive").find('.add').remove();
            clone2.appendTo($("#myTabContent2 div.tab-pane:last-child"));
            nextTab2+=1;
        }
    });

    $(document).on('click', '.fa-times-circle', function(){
        var tabname = $(this).closest('.content').attr('id');
        console.log(tabname);
        if(tabname === 'tabOne'){
            var n2 = $('#tabs1').children().length-1;
            console.log('n2' , n2);
            var href1 = $(this).closest('li').find('a').attr('href');
            if(n2 > 1){
                var clone1 = $("#tabOne").find('.add').clone();
                $("#tabOne").find('.add').remove();
                $(this).closest('li').remove();
                $(href1).remove();
                clone1.appendTo($("#myTabContent1 div.tab-pane:last-child"));
                $('#tabs1 a:last').tab('show');
            }
        }
        else if(tabname === 'tabTwo'){
            var n = $('#tabs').children().length-1;
            var href = $(this).closest('li').find('a').attr('href');
            if(n > 1){
                var clone = $("#tabTwo").find('.add').clone();
                $("#tabTwo").find('.add').remove();
                $(this).closest('li').remove();
                $(href).remove();
                clone.appendTo($("#myTabContent div.tab-pane:last-child"));
                $('#tabs a:last').tab('show');
            }
        }
        else if(tabname === 'tabFive'){
            var n4 = $('#tabs2').children().length-1;
            var href2 = $(this).closest('li').find('a').attr('href');
            if(n4 > 1){
                var clone2 = $("#tabFive").find('.add').clone();
                $("#tabFive").find('.add').remove();
                $(this).closest('li').remove();
                $(href2).remove();
                clone2.appendTo($("#myTabContent2 div.tab-pane:last-child"));
                $('#tabs2 a:last').tab('show');
            }
        }
    });


});