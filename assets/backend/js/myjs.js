
function showSweetOkAlert(title,text,color,button_txt) {
    swal({
        title: title,
        text: text,
        confirmButtonColor: color,
        confirmButtonText: button_txt
    });
}