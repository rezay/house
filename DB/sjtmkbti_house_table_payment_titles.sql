
-- --------------------------------------------------------

--
-- Table structure for table `payment_titles`
--

CREATE TABLE `payment_titles` (
  `id` int(11) NOT NULL,
  `payment_title` varchar(500) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_titles`
--

INSERT INTO `payment_titles` (`id`, `payment_title`, `status`) VALUES
(4, 'یبلsdfg34', 0),
(5, 'خرید زمین', 0),
(6, 'بابت خرید زمین', 1),
(7, 'بابت جدول بندی', 1),
(8, 'بابت خاکبرداری', 1),
(9, 'بابت فرامحله ای (عمران)', 1),
(10, 'بابت سند', 1),
(11, 'بابت عضویت', 0),
(12, 'بابت طرح و نقشه', 1),
(13, 'بابت نقل و انتقال', 1);
