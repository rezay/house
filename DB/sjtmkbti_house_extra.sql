
--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attorneys`
--
ALTER TABLE `attorneys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ground_id` (`ground_id`),
  ADD KEY `logged_user_id` (`logged_user_id`);

--
-- Indexes for table `attorney_types`
--
ALTER TABLE `attorney_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills_cheques`
--
ALTER TABLE `bills_cheques`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ground_id` (`ground_id`),
  ADD KEY `logged_user_id` (`logged_user_id`);

--
-- Indexes for table `black_list`
--
ALTER TABLE `black_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ground_id` (`ground_id`);

--
-- Indexes for table `build_states`
--
ALTER TABLE `build_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certain_documents`
--
ALTER TABLE `certain_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ground_id` (`ground_id`) USING BTREE,
  ADD KEY `logged_user_id` (`logged_user_id`);

--
-- Indexes for table `debits`
--
ALTER TABLE `debits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logged_user_id` (`ground_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ground_id` (`ground_id`) USING BTREE,
  ADD KEY `logged_user_id` (`logged_user_id`);

--
-- Indexes for table `grounds`
--
ALTER TABLE `grounds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logged_users`
--
ALTER TABLE `logged_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ground_id` (`ground_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `debit_id` (`debit_id`),
  ADD KEY `logged_user_id` (`logged_user_id`);

--
-- Indexes for table `payment_titles`
--
ALTER TABLE `payment_titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelaks`
--
ALTER TABLE `pelaks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `logged_user_id` (`logged_user_id`),
  ADD KEY `ground_id` (`ground_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `attorneys`
--
ALTER TABLE `attorneys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32151;

--
-- AUTO_INCREMENT for table `attorney_types`
--
ALTER TABLE `attorney_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `bills_cheques`
--
ALTER TABLE `bills_cheques`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46436;

--
-- AUTO_INCREMENT for table `black_list`
--
ALTER TABLE `black_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2769;

--
-- AUTO_INCREMENT for table `build_states`
--
ALTER TABLE `build_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=985;

--
-- AUTO_INCREMENT for table `certain_documents`
--
ALTER TABLE `certain_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7536;

--
-- AUTO_INCREMENT for table `debits`
--
ALTER TABLE `debits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8801;

--
-- AUTO_INCREMENT for table `grounds`
--
ALTER TABLE `grounds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23377;

--
-- AUTO_INCREMENT for table `logged_users`
--
ALTER TABLE `logged_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15863;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_titles`
--
ALTER TABLE `payment_titles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pelaks`
--
ALTER TABLE `pelaks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24974;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attorneys`
--
ALTER TABLE `attorneys`
  ADD CONSTRAINT `attorneys_ibfk_1` FOREIGN KEY (`ground_id`) REFERENCES `grounds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bills_cheques`
--
ALTER TABLE `bills_cheques`
  ADD CONSTRAINT `bills_cheques_ibfk_1` FOREIGN KEY (`ground_id`) REFERENCES `grounds` (`id`);

--
-- Constraints for table `black_list`
--
ALTER TABLE `black_list`
  ADD CONSTRAINT `black_list_ibfk_1` FOREIGN KEY (`ground_id`) REFERENCES `grounds` (`id`);

--
-- Constraints for table `certain_documents`
--
ALTER TABLE `certain_documents`
  ADD CONSTRAINT `certain_documents_ibfk_1` FOREIGN KEY (`ground_id`) REFERENCES `grounds` (`id`);

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`ground_id`) REFERENCES `grounds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logged_users`
--
ALTER TABLE `logged_users`
  ADD CONSTRAINT `logged_users_ibfk_1` FOREIGN KEY (`ground_id`) REFERENCES `grounds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`logged_user_id`) REFERENCES `logged_users` (`id`),
  ADD CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`debit_id`) REFERENCES `debits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
