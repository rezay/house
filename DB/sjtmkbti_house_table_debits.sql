
-- --------------------------------------------------------

--
-- Table structure for table `debits`
--

CREATE TABLE `debits` (
  `id` int(11) NOT NULL,
  `ground_id` int(11) NOT NULL,
  `debit_amount` int(11) NOT NULL,
  `accepted_payment` int(11) DEFAULT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0 : silver , 1 : gold',
  `payed` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
