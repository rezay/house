
-- --------------------------------------------------------

--
-- Table structure for table `pelaks`
--

CREATE TABLE `pelaks` (
  `id` int(11) NOT NULL,
  `pelak` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `pelaks`
--

INSERT INTO `pelaks` (`id`, `pelak`, `status`) VALUES
(2, '316/1447', 1),
(4, '4731/5553', 0);
