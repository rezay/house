
-- --------------------------------------------------------

--
-- Table structure for table `build_states`
--

CREATE TABLE `build_states` (
  `id` int(11) NOT NULL,
  `build_state` varchar(500) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `build_states`
--

INSERT INTO `build_states` (`id`, `build_state`, `status`) VALUES
(4, 'مرحله اسکلت', 0),
(3, 'fghیقل', 0),
(5, 'مرحله تحویل کلید', 0),
(6, 'مرحله نازک کاری', 0);
